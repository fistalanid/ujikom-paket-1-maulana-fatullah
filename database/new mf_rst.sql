-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 07:29 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mf_rst`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_cart` enum('I','O','X') NOT NULL DEFAULT 'I' COMMENT 'O Terverifikasi ,I Belum Terverifikasi ,X Dibatalkan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id_cart`, `id_user`, `id_masakan`, `kuantitas`, `harga`, `total_harga`, `keterangan`, `status_cart`) VALUES
(23, 13, 1, 131, 25000, 3275000, 'adasdas', 'I'),
(25, 0, 1, 2, 25000, 50000, 'e', 'I');

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_detail_order` enum('Diproses','Selesai','Dibayar') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `kuantitas`, `keterangan`, `status_detail_order`) VALUES
(17, 104, 3, 223, 'ssssss', 'Dibayar'),
(18, 105, 1, 22, 'apaa', 'Dibayar'),
(19, 105, 2, 9, 'apla', 'Dibayar'),
(20, 105, 3, 8, 'asp', 'Dibayar'),
(21, 112, 1, 2, 'w', 'Selesai');

-- --------------------------------------------------------

--
-- Table structure for table `f_about`
--

CREATE TABLE `f_about` (
  `id_about` int(11) NOT NULL,
  `d_facebook` text NOT NULL,
  `d_instagram` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_product`
--

CREATE TABLE `f_product` (
  `id_product` int(11) NOT NULL,
  `d_product_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_services`
--

CREATE TABLE `f_services` (
  `id_services` int(11) NOT NULL,
  `d_services_desc` text NOT NULL,
  `d_first_services_title` varchar(30) NOT NULL,
  `d_second_services_title` varchar(30) NOT NULL,
  `d_third_services_title` varchar(30) NOT NULL,
  `d_first_services_desc` text NOT NULL,
  `d_second_services_desc` text NOT NULL,
  `d_third_services_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_top_home`
--

CREATE TABLE `f_top_home` (
  `id_home` int(11) NOT NULL,
  `d_title` text NOT NULL,
  `d_subtitle` text NOT NULL,
  `d_video` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `f_top_home`
--

INSERT INTO `f_top_home` (`id_home`, `d_title`, `d_subtitle`, `d_video`) VALUES
(1, 'Menyajikan berbagai masakan khas Indonesia', 'Kunjungi setiap hari dari jam 10:00 - 21:00', 'https://www.youtube.com/');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `jenis` enum('Makanan','Minuman') NOT NULL,
  `nama_kategori` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `jenis`, `nama_kategori`) VALUES
(1, 'Minuman', 'Jajanan'),
(2, 'Makanan', 'Masakan Jawa'),
(3, 'Makanan', 'Masakan Kalimantan'),
(4, 'Makanan', 'Kue Indonesia'),
(6, 'Makanan', 'Makanan ringan Indonesia'),
(7, 'Makanan', 'Masakan Mandailing'),
(8, 'Minuman', 'Masakan Sunda'),
(9, 'Makanan', 'Masakan Sumatra'),
(10, 'Makanan', 'Ayam');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` enum('admin','pelayan','kasir','owner','pelanggan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'pelayan'),
(3, 'kasir'),
(4, 'owner'),
(5, 'pelanggan');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL,
  `nama_masakan` varchar(50) NOT NULL,
  `harga` varchar(20) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `jenis` enum('Makanan','Minuman') NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `status_masakan` enum('Tersedia','Habis') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `harga`, `gambar`, `jenis`, `id_kategori`, `status_masakan`) VALUES
(1, 'Gorengan', '25000', 'distributor.png', 'Makanan', 1, 'Tersedia'),
(2, 'Ayam Goreng', '20000', 'images.jpg', 'Makanan', 10, 'Tersedia'),
(3, 'Toge Goreng', '10000', 'kecambah-toge-goreng-foto-resep-utama.jpg', 'Makanan', 2, 'Tersedia'),
(4, 'Sate Ayam', '20000', 'resep-sate-ayam-madura-mencicipi-makanan-asli-indonesia-yang-mendunia.jpg', 'Makanan', 2, 'Tersedia'),
(5, 'Karedok', '15000', '1200px-Karedok.jfif', 'Makanan', 8, 'Tersedia'),
(8, 'Dodongkal', '5000', 'Dongkal.jpg', 'Makanan', 8, 'Tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `id_meja` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `status_meja` enum('O','I','X') NOT NULL DEFAULT 'O' COMMENT 'O kosong ,I dipesan ,X sudah di booking'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id_meja`, `no_meja`, `status_meja`) VALUES
(1, 1, 'I'),
(2, 2, 'I'),
(3, 3, 'O'),
(4, 4, 'O'),
(5, 5, 'O'),
(6, 6, 'O'),
(7, 7, 'O'),
(8, 8, 'O'),
(9, 9, 'O'),
(10, 10, 'O'),
(11, 11, 'O'),
(12, 12, 'O'),
(13, 13, 'O'),
(14, 14, 'O'),
(15, 15, 'O');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL,
  `nama_pelanggan` varchar(40) NOT NULL,
  `id_meja` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status` enum('O','X','V') NOT NULL DEFAULT 'X' COMMENT 'X untuk pesanan langsung ,O login ,V Terverifikasi',
  `status_order` enum('Belum Memesan','Sudah Memesan','Dibatalkan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id_order`, `nama_pelanggan`, `id_meja`, `tanggal`, `id_user`, `keterangan`, `status`, `status_order`) VALUES
(104, 'Saya', 1, '2019-04-04 04:42:58', 15, 'asdasaaa', 'V', 'Sudah Memesan'),
(105, 'Maul', 3, '2019-04-04 08:02:01', 16, 'ul\r\n', 'V', 'Sudah Memesan'),
(111, '', 1, '2019-04-05 21:05:08', 17, '', 'V', 'Belum Memesan'),
(112, 'Maulana Fatullah', 1, '2019-04-05 21:30:32', 2, 'a', 'O', 'Sudah Memesan'),
(113, '', 0, '2019-04-06 11:44:35', 0, '', 'O', 'Belum Memesan'),
(114, '', 2, '2019-04-06 11:44:40', 18, '', 'V', 'Sudah Memesan'),
(115, '', 2, '2019-04-06 12:00:37', 19, '', 'V', 'Belum Memesan'),
(116, 's', 2, '2019-04-06 12:06:21', 20, '', 'V', 'Sudah Memesan'),
(117, '', 2, '2019-04-06 12:07:42', 21, '', 'V', 'Belum Memesan'),
(118, '', 2, '2019-04-06 12:27:59', 22, '', 'O', 'Belum Memesan');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL,
  `provinsi` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `provinsi`) VALUES
(1, 'Aceh'),
(2, 'Sumatera Utara'),
(3, 'Bengkulu'),
(4, 'Jambi'),
(5, 'Riau'),
(6, 'Sumatera Barat'),
(7, 'Sumatera Selatan'),
(8, 'Lampung'),
(9, 'Kepulauan Bangka Belitung'),
(10, 'Kepulauan Riau'),
(11, 'Banten'),
(12, 'Jawa Barat'),
(13, 'DKI Jakarta'),
(14, 'Jawa Tengah'),
(15, 'Jawa Timur'),
(16, 'Daerah Istimewa Yogyakarta'),
(17, 'Bali'),
(18, 'Nusa Tenggara Barat'),
(19, 'Nusa Tenggara Timur'),
(20, 'Kalimantan Barat'),
(21, 'Kalimantan Selatan'),
(22, 'Kalimantan Tengah'),
(23, 'Kalimantan Timur'),
(24, 'Gorontalo'),
(25, 'Sulawesi Selatan'),
(26, 'Sulawesi Tenggara'),
(27, 'Sulawesi Tengah'),
(28, 'Sulawesi Utara'),
(29, 'Sulawesi Barat'),
(30, 'Maluku'),
(31, 'Maluku Utara'),
(32, 'Papua'),
(33, 'Papua Barat'),
(34, 'Aceh');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recovery_keys`
--

INSERT INTO `recovery_keys` (`rid`, `userID`, `token`, `valid`) VALUES
(1, 2, '120ac43659e9dfaf8228213fa3fd836d', 1),
(2, 2, 'bcc281b5349121a179fc5e99828c6972', 1),
(3, 2, '24a2766ce98547665d3c01f57ecd128f', 1),
(4, 2, 'f9e567b3a1304b5f326f796857da0c3c', 1),
(5, 2, 'f904bc7463665113de711a855d5fd6bb', 1),
(6, 2, '1ee630b9671a240c0dd9487a290f47d5', 0),
(7, 2, '8a5b7eead8a83fb57012096944f166cf', 1),
(8, 2, '3f3b3c556b1cf0d3c430fd941b26ef34', 0),
(9, 2, 'b09f1213a943c6bb021b145a563740ce', 0),
(10, 2, '62fa590c3e703e2d9a0b7d925d17c0c0', 0),
(11, 2, '92a86903ccdc48b5dabebbf3ead00c58', 1),
(12, 2, '55883fac7f276f16101d6e3b7c98c7cb', 0),
(13, 2, 'f01068587f7c36d498ed8210d597cd04', 1),
(14, 2, '4ad42685efc4cb9e46ed8c7b0678d7fd', 0),
(15, 2, 'bf25d838a563834306c18ecc3c84b468', 1),
(16, 2, '800aabea735dfd608143579a748c2d81', 1),
(17, 2, 'd987ebbf26313b133d1c03be35c7efeb', 1),
(18, 5, '9c51ba95d78cce7451efb7468b805793', 0),
(19, 4, '9cf221258637b22f3119d013845ad7c1', 0),
(20, 5, '7cfc7c61d931fe9f1420a41fdeb00c59', 1),
(21, 1, '651f11115c22ea18672fb926b2f87d4a', 1),
(22, 1, 'af6bfc732abe7e8d22770e77c413f3d6', 1),
(23, 1, '25f747e6d4ea954f217bea97a09154fd', 1),
(24, 1, '92fab61a70b099814bf2873680605792', 1),
(25, 1, '3008a5dc7de07d278d1ad98c61a0e051', 1),
(26, 2, 'f186b80a36bff42dd11ef9cf8a9ca248', 1),
(27, 2, '5b57b2db864251b3be5fe313bbf4d163', 1),
(28, 2, '59356440ee9434c2e5402f6ecce02e32', 1),
(29, 2, '22aa8c8d587a2d309cd4fcafdc75e53d', 1),
(30, 2, 'f240e63e3cf7343d5ae1db92bf205bd3', 1),
(31, 2, '09dbb21f6785d8623ee66f61ddda2f35', 1),
(32, 4, '4666ecfaa4934ce3521027322f26d416', 1),
(33, 4, 'f1729485653ca4d43cef5bd751a70cba', 1),
(34, 4, '1591dde42fcb373432c1c3c494aac1b0', 0),
(35, 4, 'c222f95f9b3fe86e70e5245eddfb1565', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `total_bayar` varchar(20) NOT NULL,
  `kembalian` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`, `kembalian`) VALUES
(39, 15, 104, '2019-04-11 07:51:41', '2230000', '770000'),
(41, 5, 105, '2019-04-06 07:51:41', '80000', '20000'),
(42, 5, 108, '2019-03-12 00:00:00', '20000', '5000');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(40) NOT NULL,
  `email` varchar(35) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `id_meja` int(11) DEFAULT NULL,
  `id_level` int(11) NOT NULL,
  `status` enum('I','O') NOT NULL DEFAULT 'O' COMMENT 'I Tidak Aktif ,O Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `email`, `username`, `password`, `id_meja`, `id_level`, `status`) VALUES
(1, 'Admin', 'admin@gmail.com\r\n\r\n', 'admin', 'admin', NULL, 1, 'O'),
(2, 'pelayan', 'pelayan@gmail.com', 'pelayan', 'pelayan', NULL, 2, 'O'),
(3, 'kasir', 'kasir@gmail.com', 'kasir', 'kasir', NULL, 3, 'O'),
(4, 'owner', 'fistalanid@gmail.com', 'owner', 'owner', NULL, 4, 'O'),
(21, '', 'null', 'RST2', '189561', 2, 5, 'I'),
(22, '', 'null', 'RST2', '920684', 2, 5, 'O');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id_detail_order`);

--
-- Indexes for table `f_about`
--
ALTER TABLE `f_about`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `f_product`
--
ALTER TABLE `f_product`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `f_services`
--
ALTER TABLE `f_services`
  ADD PRIMARY KEY (`id_services`);

--
-- Indexes for table `f_top_home`
--
ALTER TABLE `f_top_home`
  ADD PRIMARY KEY (`id_home`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
  ADD PRIMARY KEY (`id_masakan`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `f_about`
--
ALTER TABLE `f_about`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_product`
--
ALTER TABLE `f_product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_services`
--
ALTER TABLE `f_services`
  MODIFY `id_services` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_top_home`
--
ALTER TABLE `f_top_home`
  MODIFY `id_home` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
  MODIFY `id_masakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_provinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

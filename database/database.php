<?php 

class database{

	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "mf_rst";
	public $mysqli;

	function __construct(){
		$this->mysqli = new mysqli($this->host, $this->uname, $this->pass ,$this->db);
	}

	function mf_show_kasir(){
		$data =$this->mysqli->query("SELECT * FROM user WHERE id_level = 3");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	
	function mf_show_pelayan(){
		$data =$this->mysqli->query("SELECT * FROM user WHERE id_level = 2");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	
	function mf_show_transaksi(){
		$data =$this->mysqli->query("SELECT *,transaksi.tanggal FROM transaksi JOIN `order` ON transaksi.id_order=`order`.id_order");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		
		if (mysqli_num_rows($data) < 1) {
			error_reporting(0);
			echo "<h6 style='color:red'>* Transaksi Kosong</h6>";
		}
		return $hasil;
	}
	
	function mf_show_today_transaksi(){
		$data =$this->mysqli->query("SELECT *,transaksi.tanggal FROM `transaksi` JOIN `order` ON transaksi.id_order=`order`.id_order WHERE DATE(transaksi.tanggal) = DATE(NOW())");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		if (mysqli_num_rows($data) < 1) {
			error_reporting(0);
			echo "<h6 style='color:red'>* Transaksi Hari ini Kosong</h6>";
		}
		return $hasil;
	}

	function mf_show_week_transaksi(){
		$data =$this->mysqli->query("SELECT *,transaksi.tanggal FROM `transaksi` JOIN `order` ON transaksi.id_order=`order`.id_order WHERE YEARWEEK(transaksi.tanggal)=YEARWEEK(NOW())");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		
		if (mysqli_num_rows($data) < 1) {
			error_reporting(0);
			echo "<h6 style='color:red'>* Transaksi Minggu ini Kosong</h6>";
		}
		return $hasil;
	}

	function mf_show_month_transaksi(){
		$data =$this->mysqli->query("SELECT *,transaksi.tanggal FROM `transaksi` JOIN `order` ON transaksi.id_order=`order`.id_order WHERE MONTH(transaksi.tanggal) = MONTH(CURDATE())");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		
		if (mysqli_num_rows($data) < 1) {
			error_reporting(0);
			echo "<h6 style='color:red'>* Transaksi Bulan ini Kosong</h6>";
		}
		return $hasil;
	}
	
	function mf_show_owner(){
		$data =$this->mysqli->query("SELECT * FROM user WHERE id_level = 4");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function mf_show_masakan(){
		$data =$this->mysqli->query("SELECT * from `meja` JOIN `order` ON `meja`.no_meja=`order`.no_meja WHERE meja.status_meja = 'I'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function mf_update_service($q,$w,$e,$r,$t,$y,$u,$id_service){
		$data =$this->mysqli->query("UPDATE `f_services` SET `d_services_desc` = '$q', `d_first_services_title` = '$w', `d_second_services_title` = '$e', `d_third_services_title` = '$r', `d_first_services_desc` = '$t', `d_second_services_desc` = '$y', `d_third_services_desc` = '$u' WHERE `f_services`.`id_services` = '$id_service';");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	
	// function mf_show_cart(){
	// 	$data =$this->mysqli->query("SELECT * FROM cart JOIN masakan ON cart.id_masakan=masakan.id_masakan WHERE cart.id_user = ");
	// 	while($d = mysqli_fetch_array($data)){
	// 		$hasil[] = $d;
	// 	}
	// 	return $hasil;
	// }

	function mf_min_tambah_pelayan($nama_user,$email,$username,$password){
		$data = $this->mysqli->query("insert into user values('','$nama_user','$email','$username','$password','','2','O')");
	}

	function mf_min_tambah_kasir($nama_user,$email,$username,$password){
		$data = $this->mysqli->query("insert into user values('','$nama_user','$email','$username','$password','','3','O')");
	}

	function mf_min_tambah_owner($nama_user,$email,$username,$password){
		$data = $this->mysqli->query("insert into user values('','$nama_user','$email','$username','$password','','4','O')");
	}
	
	function mf_min_tambah_pelanggan($username,$password,$id_meja){
		$data = $this->mysqli->query("insert into user values('','null','null','$username','$password','$id_meja','5','O')");
	}

	function mf_min_to_cart($id_user,$id_masakan,$kuantitas,$harga,$total_harga,$keterangan){
		$data = $this->mysqli->query("insert into cart values('','$id_user','$id_masakan','$kuantitas','$harga','$total_harga','$keterangan','I')");
	}

	function mf_min_tambah_pesanan_langsung($nama_pelanggan,$no_meja,$id_user,$keterangan){
		$data = $this->mysqli->query("insert into `order` values('','$nama_pelanggan','$no_meja',NOW(),'$id_user','$keterangan','O','Belum Memesan')");
	}

	function mf_min_tambah_pesanan($id_order,$id_masakan,$kuantitas,$keterangan){
		$data = $this->mysqli->query("insert into detail_order values('','$id_order','$id_masakan','$kuantitas','$keterangan','Diproses')");
	}

	function mf_update_pesanan_langsung($id_order){
		$data = $this->mysqli->query("update `order` set status_order = 'Sudah Memesan' where id_order = '$id_order'");
	}

	function mf_update_meja($id_meja){
		$data = $this->mysqli->query("update meja set status_meja = 'I' where id_meja = '$id_meja'");
	}
	
	function mf_update_meja_langsung($id_meja){
		$data = $this->mysqli->query("update meja set status_meja = 'I' where no_meja = '$id_meja'");
	}
	
	function mf_update_keterangan_cart($keterangan,$kuantitas,$id_cart){
		$data = $this->mysqli->query("UPDATE `cart` SET `kuantitas` = '$kuantitas', `keterangan` = '$keterangan' WHERE `cart`.`id_cart` = '$id_cart'");
	}

	function mf_update_order_pelanggan($nama_pelanggan,$keterangan,$id_meja,$id_order){
		$data = $this->mysqli->query("UPDATE `mf_rst`.`order` SET `nama_pelanggan` = '$nama_pelanggan', `keterangan` = '$keterangan', `id_meja` = '$id_meja' WHERE `order`.`id_order` = '$id_order'");
	}

	function mf_update_user_pelanggan($nama_user,$id_user){
		$data = $this->mysqli->query("UPDATE `user` SET `nama_user` = '$nama_user' WHERE `user`.`id_user` = '$id_user'");
	}

	function mf_update_meja_pelanggan_kosong($id_meja){
		$data = $this->mysqli->query("update meja set status_meja = 'O' where id_meja = '$id_meja'");
	}

	function mf_update_meja_pelanggan_dipesan($id_meja){
		$data = $this->mysqli->query("update meja set status_meja = 'I' where id_meja = '$id_meja'");
	}

	function mf_min_cancel_cart($id_cart){
		$data = $this->mysqli->query("update cart set status_cart = 'X' where id_cart = '$id_cart'");
	}

	function mf_min_to_order($nama_pelanggan,$id_meja,$id_user,$keterangan){
		$data = $this->mysqli->query("insert into `order` values('','$nama_pelanggan','$id_meja',NOW(),'$id_user','$keterangan','O','Sudah Memesan')");
	}

	function mf_min_pesan_sekarang($nama_pelanggan,$id_meja,$id_user,$keterangan){
		$data = $this->mysqli->query("insert into `order` values('','$nama_pelanggan','$id_meja',NOW(),'$id_user','$keterangan','X','Belum Memesan')");
	}

	function mf_min_update_home($d_title,$d_subtitle,$d_video){
		$data = $this->mysqli->query("UPDATE `f_top_home` SET `d_title` = '$d_title', `d_subtitle` = '$d_subtitle', `d_video` = '$d_video' WHERE `f_top_home`.`id_home` = 1;");
	}
	
	function mf_min_insert_transaction($id_user,$id_order,$total_bayar,$kembalian){
		$data = $this->mysqli->query("INSERT INTO `transaksi` VALUES ('', '$id_user', '$id_order', NOW(), '$total_bayar', '$kembalian')");
	}

	function mf_min_update_status_order($id_order){
		$data = $this->mysqli->query("UPDATE `order` SET `status_order` = 'Sudah Memesan' WHERE `order`.`id_order` = '$id_order'");
	}

	function mf_min_update_detail_order($kuantitas,$keterangan,$id_detail_order){
		$data = $this->mysqli->query("UPDATE `detail_order` SET `kuantitas` = '$kuantitas', `keterangan` = '$keterangan' WHERE `detail_order`.`id_detail_order` = '$id_detail_order'");
	}

	function mf_min_update_status_detail_order_paid($id_order){
		$data = $this->mysqli->query("UPDATE `detail_order` SET `status_detail_order` = 'Dibayar' WHERE `detail_order`.`id_order` = '$id_order'");
	}
	
	function mf_min_update_status_order_paid($id_order){
		$data = $this->mysqli->query("UPDATE `order` SET `status` = 'V' WHERE `order`.`id_order` = '$id_order'");
	}
		
	function mf_min_update_status_customer_inactive($id_user){
		$data = $this->mysqli->query("UPDATE `user` SET `status` = 'I' WHERE `user`.`id_user` = '$id_user';");
	}
		
	function mf_min_update_status_table_active($id_meja){
		$data = $this->mysqli->query("UPDATE `meja` SET `status_meja` = 'O' WHERE `meja`.`id_meja` = '$id_meja';");
	}

	// function hapus($id_user){
	// 	$data = $this->mysqli->query("DELETE from user where id_user='$id_user'");
	// }
	
	function edit($id_user){
		$data =$this->mysqli->query("SELECT * FROM user INNER JOIN level ON user.id_level = level.id_level where id_user='$id_user'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function mf_min_ubah_user($id_user,$nama_user,$email,$username,$password_aman,$status){
		$data = $this->mysqli->query("update user set nama_user ='$nama_user', email ='$email', username ='$username', password ='$password_aman' , status ='$status' where id_user='$id_user'");
	}

	function tampil_data_kategori(){
		$data =$this->mysqli->query("SELECT * FROM kategori");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}


	function edit_kategori($id){
		$data =$this->mysqli->query("SELECT * FROM kategori where id='$id'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

} 
 
?>
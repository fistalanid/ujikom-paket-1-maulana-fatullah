<?php
include "../view/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
    <?php include "head.php" ?>
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <body>

  <!-- START: header -->
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      <?php include "header.php" ?>  
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <!-- END: header -->
  
  <!-- START: section -->
  <section class="probootstrap-intro" style="background-image: url(img/hero_bg_2.jpg);" data-stellar-background-ratio="0.5">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-7 probootstrap-intro-text">
          <h1 class="probootstrap-animate">Masakan</h1>
          <div class="probootstrap-subtitle probootstrap-animate">
            <h2>RST menyediakan berbagai macam masakan khasa Indonesia</h2>
          </div>
          <p class="watch-intro probootstrap-animate"><a href="https://vimeo.com/45830194" class="popup-vimeo">Watch the video <i class="icon-play2"></i></a></p>
        </div>
      </div>
    </div>
    <a class="probootstrap-scroll-down js-next" href="#next-section">Scroll down <i class="icon-chevron-down"></i></a>
  </section>
  <!-- END: section -->
  

  <section id="next-section" class="probootstrap-section">
    <div class="container">
      <div class="row">
      <?php
            $da = $mysqli->query("SELECT * FROM `masakan` ");
            
            while($asdas = mysqli_fetch_array($da)){
              $gambar       = $asdas['gambar'];
              $nama_masakan = $asdas['nama_masakan'];
              $harga        = $asdas['harga'];
        ?>
        <div class="col-md-4 col-sm-6 probootstrap-animate">
          <div class="probootstrap-block-image">
            <figure><a href="#"><img src="../assets/images/masakan/<?php echo $gambar; ?>" alt="Free Bootstrap Template by uicookies.com"></a></figure>
            <div class="text">
              <h3 class="mb30"><a href="#"><?= $nama_masakan ?></a></h3>

              <hr>
              <p class="clearfix like"><a class="pull-left" href="#"><?= "Rp.",number_format($harga) ?></a></p>
            </div>
          </div>
        </div>
            <?php } ?>
        <div class="clearfix visible-sm-block"></div>
      </div>
    </div>
  </section>


  <!-- START: footer -->
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      <?php include "footer.php" ?>
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <!-- END: footer -->

  </body>
</html>
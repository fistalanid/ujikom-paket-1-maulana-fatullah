  
  <!-- START: footer -->
  <footer role="contentinfo" class="probootstrap-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Tentang Web</h3>
            <p>Aplikasi ini dibuat untuk mempermudah pelanggan dan karyawan
        dalam menjalankan transaksi di restauran .Pelanggan akan melihat halaman Front End
        terlebih dulu untuk memperkenalkan aplikasi ,restauran dan masakan .</p>
          </div>
          <div class="probootstrap-footer-widget">
            <h3>Kontak Kami</h3>
            <ul class="probootstrap-footer-social">
              <li><a href="https://www.facebook.com/maulfatullah"><i class="icon-facebook"></i></a></li>
              <li><a href="https://www.instagram.com/maulfatullah_/"><i class="icon-instagram2"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Masakan Populer</h3>
            <ul class="probootstrap-product-list">
              <?php
                  $da = $mysqli->query("SELECT * FROM `masakan` JOIN kategori ON masakan.id_kategori=kategori.id_kategori");
                  
                  while($asdas = mysqli_fetch_array($da)){
                    $gambar         = $asdas['gambar'];
                    $nama_masakan   = $asdas['nama_masakan'];
                    $nama_kategori  = $asdas['nama_kategori'];
                    $harga          = $asdas['harga'];
              ?>
              <li class="mb20">
                <a href="#">
                  <figure><img src="../assets/images/masakan/<?php echo $gambar; ?>" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></figure>
                  <div class="text">
                    <h4><?= $nama_masakan ?></h4>
                    <p><?= $nama_kategori ?></p>
                    <p class="secondary-color rate"><?= "Rp.",number_format($harga) ?></p>
                  </div>
                </a>
              </li>
                  <?php } ?>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="probootstrap-footer-widget">
            <h3>Masakan Terbaru</h3>
            <ul class="probootstrap-blog-list">
            <?php
                  $da = $mysqli->query("SELECT * FROM `masakan` JOIN kategori ON masakan.id_kategori=kategori.id_kategori");
                  
                  while($asdas = mysqli_fetch_array($da)){
                    $gambar         = $asdas['gambar'];
                    $nama_masakan   = $asdas['nama_masakan'];
                    $nama_kategori  = $asdas['nama_kategori'];
                    $harga          = $asdas['harga'];
              ?>
              <li>
                <a href="#">
                  <figure><img src="../assets/images/masakan/<?php echo $gambar; ?>" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></figure>
                  <div class="text">
                    <h4><?= $nama_masakan ?></h4>
                    <p><?= $nama_kategori ?></p>
                    <p class="secondary-color rate"><?= "Rp.",number_format($harga) ?></p>
                  </div>
                </a>
              </li>
                  <?php } ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="row mt40">
        <div class="col-md-12 text-center">
          <p>
            <small>&copy; 2019 RST<br>
            <a href="#" class="js-backtotop">Back to top</a>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- END: footer -->

  <script src="js/scripts.min.js"></script>
  <script src="js/main.min.js"></script>
  <script src="js/custom.js"></script>

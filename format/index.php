<?php
include "../view/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      <?php include "head.php" ?>
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <body>
<a href="../view" style="z-index: 9999999999999;position: absolute ;">.</a>
  <!-- START: header -->
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      <?php include "header.php" ?>  
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <!-- END: header -->
  
  <!-- START: section -->
  <section class="probootstrap-intro" style="background-image: url(img/hero_bg_2.jpg);" data-stellar-background-ratio="0.5">
    <div class="container-fluid">
      <div class="row">
        <?php
            $sss = $mysqli->query("SELECT * FROM `f_top_home`");
            
            while($ddd = mysqli_fetch_array($sss)){
            
                $d_title    = $ddd['d_title'];
                $d_subtitle = $ddd['d_subtitle'];
                $d_video    = $ddd['d_video'];
        ?>
        <div class="col-md-7 probootstrap-intro-text">
          <h1 class="probootstrap-animate"><?= $d_title ?></h1>
          <div class="probootstrap-subtitle probootstrap-animate">
            <h2><?= $d_subtitle ?></h2>
          </div>
          <p class="watch-intro probootstrap-animate"><a href="<?= $d_video ?>" class="popup-vimeo">Watch the video <i class="icon-play2"></i></a></p>
        </div>
      <?php } ?>
      </div>
    </div>
    <a class="probootstrap-scroll-down js-next" href="#next-section">Scroll down <i class="icon-chevron-down"></i></a>
  </section>
  <!-- END: section -->
  

  <section id="next-section">
    <div class="container-fluid">
      <div class="row probootstrap-gutter0">
      <?php
            $da = $mysqli->query("SELECT * FROM `masakan` ");
            
            while($asdas = mysqli_fetch_array($da)){
              $gambar       = $asdas['gambar'];
              $nama_masakan = $asdas['nama_masakan'];
              $harga        = $asdas['harga'];
        ?>
        <div class="col-md-4 col-sm-6">
          <a href="#" class="probootstrap-hover-overlay">
            <img src="../assets/images/masakan/<?php echo $gambar; ?>" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
            <div class="probootstrap-text-overlay">
              <h3><?= $nama_masakan ?></h3>
              <p><?= "Rp.",number_format($harga) ?></p>
            </div>
          </a>
        </div>
            <?php } ?>
      </div>
    </div>
  </section>

  <!-- START: section -->
  <?php
                $sss = $mysqli->query("SELECT * FROM `f_services`");
                
                while($ddd = mysqli_fetch_array($sss)){
                
                    $id_services                = $ddd['id_services'];
                    $d_services_desc            = $ddd['d_services_desc'];
                    $d_first_services_title     = $ddd['d_first_services_title'];
                    $d_second_services_title    = $ddd['d_second_services_title'];
                    $d_third_services_title     = $ddd['d_third_services_title'];
                    $d_first_services_desc      = $ddd['d_first_services_desc'];
                    $d_second_services_desc     = $ddd['d_second_services_desc'];
                    $d_third_services_desc      = $ddd['d_third_services_desc'];
            ?>
            <section class="probootstrap-section">
                <div class="container">
                    <div class="row">
                        <div
                            class="col-lg-12 col-md-12 mb70 section-heading probootstrap-animate fadeInUp probootstrap-animated">
                            <h2>Pelayanan Kami</h2>
                            <p class="lead"><?= $d_services_desc ?></p>
                        </div>
                    </div>
                    <div class="row mb70">
                        <div class="col-md-4 probootstrap-animate fadeInUp probootstrap-animated">
                            <div class="probootstrap-box">
                                <div class="icon text-center"><i class="icon-tools2"></i></div>
                                <h3><?= $d_first_services_title ?></h3>
                                <p><?= $d_first_services_desc ?></p>
                            </div>
                        </div>
                        <div class="col-md-4 probootstrap-animate fadeInUp probootstrap-animated">
                            <div class="probootstrap-box">
                                <div class="icon text-center"><i class="icon-desktop"></i></div>
                                <h3><?= $d_second_services_title ?></h3>
                                <p><?= $d_second_services_desc ?></p>
                            </div>
                        </div>
                        <div class="col-md-4 probootstrap-animate fadeInUp probootstrap-animated">
                            <div class="probootstrap-box">
                                <div class="icon text-center"><i class="icon-lightbulb"></i></div>
                                <h3><?= $d_third_services_title ?></h3>
                                <p><?= $d_third_services_desc ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </section>
                            <?php } ?>
  <!-- END: section -->


  <!-- START: section -->
  <!-- <section class="probootstrap-section probootstrap-section-colored">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 mb70 section-heading probootstrap-animate">
          <h2>Our Product</h2>
          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
        </div>
      </div>
      <div class="row mb70">
        <div class="col-md-4 probootstrap-animate">
          <div class="probootstrap-block-image">
            <figure><a href="#"><img src="img/img_1.jpg" alt="Free Bootstrap Template by uicookies.com"></a></figure>
            <div class="text">
              <h3 class="mb30"><a href="#">Duden Flows by Their Place</a></h3>
              <p class="dark">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
              <p class="secondary-color rate"><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-half"></i></p>
              <hr>
              <p class="clearfix like"><a class="pull-left" href="#"><i class="icon-thumbs-up"></i> 5,216</a> <a class="pull-right" href="#"><i class="icon-thumbs-down"></i> 32</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-4 probootstrap-animate">
          <div class="probootstrap-block-image">
            <figure><img src="img/img_2.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
            <div class="text">
              <h3 class="mb30"><a href="#">Fly Into Your Mouth</a></h3>
              <p class="dark">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
              <p class="secondary-color rate"><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-half"></i></p>
              <hr>
              <p class="clearfix like"><a class="pull-left" href="#"><i class="icon-thumbs-up"></i> 4,923</a> <a class="pull-right" href="#"><i class="icon-thumbs-down"></i> 12</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-4 probootstrap-animate">
          <div class="probootstrap-block-image">
            <figure><img src="img/img_3.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
            <div class="text">
              <h3 class="mb30"><a href="#">Roasted Parts of Sentences</a></h3>
              <p class="dark">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
              <p class="secondary-color rate"><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-half"></i></p>
              <hr>
              <p class="clearfix like"><a class="pull-left" href="#"><i class="icon-thumbs-up"></i> 19,552</a> <a class="pull-right" href="#"><i class="icon-thumbs-down"></i> 7</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-4 probootstrap-animate">
          <p class="text-center">
            <a href="#" class="btn btn-ghost btn-ghost-white btn-lg btn-block" role="button">View All Products</a>
          </p>
        </div>
      </div>
    </div>
  </section> -->
  <!-- END: section -->

<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <?php include "footer.php" ?>
<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  </body>
</html>
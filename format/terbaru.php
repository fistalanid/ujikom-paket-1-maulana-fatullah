<?php
include "../view/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
    <?php include "head.php" ?>
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <body>

  <!-- START: head.php -->
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
    <?php include "header.php" ?>  
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <!-- END: head.php -->
  
  <!-- START: section -->
  <section class="probootstrap-intro" style="background-image: url(img/hero_bg_2.jpg);" data-stellar-background-ratio="0.5">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-7 probootstrap-intro-text">
          <h1 class="probootstrap-animate">Masakan Terbaru RST</h1>
          <div class="probootstrap-subtitle probootstrap-animate">
            <h2>RST telah menyediakan beberapa menu baru yang telah dibuat oeh para Juru Masak RST</h2>
          </div>
        </div>
      </div>
    </div>
    <a class="probootstrap-scroll-down js-next" href="#next-section">Scroll down <i class="icon-chevron-down"></i></a>
  </section>
  <!-- END: section -->
  

  <section id="next-section">
    <div class="container-fluid">
      <div class="row probootstrap-gutter0">
      <?php
            $da = $mysqli->query("SELECT * FROM `masakan` ORDER BY nama_masakan ASC LIMIT 6");
            
            while($asdas = mysqli_fetch_array($da)){
              $gambar       = $asdas['gambar'];
              $nama_masakan = $asdas['nama_masakan'];
              $harga       = $asdas['harga'];
        ?>
        <div class="col-md-4 col-sm-6">
          <a href="#" class="probootstrap-hover-overlay">
            <img src="../assets/images/masakan/<?php echo $gambar; ?>" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
            <div class="probootstrap-text-overlay">
              <h3><?= $nama_masakan ?></h3>
              <p><?= "Rp.",number_format($harga) ?></p>
            </div>
          </a>
        </div>
            <?php } ?>
      </div>
    </div>
  </section>

  
  <!-- START: footer.php -->
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      <?php include "footer.php" ?>
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <!-- END: footer.php -->

  </body>
</html>
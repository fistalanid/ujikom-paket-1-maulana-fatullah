<footer class="page-footer gradient-45deg-light-blue-cyan" style="<?= $warna ?>">
    <div class="footer-copyright">
        <div class="container">
            <span>Copyright ©
                <script type="text/javascript">
                    document.write(new Date().getFullYear());
                </script></span>
            <span class="right hide-on-small-only"> RST 1.10 Beta</span>
        </div>
    </div>
</footer>
<!-- jQuery Library -->
<script type="text/javascript" src="../../assets/vendors/jquery-3.2.1.min.js"></script>
<!--materialize js-->
<script type="text/javascript" src="../../assets/js/materialize.min.js"></script>
<!--scrollbar-->
<script type="text/javascript" src="../../assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="../../assets/js/plugins.js"></script>
<!--custom-script.js - Add your own theme custom JS-->
<script type="text/javascript" src="../../assets/js/custom-script.js"></script>

<!-- DataTables JavaScript -->
<script src="../../assets/vendors/datatables/js/jquery.dataTables.min.js"></script>
<script src="../../assets/vendors/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../../assets/vendors/datatables-responsive/dataTables.responsive.js"></script>

<!-- <script src="../../assets/js/m-materialize.js"></script> -->
<script src="../../assets/js/init.js"></script>
<script src="../../assets/js/cleave.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>


<script>
    var cleave = new Cleave('#input-phone', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
</script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
</script>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function () {
        $('.modal').modal();
    });
</script>

<script>
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('select').formSelect();
  });
</script>
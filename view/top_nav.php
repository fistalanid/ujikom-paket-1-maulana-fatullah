<?php
include "conn.php";
include "koneksi.php";
if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_administrator");
}

elseif($_SESSION['id_level']=="1"){
  $warna  = "background: linear-gradient(45deg, #363442 0%, #2196F3 50%, #E91E63 100%);";
  $widget = "background: #363442";
  $widget_t = "background: #2e8fea";
  $widget_b = "background: #dc266d";
}

elseif($_SESSION['id_level']=="2"){
  $warna  = "background: linear-gradient(45deg, #4CAF50 0%, #2196F3 50%, #009688 100%);";
  
}

elseif($_SESSION['id_level']=="3"){
  $warna  = "background: linear-gradient(45deg, #9C27B0 0%, #2196F3 50%, #827f7f 100%);";
  
}

elseif($_SESSION['id_level']=="4"){
  $warna  = "background: linear-gradient(30deg, #363442 0%, #E91E63 50%, #353b4f 100%);";
  
}

elseif($_SESSION['id_level']=="5"){
  $warna  = "background: linear-gradient(45deg, #22cdff 0%, #2196F3 50%, #607D8B 100%);";
  
}
$dada = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' ORDER BY user.id_user DESC"));
?>
<!-- START HEADER -->
<header id="header" class="page-topbar">
  <!-- start header nav-->
  <div class="navbar-fixed">
    <nav class="navbar-color gradient-45deg-light-blue-cyan" style="<?= $warna ?>">
      <div class="nav-wrapper">
        <ul class="left">
          <li>
            <h1 class="logo-wrapper">
              <a href="index.html" class="brand-logo darken-1">
                <img src="../../assets/images/logo/materialize-logo.png" alt="materialize logo">
              </a>
            </h1>
          </li>
        </ul>
        <ul class="right">
          <li>
          <li>
            <?php
            $sa = "SELECT *,`order`.id_meja,HOUR(NOW()) as sekarang,HOUR(`order`.tanggal) as dipesan,HOUR(`transaksi`.tanggal) as detail_pesan FROM `order` JOIN meja ON `order`.`id_meja`=meja.id_meja JOIN transaksi ON `order`.id_order=transaksi.id_order WHERE `order`.id_order ORDER BY `order`.`tanggal` DESC";
            $de = $mysqli->query($sa);
            $y = mysqli_num_rows($de);
          ?>
            <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button"
              data-activates="notifications-dropdown">
              <i class="material-icons">notifications_none
                <small class="notification-badge pink accent-2"
                  style="background-color: #2675c0 !important;"><?= $y ?></small>
              </i>
            </a>
            <ul id="notifications-dropdown" class="dropdown-content"
              style="white-space: nowrap; opacity: 1; left: 951.609px; position: absolute; top: 64px; display: none;">
              <li>
                <h6>Notifikasi
                  <span class="new badge"><?= $y ?></span>
                </h6>
              </li>
              <li class="divider"></li>
              <?php
                while($r = mysqli_fetch_array($de))
              {
                  $meja     = $r['id_meja'];
                  $status   = $r['status_meja'];
                  
                  $dipesan = $r['sekarang']-$r['dipesan'];
                  $kosong = $r['sekarang']-$r['detail_pesan'];
              ?>
              <li>
                <a href="#!" class="grey-text text-darken-2">
                  <?php
                    if ($status == 'O') {
                ?>
                  <span class="material-icons icon-bg-circle cyan small">event_available</span>Meja Nomor <?= $meja ?>
                  Kosong</a>
                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00"><?= $kosong ?> Jam Yang Lalu</time>

                <?php } elseif($status == 'I') { ?>
                <span class="material-icons icon-bg-circle red small">event_busy</span>Meja Nomor <?= $meja ?>
                Dipesan</a>
                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00"><?= $dipesan ?> Jam Yang Lalu</time>
              </li>
              <?php } }?>
            </ul>
          </li>
          </li>
        </ul>
        <div class="header-search-wrapper hide-on-med-and-down">


        </div>
        <ul class="right hide-on-med-and-down">

          <li>
            <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
              <i class="material-icons">settings_overscan</i>
            </a>
          </li>

          <li>
            <?php
              if ($_SESSION['id_level'] == '5') {
            ?>
            <a href="../mf_p_logout_pelanggan.php?id_order=<?= $dada['id_order'] ?>&&id_user=<?= $dada['id_user'] ?>&&id_meja=<?= $dada['id_meja'] ?>"  onClick="return confirm('Apa telah selesai makan ?')" class="waves-effect waves-block waves-heavy">
                <i class="material-icons">keyboard_tab</i></a>
              <?php } else { ?>
              <a href="../mf_p_logout.php" class="waves-effect waves-block waves-heavy"
                onClick="return confirm('Anda ingin Keluar ?')">
                <i class="material-icons">keyboard_tab</i>
                <?php } ?>
              </a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
  <!-- end header nav-->
</header>
<!-- END HEADER -->
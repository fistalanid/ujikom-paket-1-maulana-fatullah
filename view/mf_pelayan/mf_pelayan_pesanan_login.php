<?php
include "../head.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
session_start();

if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_waiter");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../mf_min/mf_min.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">
<title>RST | Pelayan :: <?php echo $_SESSION['username']; ?></title>

<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <?php include "../top_nav.php"; ?>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <?php include "aside.php"; ?>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div class="card-panel">
                        <h4 class="header">Daftar Meja Aktif</h4>
                        <div class="row">
                            <?php
                                $data = "SELECT * FROM user JOIN meja ON user.id_meja=meja.id_meja WHERE meja.status_meja = 'I' AND user.id_level = '5' AND user.status = 'O' ORDER BY `user`.id_meja ASC ";
                                $bacadata = $mysqli->query($data);
                                while($select_result = mysqli_fetch_array($bacadata))
                            {
                                $ww = "SELECT * FROM user WHERE `user`.id_level = '5' ORDER BY `user`.id_meja ASC ";
                                $sasda = $mysqli->query($ww);
                                while($pp = mysqli_fetch_array($sasda));
                                if (mysqli_num_rows($sasda) <= 1) {
                                    error_reporting(0);
                                }
                                $id_meja          = $select_result['id_meja'];
                                $id_user          = $select_result['id_user'];
                                $nama_pelanggan   = $select_result['nama_user'];
                                $username         = $select_result['username'];
                                $password         = $select_result['password'];
                                $no_meja          = $select_result['id_meja'];

                            ?>
                            <div class="col s12 m4 l4">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light">
                                        <div class="col s12 m12 l12" style="height: 35%">
                                            <div class="col s5 m5 l5"></div>
                                            <div class="col s4 m4 l4" style="text-shadow: black 2px 2px 5px;font-size: 40px;color: #ff4081;z-index: 99999;position: relative;margin-top: 38%">
                                                <?php echo $no_meja; ?>
                                            </div>
                                            <div class="col s3 m4 l4"></div>
                                        </div>
                                        <img style="position:absolute;z-index: 1;margin-top:10%" class="activator" src="../../assets/images/gallary/frame.png">
                                    </div>
                                    <div class="card-content">
                                        <span class="card-title activator grey-text text-darken-4">
                                            <?php echo $nama_pelanggan?>
                                        <i class="material-icons right">more_vert</i></span>
                                        <p><a href="#lihat?id_user=<?= $id_user ?>" class="btn-small waves-effect waves-light modal-trigger">Lihat</a></p>
                                    </div>
                                    <div class="card-reveal">
                                        <span class="card-title grey-text text-darken-4">
                                            .<?= $no_meja ?><i class="material-icons right">close</i></span>
                                        <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=nFWWeEdVWrjWQmtm"
                                            method="post">
                                            <div class="input-field" style="margin-top: 5px">
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" value="<?php echo $username; ?>" disabled>
                                                        <label class="active">Username</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" value="<?php echo $password; ?>" disabled>
                                                        <label class="active">Password</label>
                                                    </div>
                                                </div>
                                                <?php
                                                    $apa = $mysqli->query("SELECT * FROM `order` WHERE id_user = '$id_user' AND status = 'O'");
                                                    if(mysqli_num_rows($apa)){
                                                 } else { ?>
                                                    <a href="../../config/mf_min_cancel_order.php?id_user=<?= $id_user ?>&&id_meja=<?= $id_meja ?>" onclick="return confirm('Batalkan Pemesanan ?')" class="btn waves-effect waves-light">Batalkan</a>
                                                 <?php } ?>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=1LzTtCxkaoeNpmEa" method="post"
                                enctype="multipart/form-data">
                                <div id="pesan?id_order=<?php echo $id_order; ?>" class="modal modal-fixed-footer"
                                    style="height: 100%">
                                    <div class="modal-content">
                                        <div id="responsive-table">
                                            <h4 class="header">Data Masakan</h4>
                                            <div class="row section">
                                                <div class="col s12">
                                                    <div class="row section">
                                                        <div class="col s12">
                                                        </div>
                                                        <?php
                                                            $s = "SELECT * from masakan";
                                                            $a = $mysqli->query($s);
                                                            while($c = mysqli_fetch_array($a))
                                                        {
                                                            $id_masakan          = $c['id_masakan'];
                                                            $nama_masakan        = $c['nama_masakan'];
                                                            $harga               = $c['harga'];
                                                            $gambar              = $c['gambar'];
                                                        ?>
                                                        <!-- <div class="col"></div> -->
                                                        <div class="col s12 m6 l6">
                                                            <div class="card">
                                                                <div class="card-image waves-effect waves-block waves-light" style="height: 85%;">
                                                                    <div class="col s12 m12 l12">
                                                                    </div>
                                                                    <img class="activator" src="../../assets/images/masakan/<?php echo $gambar;?>">
                                                                </div>
                                                                <div class="card-content">
                                                                    <span class="card-title activator grey-text text-darken-4">
                                                                        <?php echo $nama_masakan;?><i class="material-icons right">more_vert</i>
                                                                    </span>
                                                                    <p>
                                                                        <?= $harga?>
                                                                        <p>
                                                                </div>
                                                                <div class="card-reveal">
                                                                    <span class="card-title grey-text text-darken-4"><i
                                                                            class="material-icons right">close</i></span>
                                                                    <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=1LzTtCxkaoeNpmEa"
                                                                        method="post">
                                                                        <div class="input-field">
                                                                            <div class="row">
                                                                                <input type="hidden" name="id_order"
                                                                                    value="<?php echo $id_order; ?>">
                                                                                <input type="hidden" name="id_masakan"
                                                                                    value="<?php echo $id_masakan; ?>">
                                                                                <div class="input-field col s12">
                                                                                    <input name="kuantitas" type="text"
                                                                                        class="validate col s8 m4 l5">
                                                                                    <label class="active">Jumlah</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="input-field col s12">
                                                                                    <input name="keterangan" type="text">
                                                                                    <label class="active">Catatan</label>
                                                                                </div>
                                                                            </div>
                                                                            <button class="btn waves-effect waves-light" type="submit" name="action">Pesan</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <?php  } ?>
                        </div>
                    </div>

                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
    <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
<?php 
  include "../koneksi.php";
  include "../cnn.php";
  $user = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM user where username = '$_SESSION[username]'"));
?>
<aside id="left-sidebar-nav">
  <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container" style="transform: translateX(0px);">
    <li class="user-details cyan darken-2">
      <div class="row">
        <div class="col col s4 m4 l4">
          <img src="../../assets/images/avatar/avatar-16.png" alt=""
            class="circle responsive-img valign profile-image cyan">
        </div>
        <div class="col col s8 m8 l8">

          <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#"
            data-activates="profile-dropdown-nav">
            <?php echo $_SESSION['username']; ?>
            <i class="mdi-navigation-arrow-drop-down right"></i>
          </a>
          <ul id="profile-dropdown-nav" class="dropdown-content"
            style="white-space: nowrap; position: absolute; top: 59.0469px; left: 101.234px; display: none; opacity: 1;">
            <li>
              <a href="mf_profil.php" class="grey-text text-darken-1">
                <i class="material-icons">face</i> Profile</a>
            </li>
            <li>
              <a href="mf_help.php" class="grey-text text-darken-1">
                <i class="material-icons">live_help</i> Help</a>
            </li>
            <li>
              <a href="../mf_p_logout.php" class="grey-text text-darken-1"
                onClick="return confirm('Anda ingin Keluar ?')">
                <i class="material-icons">keyboard_tab</i> Logout</a>
            </li>
          </ul>
          <p class="user-roal">Pelayan</p>
        </div>
      </div>
    </li>
    <li class="no-padding">
      <ul class="collapsible" data-collapsible="accordion">
        <li class="bold">
          <a href="mf_pelayan.php" class="waves-effect waves-cyan">
            <i class="material-icons">pie_chart_outlined</i>
            <span class="nav-text">Dashboard</span>
          </a>
        </li>
        <li class="bold">
          <a href="mf_pelayan_masakan.php" class="waves-effect waves-cyan">
            <i class="material-icons">restaurant_menu</i>
            <span class="nav-text">Masakan</span>
          </a>
        </li>
        <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        <li class="bold">
          <a class="waves-effect waves-cyan dropdown-trigger" data-activates="transaksi">
            <i class="material-icons">done_all</i>
            <span class="nav-text">Transaksi</span>
          </a>
        </li>
        <ul id="transaksi" class="dropdown-content" tabindex="0">
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_pelayan_semua_transaksi.php">
              <i class="material-icons">done_all</i>Semua Transaksi</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_pelayan_transaksi_harian.php">
              <i class="material-icons">today</i>Transaksi Harian</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_pelayan_transaksi_mingguan.php">
              <i class="material-icons">event</i>Transaksi Mingguan</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_pelayan_transaksi_bulanan.php">
              <i class="material-icons">date_range</i>Transaksi Bulanan</a>
          </li>
        </ul>
        <li class="bold">
          <a class="waves-effect waves-cyan dropdown-trigger" data-activates="user">
            <i class="material-icons">note_add</i>
            <span class="nav-text">Pesanan</span>
          </a>
        </li>

        <ul id="user" class="dropdown-content" tabindex="0">
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_pelayan_pesanan_login.php">
              <i class="material-icons">note</i>Pesan Login</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_pelayan_pesanan_belum_dibayar.php">
              <i class="material-icons">note</i>Pesanan Belum Dibayar</a>
          </li>
        </ul>
        <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      </ul>
    </li>
    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
      <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
      <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
    </div>
  </ul>
  <a href="#" data-activates="slide-out"
    class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
    <i class="material-icons">menu</i>
  </a>
</aside>

<!--- Modal --->
<!-- proses tambah meja f3yairPJbq9c7ti0 -->
<!-- aksi di mf_min_proc FpE46vHa3RKhw9N4 -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=f3yairPJbq9c7ti0" method="post"
  enctype="multipart/form-data">
  <div id="modal" class="modal modal-fixed-footer" style="height:65%">
    <div class="modal-content">
      <h6 class="header">Daftarkan Pelanggan</h6>
      <?php
          
          $data = "SELECT * FROM user WHERE id_user = '$user[id_user]'";
          $bacadata = $mysqli->query($data);
          while($select_result = mysqli_fetch_array($bacadata)){
            $id_user = $select_result['id_user'];
        ?>
      <input name="id_user" type="hidden" value="<?php echo $id_user ?>" required="">
      <div class="input-field col s6">
        <i class="material-icons prefix">account_circle</i>
        <input name="nama_pelanggan" id="icon_prefix" type="text" class="validate" required>
        <label for="icon_prefix">Nama Pelanggan</label>
      </div>
      <div class="input-field col s6">
        <i class="material-icons prefix">note</i>
        <textarea name="keterangan" id="icon_prefix2" class="materialize-textarea" required></textarea>
        <label for="icon_prefix2">Catatan</label>
      </div>
      <?php } ?>
      <div class="container"><label>Pilih Meja</label><br>
        <?php
              $a = "SELECT * FROM meja WHERE status_meja = 'O'";
              $p = $mysqli->query($a);
              while($c = mysqli_fetch_array($p))
              {
                $no_meja = $c['no_meja'];
            ?>
        <label>
          <input class="with-gap " name="no_meja" type="radio" value="<?php echo $no_meja ?>" required />
          <span>
            <?php echo $no_meja ?></span>
        </label>
        <?php } ?>
      </div>
    </div>

    <div class="modal-footer">
      <button type="submit" class="waves-effect waves-light btn modal-trigger"
        style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Pesan</button>
    </div>
  </div>
</form>
<!-- Modal -->
<div class="row">
  <div class="col s10 m112 l11"></div>
  <a href="#keranjang" class="btn-floating modal-trigger" style="position: fixed;z-index: 999999;top: 90%"><i
      class="material-icons">shopping_cart</i></a>
</div>

<!--- Keranjang --->
<!-- proses memindahkan masakan ke detail_order -->
<form action="../../config/mf_min_to_detail.php" method="post" enctype="multipart/form-data">
  <div id="keranjang" class="modal modal-fixed-footer">
    <?php
      $sssa = mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'X'");
      $a = mysqli_fetch_array($sssa);
      if(mysqli_num_rows($sssa)){
    ?>
    <a href='../../config/mf_min_cancel_pelanggan.php?id_order=<?= $a['id_order'] ?>&&id_meja=<?= $a['id_meja'] ?>'
      onClick='return confirm("Batalkan pelanggan ?")'><i class='material-icons btn-floating'
        style='float:right;padding:10px;border-radius:1px'>delete_forever</i></a>
    <?php } ?>


    <div class="modal-content">
      <h6 style="font-weight:bold">Keranjang Masakan</h6>
      <div class="row">
        <div class="input-field col s12 m6 l5">
          <blockquote>
            <?php echo $a['nama_pelanggan']; ?>
          </blockquote>
          <label class="active">Nama Pelanggan</label>
        </div>
        <blockquote style="float:right;margin-top:4%">
          <input type="hidden" name="id_order" value="<?php echo $a['id_order']; ?>">
          <?php echo $a['id_order']; ?>
        </blockquote>
        <label class="active" style="float:right;margin-right:-4%">ID Order</label>
      </div>
      <table class="responsive-table centered Highlight striped bordered">
        <thead>
          <tr>
            <th style="text-align:center;">Masakan</th>
            <th width="1%" style="text-align:center;">Kuantitas</th>
            <th style="text-align:center;">Satuan</th>
            <th style="text-align:center;">Jumlah</th>
            <th style="text-align:center;">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
                
                $data = "SELECT *,`masakan`.`id_masakan`,`masakan`.`nama_masakan`,SUM(`cart`.`kuantitas`) as qty,`cart`.`harga`,SUM(`cart`.`total_harga`) as t_harga,`cart`.`keterangan`,`cart`.`id_cart`,`cart`.`status_cart`,`cart`.`id_user` FROM `cart` JOIN `masakan` ON `cart`.`id_masakan`=`masakan`.`id_masakan` WHERE `cart`.`id_user` = '$user[id_user]' AND `cart`.`status_cart` = 'I' GROUP BY `cart`.`id_masakan` ";
                $bacadata = $mysqli->query($data);
                while($select_result = mysqli_fetch_array($bacadata))
                {
                $nama_masakan          = $select_result['nama_masakan'];
                $id_masakan            = $select_result['id_masakan'];
                $id_cart               = $select_result['id_cart'];
                $id_user               = $select_result['id_user'];
                $kuantitas             = $select_result['qty'];
                $harga                 = $select_result['harga'];
                $total_harga           = $select_result['t_harga'];
                $status                = $select_result['status_cart'];
                $keterangan            = $select_result['keterangan'];
            ?>
          <tr>
            <td>
              <input type="hidden" name="id_masakan[]" value="<?php echo $id_masakan ?>">
              <?= $nama_masakan; ?>
            </td>
            <td>
              <input type="hidden" name="kuantitas[]" value="<?php echo $kuantitas ?>">
              <?php echo $kuantitas ?>
            </td>
            <input type="hidden" name="keterangan[]" value="<?php echo $keterangan ?>">
            <input type="hidden" name="id_user" value="<?php echo $id_user ?>">
            <td>
              <?= "Rp.".number_format($harga) ?>
            </td>
            <td>
              <input type="hidden" name="total_harga" value="<?php echo $total_harga ?>">
              <?= "Rp.".number_format($total_harga) ?>
            </td>
            <td>
              <a href="../../config/mf_min_cancel_cart.php?id_user=<?php echo $user['id_user'] ?>&&id_masakan=<?php echo $id_masakan ?>"
                class="btn-floating" onClick="return confirm('Hapus <?= $nama_masakan ?> ?')"><i class="material-icons">delete</i></a> |
              <a href="#detail_cart?id_user=<?php echo $user['id_user'] ?>&&id_masakan=<?php echo $id_masakan ?>&&status_cart=<?php echo $status ?>"
                class="btn modal-trigger">Rincian</a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="row">
        <div class="input-field col ">
          <blockquote>
            <?php echo $a['keterangan']; ?>
          </blockquote>
          <label class="active">Catatan</label>
        </div>
      </div>
    </div>
    <div class="modal-footer">

      <label style="float:left;padding-right:15px">Nomor Meja</label>
      <label style="float:left">
        <input class="with-gap " name="id_meja" type="radio" value="<?php echo $a['id_meja']; ?>" checked />
        <span>
          <?php echo $a['id_meja']; ?></span>
      </label>
      <?php
          $sssa = mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'X'");
          $a = mysqli_fetch_array($sssa);
          if(mysqli_num_rows($sssa)){
        ?>
        <a href='#ubah_pelanggan?id_order=<?= $a['id_order']; ?>' class='waves-effect waves-light btn modal-trigger'
        style='margin-right:10px'>Edit</a>
      <?php }
      $data = "SELECT * FROM cart JOIN masakan ON cart.id_masakan=masakan.id_masakan WHERE cart.id_user = '$user[id_user]' AND status_cart = 'I' GROUP BY cart.id_masakan ";
      $bacadata = $mysqli->query($data);
      if(mysqli_num_rows($bacadata)){
        echo "
        
        <button type='submit' class='waves-effect waves-light btn modal-trigger' style='background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%);float:right;'>Konfirmasi</button>";
      } else {


      }
        
      ?>
    </div>
  </div>
</form>
<!-- Keranjang -->
<?php
    $saya = "SELECT * from cart";
    $baca_cart = $mysqli->query($saya);
    while($cart_saya = mysqli_fetch_array($baca_cart)){
      $id_masakan_gan = $cart_saya['id_masakan'];
      $data = "SELECT * FROM cart JOIN masakan ON cart.id_masakan=masakan.id_masakan WHERE cart.id_user = '$user[id_user]' AND cart.status_cart = 'I' AND cart.id_masakan = '$id_masakan_gan'";
        $bacadata = $mysqli->query($data);
        while($select_result = mysqli_fetch_array($bacadata));
  ?>
<!--- Modal --->
<!-- detail dan edit makanan di keranjang aH6pwyxErE6wiKyt2 -->
<!-- aksi di mf_min_proc FpE46vHa3RKhw9N4 -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=aH6pwyxErE6wiKyt2" method="post"
  enctype="multipart/form-data">
  <div id="detail_cart?id_user=<?php echo $user['id_user'] ?>&&id_masakan=<?php echo $id_masakan_gan ?>&&status_cart=<?php echo $status ?>"
    class="modal modal-fixed-footer">
    <div class="modal-content">
      <h6 class="header" style="font-weight:bold">Detail Masakan</h6>
      <?php
            $a = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'X'"));        
          ?>

      <table class="responsive-table centered Highlight striped bordered">
        <thead>
          <tr>
            <th style="text-align:center;">Masakan</th>
            <th width="1%" style="text-align:center;">Kuantitas</th>
            <th style="text-align:center;">Satuan</th>
            <th style="text-align:center;">Catatan</th>
            <th style="text-align:center;">Opsi</th>
          </tr>
        </thead>
        <tbody>
          <?php
            
            $data = "SELECT masakan.id_masakan,masakan.nama_masakan,cart.kuantitas as qty,cart.harga,cart.total_harga as t_harga,cart.keterangan,cart.id_cart,cart.status_cart FROM cart JOIN masakan ON cart.id_masakan=masakan.id_masakan WHERE cart.id_user = '$user[id_user]' AND cart.status_cart = 'I' AND cart.id_masakan = '$id_masakan_gan' ORDER BY masakan.nama_masakan ASC";
            $bacadata = $mysqli->query($data);
            while($select_result = mysqli_fetch_array($bacadata))
            {
            $nama_masakan          = $select_result['nama_masakan'];
            $id_masakan            = $select_result['id_masakan'];
            $id_cart               = $select_result['id_cart'];
            $kuantitas             = $select_result['qty'];
            $harga                 = $select_result['harga'];
            $total_harga           = $select_result['t_harga'];
            $status                = $select_result['status_cart'];
            $keterangan            = $select_result['keterangan'];
        ?>
          <tr>
            <td>
              <input type="hidden" name="id_masakan" value="<?php echo $id_masakan ?>">
              <?= $nama_masakan; ?>
            </td>
            <td>
              <input type="hidden" name="kuantitas" value="<?php echo $kuantitas ?>">
              <?php echo $kuantitas ?>
            </td>
            <td>
              <?= "Rp.".number_format($harga) ?>
            </td>
            <td>
              <input type="hidden" name="total_harga" value="<?php echo $keterangan ?>">
              <?= $keterangan; ?>
            </td>
            <td>
              <a href="../../config/mf_min_cancel_cart_detail.php?id_cart=<?php echo $id_cart ?>"
                class="btn-floating"><i class="material-icons">delete</i></a> |
              <a href="#edit_cart?id_cart=<?php echo $id_cart ?>&&id_user=<?php echo $user['id_user'] ?>&&id_masakan=<?php echo $id_masakan ?>&&status=<?php echo $status ?>"
                class="btn modal-trigger">Ubah</a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <label style="float:left;padding-right:15px">Nomor Meja</label>
      <label style="float:left">
        <input class="with-gap " name="id_meja" type="radio" value="<?php echo $a['id_meja']; ?>" checked />
        <span>
          <?php echo $a['id_meja']; ?></span>
      </label>
    </div>
  </div>
</form>
<!-- Modal -->
<?php }
    $ce = "SELECT * from cart JOIN user JOIN masakan WHERE user.username = '$_SESSION[username]'";
    $edit_cart = $mysqli->query($ce);
      while($cart_saya_gan = mysqli_fetch_array($edit_cart))
    {
      $id_masakan_gan = $cart_saya_gan['id_masakan'];
      $nama_masakan_gan = $cart_saya_gan['nama_masakan'];
      $id_cart_gan = $cart_saya_gan['id_cart'];
      $kuantitas_gan = $cart_saya_gan['kuantitas'];
      $id_user_gan = $cart_saya_gan['id_user'];
      $keterangan = $cart_saya_gan['keterangan'];
      
  ?>
<!-- proses edit cart cH4vbnPjxWVnuaad -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=cH4vbnPjxWVnuaad" method="post"
  enctype="multipart/form-data">
  <div
    id="edit_cart?id_cart=<?php echo $id_cart_gan ?>&&id_user=<?php echo $id_user_gan ?>&&id_masakan=<?php echo $id_masakan_gan ?>&&status=<?php echo $status ?>"
    class="modal modal-fixed-footer" style="height:65%">
    <div class="modal-content">
      <h6 class="header" style="font-weight:bold">Ubah Catatan dan Kuantitas</h6>
      <div class="row">
        <div class="input-field col s12 m6 l6">
          <blockquote>
            <?php echo $nama_masakan_gan ?>
          </blockquote>
          <label class="active">Masakan</label>
        </div>
        <div class="row">
          <div class="input-field col s3" style="float:right">
            <i class="material-icons prefix">list</i>
            <input name="kuantitas" type="number" value="<?= $kuantitas_gan?>">
            <label for="icon_prefix2">Kuantitas</label>
          </div>
        </div>
      </div>
      <?php
          
          $data = "SELECT * FROM user WHERE id_user = '$user[id_user]'";
          $bacadata = $mysqli->query($data);
          while($select_result = mysqli_fetch_array($bacadata))
      {
            $id_user = $select_result['id_user'];
        ?>
      <input name="id_user" type="hidden" value="<?= $id_user ?>">
      <input name="id_cart" type="hidden" value="<?= $id_cart_gan?>">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">note</i>
          <textarea name="keterangan" id="icon_prefix2" class="materialize-textarea"
            required><?= $keterangan ?></textarea>
          <label for="icon_prefix2">Catatan</label>
        </div>
      </div>
      <?php } ?>

    </div>

    <div class="modal-footer">
      <button type="submit" class="waves-effect waves-light btn modal-trigger"
        style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Ubah</button>
    </div>
  </div>
</form>
<?php } ?>

<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- Modal -->
<?php
    $ce = "SELECT * from `order`";
    $edit_pelanggan = $mysqli->query($ce);
      while($pelanggan = mysqli_fetch_array($edit_pelanggan))
    {
      $id_order_lo       = $pelanggan['id_order'];
      $nama_pelanggan_lo = $pelanggan['nama_pelanggan'];
      $catatan_lo        = $pelanggan['keterangan'];
      $id_meja_lo        = $pelanggan['id_meja'];
      
  ?>
<!-- proses edit pelanggan NPfGFnbGzuuMsDtx -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=NPfGFnbGzuuMsDtx" method="post"
  enctype="multipart/form-data">
  <div id="ubah_pelanggan?id_order=<?php echo $id_order_lo ?>" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h6 class="header" style="font-weight:bold">Ubah Catatan dan Kuantitas</h6>
      <div class="row">
        <div class="input-field col s12 m6 l6">
          <blockquote>
            <input name="nama_pelanggan" type="text" value="<?= $nama_pelanggan_lo ?>">
          </blockquote>
          <label class="active">Nama Pelanggan</label>
        </div>
        <div class="row">
          <blockquote style="float:right;margin-top:4%">
            <input type="hidden" name="id_order" value="<?= $id_order_lo ?>">

            <input name="id_meja" type="hidden" value="<?php echo $id_meja_lo ?>" />
            <?= $id_order_lo ?>
          </blockquote>
          <label class="active" style="float:right;margin-right:-4%">ID Order</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">note</i>
          <textarea name="keterangan" id="icon_prefix2" class="materialize-textarea"
            required><?= $catatan_lo ?></textarea>
          <label for="icon_prefix2">Catatan</label>
        </div>
      </div>
      <div class="container"><label>Pilih Meja</label><br>
        <input class="with-gap" name="id_meja_lama" type="radio" value="<?php echo $id_meja_lo ?>" required checked />

        <span><?php echo $id_meja_lo ?></span>
        <?php
              $a = "SELECT * FROM meja WHERE status_meja = 'O'";
              $p = $mysqli->query($a);
              while($c = mysqli_fetch_array($p))
              {
                $no_meja = $c['no_meja'];
            ?>
        <label>
          <input class="with-gap " name="id_meja" type="radio" value="<?php echo $no_meja ?>" />
          <span><?php echo $no_meja ?></span>
        </label>
        <?php } ?>
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="waves-effect waves-light btn"
        style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Ubah</button>
    </div>
  </div>
</form>
<?php } 
  $fff = "SELECT *,`order`.`id_meja`,`order`.`id_user` FROM `meja` JOIN `order` ON `meja`.id_meja=`order`.id_meja JOIN user ON user.id_user=`order`.id_user ORDER BY `order`.id_meja ASC";
  $keranjang = $mysqli->query($fff);
    while($keranjang_ane = mysqli_fetch_array($keranjang)){
      $id_meja_ane  = $keranjang_ane['id_meja'];
      $id_user_ane  = $keranjang_ane['id_user'];
      $id_order_ane = $keranjang_ane['id_order'];
?>

<!--- Keranjang Pelanggan --->
<!-- proses memindahkan masakan ke detail_order -->
<form action="../../config/mf_min_to_detail.php" method="post" enctype="multipart/form-data">
  <div id="keranjang_pelanggan?id_order=<?= $id_order_ane ?>" class="modal">
    <?php
          $sssa = mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'X'");
          $a = mysqli_fetch_array($sssa);
          if(mysqli_num_rows($sssa)){
        ?>
    <a href='../../config/mf_min_cancel_pelanggan.php?id_order=<?= $a['id_order'] ?>&&id_meja=<?= $a['id_meja'] ?>'
      onClick='return confirm("Batalkan pelanggan ?")'><i class='material-icons btn-floating'
        style='float:right;padding:10px;border-radius:1px'>delete_forever</i></a>
    <?php } ?>
    <div class="modal-content">
      <h6 style="font-weight:bold">Pesanan Pelanggan</h6>
      <table class="responsive-table centered Highlight striped bordered">
        <thead>
          <tr>
            <th style="text-align:center;">Masakan</th>
            <th width="1%" style="text-align:center;">Kuantitas</th>
            <th style="text-align:center;">Satuan</th>
            <th style="text-align:center;">Jumlah</th>
            <th style="text-align:center;">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
                $total_semua = 0 ;          
                $data = "SELECT * FROM `detail_order` JOIN `masakan` ON `detail_order`.`id_masakan`=`masakan`.`id_masakan` WHERE detail_order.id_order = '$id_order_ane'";
                $bacadata = $mysqli->query($data);
                while($select_result = mysqli_fetch_array($bacadata))
                {
                $id_masakan            = $select_result['id_masakan'];
                $id_order              = $select_result['id_order'];
                $id_detail_order       = $select_result['id_detail_order'];
                $nama_masakan          = $select_result['nama_masakan'];
                $kuantitas             = $select_result['kuantitas'];
                $harga                 = $select_result['harga'];
                $total_harga           = $select_result['kuantitas']*$select_result['harga'];
                $total_semua           += ($total_harga);

            ?>
          <tr>
            <td>
              <input type="hidden" name="id_masakan[]" value="<?php echo $id_masakan ?>">
              <?= $nama_masakan; ?>
            </td>
            <td>
              <input type="hidden" name="kuantitas[]" value="<?php echo $kuantitas ?>">
              <?php echo $kuantitas ?>
            </td>
            <input type="hidden" name="keterangan[]" value="<?php echo $keterangan ?>">
            <input type="hidden" name="id_user" value="<?php echo $id_user ?>">
            <td>
              <?= "Rp.".number_format($harga) ?>
            </td>
            <td>
              <input type="hidden" name="total_harga" value="<?php echo $total_harga ?>">
              <?= "Rp.".number_format($total_harga) ?>
            </td>
            <td>
              <?php
                $p_proses = $mysqli->query("SELECT * FROM detail_order WHERE id_detail_order = '$id_detail_order'");
                $ppp = mysqli_fetch_array($p_proses);
                if ($ppp['status_detail_order'] == 'Diproses') {
              ?>
              
                <a href="../../config/mf_min_update_status_detail_selesai.php?id_detail_order=<?= $id_detail_order ?>" onClick="return confirm('<?php echo $kuantitas ," " ,$nama_masakan; ?> telah siap ?')" class="btn modal-trigger">Diproses</a>
                |
              <a href="#edit_detail?id_detail_order=<?php echo $id_detail_order ?>" class="btn modal-trigger">Ubah</a>
              <?php } elseif ($ppp['status_detail_order'] == 'Selesai') { ?>
                <a class="btn" style="background-color: #00adff;" readonly>Selesai</a>
              <?php } ?>
            </td>

          </tr>
          <?php } ?>
          <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Total : RP.<?= number_format($total_semua) ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</form>
  <?php } ?>
<!-- Keranjang Pelanggan -->

<style>
.lebar{
  width :30%;
}
.tengah{
  text-align:center;
}
</style>
<?php
    $ff = "SELECT * from user";
    $asa = $mysqli->query($ff);
    while($rr = mysqli_fetch_array($asa))
  {
    $id_user  = $rr['id_user'];
    $username = $rr['username'];
    $password = $rr['password'];
?>
<!--- Lihat Pelanggan --->
<!-- proses memindahkan masakan ke detail_order -->
<form action="../../config/mf_min_to_detail.php" method="post" enctype="multipart/form-data">
  <div id="lihat?id_user=<?= $id_user ?>" class="modal lebar">
    <div class="modal-content">
      <h6 style="font-weight:bold;" class="tengah">Detail Pelanggan</h6>
      <br>
      <p class="tengah">Username</p>
      <h5 class="tengah"><?= $username ?></h5>
      <li class="divider" tabindex="-1"></li>
      <p class="tengah">Password</p>
      <h5 class="tengah"><?= $password ?></h5>
    </div>
  </div>
</form>
<!-- Lihat Pelanggan -->
  <?php }
    $rsa = "SELECT * from detail_order";
    $rtt = $mysqli->query($rsa);
    while($ggff = mysqli_fetch_array($rtt)){
      $id_masakan_gan        = $ggff['id_masakan'];
      $id_order_gan          = $ggff['id_order'];
      $id_detail_order_gan   = $ggff['id_detail_order'];
  ?>
<!--- Modal --->
<!-- detail dan edit makanan di keranjang aH6pwyxErE6wiKyt2 -->
<!-- aksi di mf_min_proc FpE46vHa3RKhw9N4 -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=aH6pwyxErE6wiKyt2" method="post"
  enctype="multipart/form-data">
  <div id="detail_cart_pelanggan?id_detail_order=<?= $id_detail_order_gan ?>"
    class="modal">
    <div class="modal-content">
      <h6 class="header" style="font-weight:bold">Detail Pesanan</h6>
      <?php
            $a = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'X'"));        
          ?>

      <table class="responsive-table centered Highlight striped bordered">
        <thead>
          <tr>
            <th style="text-align:center;">Masakan</th>
            <th width="1%" style="text-align:center;">Kuantitas</th>
            <th style="text-align:center;">Satuan</th>
            <th style="text-align:center;">Catatan</th>
            <th style="text-align:center;">Catatan</th>
          </tr>
        </thead>
        <tbody>
          <?php
            
            $data = "SELECT * FROM detail_order JOIN masakan ON detail_order.id_masakan=masakan.id_masakan WHERE detail_order.id_detail_order = '$id_detail_order_gan' ORDER BY masakan.nama_masakan ASC";
            $bacadata = $mysqli->query($data);
            while($select_result = mysqli_fetch_array($bacadata))
            {

            $id_detail_order       = $select_result['id_detail_order'];
            $id_masakan            = $select_result['id_masakan'];
            $nama_masakan          = $select_result['nama_masakan'];
            $kuantitas_gan         = $select_result['kuantitas'];
            $harga                 = $select_result['harga'];
            $keterangan            = $select_result['keterangan'];
            $status                = $select_result['status_detail_order'];
        ?>
          <tr>
            <td>
              <input type="hidden" name="id_masakan" value="<?php echo $id_masakan ?>">
              <?= $nama_masakan; ?>
            </td>
            <td>
              <input type="hidden" name="kuantitas" value="<?php echo $kuantitas_gan ?>">
              <?php echo $kuantitas_gan ?>
            </td>
            <td>
              <?= "Rp.".number_format($harga) ?>
            </td>
            <td>
              <input type="hidden" name="total_harga" value="<?php echo $keterangan ?>">
              <?= $keterangan; ?>
            </td>
            <td>
              <?php
                $p_proses = $mysqli->query("SELECT * FROM detail_order WHERE id_detail_order = '$id_detail_order'");
                $ppp = mysqli_fetch_array($p_proses);
                if ($ppp['status_detail_order'] == 'Diproses') {
              ?>
                <a href="../../config/mf_min_update_status_detail_selesai.php?id_detail_order=<?= $id_detail_order ?>" class="btn modal-trigger">Diproses</a>
              <?php } elseif ($ppp['status_detail_order'] == 'Selesai') { ?>
                <a href="../../config/mf_min_update_status_detail_diproses.php?id_detail_order=<?= $id_detail_order ?>" class="btn modal-trigger">Selesai</a>
              <?php } ?>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</form>
<?php }
    $asp = "SELECT * from detail_order JOIN user JOIN masakan ON masakan.id_masakan=detail_order.id_masakan WHERE user.username = '$_SESSION[username]'";
    $edit_detail = $mysqli->query($asp);
      while($detail_assas = mysqli_fetch_array($edit_detail))
    {
      $id_masakan_gan = $detail_assas['id_masakan'];
      $id_detail_order = $detail_assas['id_detail_order'];
      $nama_masakan_gan = $detail_assas['nama_masakan'];
      $kuantitas_gan = $detail_assas['kuantitas'];
      $id_user_gan = $detail_assas['id_user'];
      $keterangan = $detail_assas['keterangan'];
      
  ?>

<!-- proses edit detail fz3Wb1GLiTd3rDq0 -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=fz3Wb1GLiTd3rDq0" method="post" enctype="multipart/form-data">
  <div id="edit_detail?id_detail_order=<?php echo $id_detail_order ?>"
    class="modal modal-fixed-footer" style="height:65%">
    <div class="modal-content">
      <h6 class="header">Ubah Catatan dan Kuantitas</h6>
      <div class="row">
        <div class="input-field col s12 m6 l6">
          <blockquote>
            <?php echo $nama_masakan_gan ?>
          </blockquote>
          <label class="active">Masakan</label>
        </div>
        <div class="row">
          <div class="input-field col s3" style="float:right">
            <i class="material-icons prefix">list</i>
            <input name="kuantitas" type="number" value="<?= $kuantitas_gan?>">
            <label for="icon_prefix2">Kuantitas</label>
          </div>
        </div>
      </div>
      <?php
          $user = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM user where username = '$_SESSION[username]'"));
          $data = "SELECT * FROM user WHERE id_user = '$user[id_user]'";
          $bacadata = $mysqli->query($data);
          while($select_result = mysqli_fetch_array($bacadata))
      {
            $id_user = $select_result['id_user'];
        ?>
      <input name="id_detail_order" type="hidden" value="<?= $id_detail_order ?>">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">note</i>
          <textarea name="keterangan" id="icon_prefix2" class="materialize-textarea" required><?= $keterangan ?></textarea>
          <label for="icon_prefix2">Catatan</label>
        </div>
      </div>
      <?php } ?>

    </div>

    <div class="modal-footer">
      <button type="submit" class="waves-effect waves-light btn modal-trigger" style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Ubah</button>
    </div>
  </div>
</form>
<?php } ?>
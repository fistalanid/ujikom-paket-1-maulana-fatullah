<?php
include "../head.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
session_start();

if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_waiter");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../mf_min/mf_min.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">
<title>RST | Pelayan :: <?php echo $_SESSION['username']; ?></title>

<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <?php include "../top_nav.php"; ?>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <?php include "aside.php"; ?>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div class="card-panel">
                        <h4 class="header">Meja Pesanan Belum Dibayar</h4>
                        <div class="row">
                            <?php
                                $data = "SELECT *,`order`.`id_meja`,`order`.`id_user` FROM `meja` JOIN `order` ON `meja`.id_meja=`order`.id_meja JOIN user ON user.id_user=`order`.id_user WHERE NOT `order`.status = 'V' AND user.status = 'O' ORDER BY `order`.id_meja ASC";
                                $bacadata = $mysqli->query($data);
                                while($select_result = mysqli_fetch_array($bacadata))
                            {
                                $id_meja          = $select_result['id_meja'];
                                $id_user          = $select_result['id_user'];
                                $id_order         = $select_result['id_order'];
                                $nama_pelanggan   = $select_result['nama_pelanggan'];
                                $status_meja      = $select_result['status_meja'];
                                $no_meja          = $select_result['no_meja'];
                                $status_order     = $select_result['status_order'];
                                $keterangan       = $select_result['keterangan'];
                                $tanggal          = $select_result['tanggal'];
                            ?>
                            <div class="col s12 m4 l4">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light">
                                        <div class="col s12 m12 l12" style="height: 35%">
                                            <div class="col s5 m5 l5"></div>
                                            <div class="col s4 m4 l4" style="text-shadow: black 2px 2px 5px;font-size: 40px;color: #ff4081;z-index: 99999;position: relative;margin-top: 38%">
                                                <?php echo $no_meja; ?>
                                            </div>
                                            <div class="col s3 m4 l4"></div>
                                        </div>
                                        <img style="position:absolute;z-index: 1;margin-top:10%" class="activator" src="../../assets/images/gallary/frame.png">
                                    </div>
                                    <div class="card-content">
                                        <span class="card-title activator grey-text text-darken-4">
                                            <?php echo $nama_pelanggan?><i class="material-icons right">more_vert</i></span>
                                        
                                            <?php
                                                $p_proses = $mysqli->query("SELECT * FROM detail_order WHERE id_order = '$id_order' AND status_detail_order = 'Diproses' ");
                                                $ppp = mysqli_fetch_array($p_proses);
                                                if ($ppp['status_detail_order'] == 'Diproses') {
                                            ?>
                                        <p><a href="../../config/mf_min_menu_ready.php?id_order=<?= $id_order ?>" class="btn waves-effect waves-light modal-trigger" onclick="return confirm('Sajikan Semua Menu ?')" style="background-color: #00adff;">Siap Saji</a></p>
                                            <?php } else {  } ?>

                                    </div>
                                    <div class="card-reveal">
                                        <span class="card-title grey-text text-darken-4">
                                            <?php echo $status_order ?><i class="material-icons right">close</i></span>
                                        <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=nFWWeEdVWrjWQmtm"
                                            method="post">
                                            <div class="input-field" style="margin-top: 5px">
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" class="validate col s12 m12 l12" value="<?php echo $tanggal;?>"
                                                            disabled>
                                                        <label class="active">Tanggal Pesanan</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" value="<?php echo $keterangan; ?>" disabled>
                                                        <label class="active">Catatan</label>
                                                    </div>
                                                </div>
                                                <a href="#keranjang_pelanggan?id_order=<?= $id_order ?>" class="btn waves-effect waves-light modal-trigger">Pesanan</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=1LzTtCxkaoeNpmEa" method="post"
                                enctype="multipart/form-data">
                                <div id="pesan?id_order=<?php echo $id_order; ?>" class="modal modal-fixed-footer"
                                    style="height: 100%">
                                    <div class="modal-content">
                                        <div id="responsive-table">
                                            <h4 class="header">Data Masakan</h4>
                                            <div class="row section">
                                                <div class="col s12">
                                                    <div class="row section">
                                                        <div class="col s12">
                                                        </div>
                                                        <?php
                                                            $s = "SELECT * from masakan";
                                                            $a = $mysqli->query($s);
                                                            while($c = mysqli_fetch_array($a))
                                                        {
                                                            $id_masakan          = $c['id_masakan'];
                                                            $nama_masakan        = $c['nama_masakan'];
                                                            $harga               = $c['harga'];
                                                            $gambar              = $c['gambar'];
                                                        ?>
                                                        <!-- <div class="col"></div> -->
                                                        <div class="col s12 m6 l6">
                                                            <div class="card">
                                                                <div class="card-image waves-effect waves-block waves-light" style="height: 85%;">
                                                                    <div class="col s12 m12 l12">
                                                                    </div>
                                                                    <img class="activator" src="../../assets/images/masakan/<?php echo $gambar;?>">
                                                                </div>
                                                                <div class="card-content">
                                                                    <span class="card-title activator grey-text text-darken-4">
                                                                        <?php echo $nama_masakan;?><i class="material-icons right">more_vert</i>
                                                                    </span>
                                                                    <p>
                                                                        <?= $harga?>
                                                                        <p>
                                                                </div>
                                                                <div class="card-reveal">
                                                                    <span class="card-title grey-text text-darken-4"><i
                                                                            class="material-icons right">close</i></span>
                                                                    <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=1LzTtCxkaoeNpmEa"
                                                                        method="post">
                                                                        <div class="input-field">
                                                                            <div class="row">
                                                                                <input type="hidden" name="id_order"
                                                                                    value="<?php echo $id_order; ?>">
                                                                                <input type="hidden" name="id_masakan"
                                                                                    value="<?php echo $id_masakan; ?>">
                                                                                <div class="input-field col s12">
                                                                                    <input name="kuantitas" type="text"
                                                                                        class="validate col s8 m4 l5">
                                                                                    <label class="active">Jumlah</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="input-field col s12">
                                                                                    <input name="keterangan" type="text">
                                                                                    <label class="active">Catatan</label>
                                                                                </div>
                                                                            </div>
                                                                            <button class="btn waves-effect waves-light" type="submit" name="action">Pesan</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                        </div>
                    </div>

                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
    <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
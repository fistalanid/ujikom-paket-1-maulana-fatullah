<?php
include "../head.php";
include "../koneksi.php";
include "../conn.php";
include "../cnn.php";
include "../../database/database.php";
$db = new database();
session_start();

if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_waiter");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../mf_min/mf_min.php");
}

elseif($_SESSION['id_level']=="2"){
    header("location:../mf_pelayan/mf_pelayan.php");
  }

  elseif($_SESSION['id_level']=="3"){
    header("location:../mf_kasir/mf_kasir.php");
  }
    
elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}
$dada = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' "));
$user = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM user where username = '$_SESSION[username]'"));


$sssa = mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'O' AND user.status = 'O' ");
  $ada = mysqli_fetch_array($sssa);
    $app = $mysqli->query("SELECT * FROM user WHERE id_user = '$ada[id_user]' AND status = 'O' ");
            if (mysqli_num_rows($app)) {
?>
<html lang="en">
<title>RST | Pelanggan
</title>

<body class="loaded">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START HEADER -->

  <?php include "../top_nav.php"; ?>

  <!-- END HEADER -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START MAIN -->
  <!-- START WRAPPER -->
  <div class="wrapper">
    <!-- START LEFT SIDEBAR NAV-->
    <div class="row">
      <div class="col s10 m12 l11"></div>
    </div>
    <!-- END LEFT SIDEBAR NAV-->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTENT -->
    <section id="content">
      <!--start container-->
      <div class="container">
        <!--card stats start-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <div id="card-widgets">
          <section id="content">
            <!--start container-->
            <div class="container">
              <div class="section">
                <div class="row">
                  <div class="col-md-4 col-sm-4">
                    <div class="card-panel">
                      <!--Responsive Table-->
                      <div id="responsive-table">
                        <h6 class="header" style="float:left">Data Masakan</h6>
                        <div class="row section">
                          <div class="col s12">
                            <div class="col s12 m6 l6">
                              <div class="card horizontal" style="height: 130px">
                                <div class="card-image">
                                  <img src="../../assets/images/gallary/frame.png" style="height: 101%;">
                                </div>
                                <div class="card-stacked">
                                  <div class="card-content" style="padding: 13px">
                                    <p>Makanan</p>
                                  </div>
                                  <div class="card-action">
                                    <a href="#lihat_masakan?jenis=Makanan"
                                      class="btn waves-effect waves-light btn modal-trigger">Lihat
                                      Makanan</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col s12 m6 l6">
                              <div class="card horizontal" style="height: 130px">
                                <div class="card-image">
                                  <img src="../../assets/images/gallary/frame.png" style="height: 101%;">
                                </div>
                                <div class="card-stacked">
                                  <div class="card-content" style="padding: 13px">
                                    <p>Minuman</p>
                                  </div>
                                  <div class="card-action">
                                    <a href="#lihat_masakan?jenis=Minuman"
                                      class="btn waves-effect waves-light btn modal-trigger">Lihat
                                      Minuman</a>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="row section">
                              <div class="col s12">
                                <div class="row section">
                                  <div class="col s12">
                                    <!-- <p>The Medium Card limits the height of the card to 400px.</p> -->
                                  </div>

                                  <!--- Ubah Masakan --->

                                  <div id="lihat_masakan?jenis=Makanan" class="modal">
                                    <div class="modal-content">
                                      <h6 style="font-weight :bold">Kategori Makanan</h6>
                                      <?php
                                                                            $makan = "SELECT * FROM kategori WHERE jenis = 'Makanan' ";
                                                                                $o_makan = $mysqli->query($makan);
                                                                                while($p_makan = mysqli_fetch_array($o_makan)){
                                                                            ?>

                                      <div class="col s12 m6 l6">
                                        <div class="card horizontal" style="height: 130px">
                                          <div class="card-image">
                                            <img src="../../assets/images/gallary/frame.png" style="height: 101%;">
                                          </div>
                                          <div class="card-stacked">
                                            <div class="card-content" style="padding: 13px">
                                              <p><?= $p_makan['nama_kategori'] ?>
                                              </p>
                                            </div>
                                            <div class="card-action">
                                              <a href="#lihat_kategori?nama_kategori=<?= $p_makan['nama_kategori'] ?>"
                                                class="btn waves-effect waves-light btn modal-trigger">Lihat</a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <?php } ?>
                                    </div>
                                  </div>

                                  <div id="lihat_masakan?jenis=Minuman" class="modal">
                                    <div class="modal-content">
                                      <h6 style="font-weight :bold">Kategori Minuman</h6>
                                      <?php 
                                                                                $minum = "SELECT * FROM kategori WHERE jenis = 'Minuman' ";
                                                                                    $o_minum = $mysqli->query($minum);
                                                                                    while($p_minum = mysqli_fetch_array($o_minum)){
                                                                            ?>
                                      <div class="col s12 m6 l6">
                                        <div class="card horizontal" style="height: 130px">
                                          <div class="card-image">
                                            <img src="../../assets/images/gallary/frame.png" style="height: 101%;">
                                          </div>
                                          <div class="card-stacked">
                                            <div class="card-content" style="padding: 13px">
                                              <p><?= $p_minum['nama_kategori'] ?>
                                              </p>
                                            </div>
                                            <div class="card-action">
                                              <a href="#lihat_kategori?nama_kategori=<?= $p_minum['nama_kategori'] ?>"
                                                class="btn waves-effect waves-light btn modal-trigger">Lihat</a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <?php 
                                                                    $asfasfasf = "SELECT * FROM kategori WHERE jenis = 'Minuman' ";
                                                                        $csgsdsdg = $mysqli->query($asfasfasf);
                                                                        while($asfeafsaef = mysqli_fetch_array($csgsdsdg)){
                                                                ?>
                                  <div id="lihat_kategori?nama_kategori=<?= $asfeafsaef['nama_kategori'] ?>"
                                    class="modal">
                                    <div class="modal-content">
                                      <h6 style="font-weight :bold"><?= $asfeafsaef['nama_kategori'] ?></h6>




                                      <div class="row section">
                                        <div class="col s12">
                                          <!-- <p>The Medium Card limits the height of the card to 400px.</p> -->
                                        </div>
                                        <?php
                                          $data = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$asfeafsaef[nama_kategori]' ORDER BY nama_masakan ASC";
                                          $bacadata = $mysqli->query($data);
                                          while($select_result = mysqli_fetch_array($bacadata))
                                      {
                                          $id_masakan          = $select_result['id_masakan'];
                                          $nama_masakan        = $select_result['nama_masakan'];
                                          $harga               = $select_result['harga'];
                                          $gambar              = $select_result['gambar'];
                                      ?>
                                        <div class="col s12 m4 l4">
                                          <div class="card">
                                            <div class="card-image waves-effect waves-block waves-light"
                                              style="height: 35%;">
                                              <div class="col s12 m12 l12">
                                              </div>
                                              <img class="activator"
                                                src="../../assets/images/masakan/<?php echo $gambar;?>">
                                            </div>
                                            <div class="card-content">
                                              <span class="card-title activator grey-text text-darken-4"
                                                style="font-size:1.3em">
                                                <?php echo $nama_masakan;?><i
                                                  class="material-icons right">more_vert</i></span>
                                              <p>
                                                <?= "Rp.".number_format($harga) ?>
                                                <p>
                                            </div>
                                            <div class="card-reveal">
                                              <span class="card-title grey-text text-darken-4"><i
                                                  class="material-icons right">close</i></span>
                                              <form
                                                action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=BE6PnyytM45acZyu"
                                                method="post">
                                                <div class="input-field" style="margin-top: 5px">
                                                  <div class="row">
                                                    <input type="hidden" name="id_order"
                                                      value="<?php echo $dada['id_order']; ?>">
                                                    <input type="hidden" name="id_user"
                                                      value="<?php echo $ada['id_user']; ?>">
                                                    <input type="hidden" name="id_masakan"
                                                      value="<?php echo $id_masakan; ?>">
                                                    <input type="hidden" name="harga" value="<?php echo $harga; ?>">


                                                    <div class="input-field col s12">
                                                      <input name="kuantitas" type="number"
                                                        class="validate col s4 m6 l4" required>
                                                      <label class="active">Jumlah</label>
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="input-field col s12">
                                                      <input name="keterangan" type="text" required>
                                                      <label class="active">Catatan</label>
                                                    </div>
                                                  </div>
                                                  <a
                                                    href="../../config/mf_min_proc.php?id_masakan=<?php echo $id_masakan;?>"><button
                                                      class="btn waves-effect waves-light" type="submit"
                                                      name="action">Pesan</button></a>
                                                  <!-- <button class="btn waves-effect waves-light" type="submit" name="action"><a href="?<?php echo $id_masakan;?>">Daftarkan</a></button>-->
                                                </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div>
                                        <?php } ?>
                                      </div>
                                    </div>
                                  </div>
                                  <?php
                                                                            $ggfdgfg = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$asfeafsaef[nama_kategori]' ORDER BY nama_masakan ASC";
                                                                            $eryet = $mysqli->query($ggfdgfg);
                                                                            while($fgjfg = mysqli_fetch_array($eryet)){
                                                                        ?>
                                  <!--- Ubah Masakan --->
                                  <form
                                    action="../../config/mf_min_ubah_masakan.php?id_masakan=<?php echo $fgjfg['id_masakan'];?>"
                                    method="post" enctype="multipart/form-data">
                                    <div id="ubah_masakan<?php echo $fgjfg['id_masakan'];?>"
                                      class="modal modal-fixed-footer">
                                      <div class="modal-content">
                                        <h6 style="font-weight:bold">Ubah
                                          Masakan</h6>
                                        <input name="nama_masakan" type="text"
                                          value="<?php echo $fgjfg['nama_masakan'];?>" required="">
                                        <input name="harga" type="number" value="<?php echo $fgjfg['harga'];?>"
                                          required="">
                                        <label>Gambar</label>
                                        <br>
                                        <br>
                                        <input type="file" value="<?php echo $fgjfg['gambar'];?>" name="gambar"
                                          accept="image/*">
                                        <br>
                                        <br>
                                        <div class="input-field col s12">
                                          <select name="id_kategori">
                                            <option disabled>Pilih
                                              Kategori</option>
                                            <option name="id_kategori" value="<?= $fgjfg['id_kategori'] ?>" selected>
                                              <?= $fgjfg['nama_kategori'] ?>
                                            </option>

                                            <?php
                                                                                        $a = "SELECT * FROM `kategori` WHERE NOT nama_kategori = '$fgjfg[nama_kategori]' ORDER BY nama_kategori ASC";
                                                                                        $p = $mysqli->query($a);
                                                                                        while($c = mysqli_fetch_array($p))
                                                                                        {
                                                                                            $id_kategori    = $c['id_kategori'];
                                                                                            $nama_kategori  = $c['nama_kategori'];
                                                                                    ?>
                                            <option name="id_kategori" value="<?= $id_kategori ?>">
                                              <?= $nama_kategori ?>
                                            </option>
                                            <?php } ?>
                                          </select>
                                          <label>Kategori Menu</label>
                                        </div>
                                        <div class="container"><label>Jenis
                                            Menu</label><br>
                                          <?php
                                                                                        if ($fgjfg['jenis'] == 'Makanan') {
                                                                                        ?>
                                          <label>
                                            <input class="with-gap" name="jenis" type="radio" value="Makanan" required
                                              checked />
                                            <span>Makanan</span>
                                            <input class="with-gap" name="jenis" type="radio" value="Minuman"
                                              required />
                                            <span>Minuman</span>
                                          </label>
                                          <?php } else { ?>
                                          <label>
                                            <input class="with-gap" name="jenis" type="radio" value="Minuman" required
                                              checked />
                                            <span>Minuman</span>
                                            <input class="with-gap" name="jenis" type="radio" value="Makanan"
                                              required />
                                            <span>Makanan</span>
                                          </label>
                                          <?php } ?>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="waves-effect waves-light btn modal-trigger"
                                          style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Submit</button>
                                      </div>
                                    </div>
                                  </form>
                                  <!-- Ubah Masakan -->
                                  <?php } }
                                      $fhgh = "SELECT * FROM kategori WHERE jenis = 'Makanan' ";
                                          $kgyjhf = $mysqli->query($fhgh);
                                          while($wqeweqs = mysqli_fetch_array($kgyjhf)){
                                  ?>
                                  <div id="lihat_kategori?nama_kategori=<?= $wqeweqs['nama_kategori'] ?>" class="modal">
                                    <div class="modal-content">
                                      <h6 style="font-weight:bold"><?= $wqeweqs['nama_kategori'] ?></h6>
                                      <div class="row section">
                                        <div class="col s12">
                                          <!-- <p>The Medium Card limits the height of the card to 400px.</p> -->
                                        </div>
                                        <?php
                                            $data = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$wqeweqs[nama_kategori]' ORDER BY nama_masakan ASC";
                                            $bacadata = $mysqli->query($data);
                                            while($select_result = mysqli_fetch_array($bacadata))
                                        {
                                            $id_masakan          = $select_result['id_masakan'];
                                            $nama_masakan        = $select_result['nama_masakan'];
                                            $harga               = $select_result['harga'];
                                            $gambar              = $select_result['gambar'];
                                        ?>
                                        <div class="col s12 m4 l4">
                                          <div class="card">
                                            <div class="card-image waves-effect waves-block waves-light"
                                              style="height: 35%;">
                                              <div class="col s12 m12 l12">
                                              </div>
                                              <img class="activator"
                                                src="../../assets/images/masakan/<?php echo $gambar;?>">
                                            </div>
                                            <div class="card-content">
                                              <span class="card-title activator grey-text text-darken-4"
                                                style="font-size:1.3em">
                                                <?php echo $nama_masakan;?><i
                                                  class="material-icons right">more_vert</i></span>
                                              <p>
                                                <?= "Rp.".number_format($harga) ?>
                                                <p>
                                            </div>
                                            <div class="card-reveal">
                                              <span class="card-title grey-text text-darken-4"><i
                                                  class="material-icons right">close</i></span>
                                              <form
                                                action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=BE6PnyytM45acZyu"
                                                method="post">
                                                <div class="input-field" style="margin-top: 5px">
                                                  <div class="row">
                                                    <input type="hidden" name="id_order"
                                                      value="<?php echo $dada['id_order']; ?>">
                                                    <input type="hidden" name="id_user"
                                                      value="<?php echo $ada['id_user']; ?>">
                                                    <input type="hidden" name="id_masakan"
                                                      value="<?php echo $id_masakan; ?>">
                                                    <input type="hidden" name="harga" value="<?php echo $harga; ?>">


                                                    <div class="input-field col s12">
                                                      <input name="kuantitas" type="number"
                                                        class="validate col s4 m6 l4" required>
                                                      <label class="active">Jumlah</label>
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="input-field col s12">
                                                      <input name="keterangan" type="text" required>
                                                      <label class="active">Catatan</label>
                                                    </div>
                                                  </div>
                                                  <a
                                                    href="../../config/mf_min_proc.php?id_masakan=<?php echo $id_masakan;?>"><button
                                                      class="btn waves-effect waves-light" type="submit"
                                                      name="action">Pesan</button></a>
                                                  <!-- <button class="btn waves-effect waves-light" type="submit" name="action"><a href="?<?php echo $id_masakan;?>">Daftarkan</a></button>-->
                                                </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div>
                                        <?php } ?>
                                      </div>
                                    </div>
                                  </div>
                                  <?php
                                                                            $rffdhf = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$wqeweqs[nama_kategori]' ORDER BY nama_masakan ASC";
                                                                            $dgsdg = $mysqli->query($rffdhf);
                                                                            while($asfagg = mysqli_fetch_array($dgsdg)){
                                                                        ?>
                                  <!--- Ubah Masakan --->
                                  <form
                                    action="../../config/mf_min_ubah_masakan.php?id_masakan=<?php echo $asfagg['id_masakan'];?>"
                                    method="post" enctype="multipart/form-data">
                                    <div id="ubah_masakan<?php echo $asfagg['id_masakan'];?>"
                                      class="modal modal-fixed-footer">
                                      <div class="modal-content">
                                        <h6 style="font-weight:bold">Ubah
                                          Masakan</h6>
                                        <input name="nama_masakan" type="text"
                                          value="<?php echo $asfagg['nama_masakan'];?>" required="">
                                        <input name="harga" type="number" value="<?php echo $asfagg['harga'];?>"
                                          required="">
                                        <label>Gambar</label>
                                        <br>
                                        <br>
                                        <input type="file" value="<?php echo $asfagg['gambar'];?>" name="gambar"
                                          accept="image/*">
                                        <br>
                                        <br>
                                        <div class="input-field col s12">
                                          <select name="id_kategori">
                                            <option disabled>Pilih
                                              Kategori</option>
                                            <option name="id_kategori" value="<?= $asfagg['id_kategori'] ?>" selected>
                                              <?= $asfagg['nama_kategori'] ?>
                                            </option>

                                            <?php
                                                                                                $sdgdsgsdg = "SELECT * FROM `kategori` WHERE NOT nama_kategori = '$asfagg[nama_kategori]' ORDER BY nama_kategori ASC";
                                                                                                $dgsgsdg = $mysqli->query($sdgdsgsdg);
                                                                                                while($cghg = mysqli_fetch_array($dgsgsdg))
                                                                                                {
                                                                                                    $id_kategori    = $cghg['id_kategori'];
                                                                                                    $nama_kategori  = $ccghg['nama_kategori'];
                                                                                            ?>
                                            <option name="id_kategori" value="<?= $id_kategori ?>">
                                              <?= $nama_kategori ?>
                                            </option>
                                            <?php } ?>
                                          </select>
                                          <label>Kategori Menu</label>
                                        </div>
                                        <div class="container"><label>Jenis
                                            Menu</label><br>
                                          <?php
                                                                                        if ($asfagg['jenis'] == 'Makanan') {
                                                                                        ?>
                                          <label>
                                            <input class="with-gap" name="jenis" type="radio" value="Makanan" required
                                              checked />
                                            <span>Makanan</span>
                                            <input class="with-gap" name="jenis" type="radio" value="Minuman"
                                              required />
                                            <span>Minuman</span>
                                          </label>
                                          <?php } else { ?>
                                          <label>
                                            <input class="with-gap" name="jenis" type="radio" value="Minuman" required
                                              checked />
                                            <span>Minuman</span>
                                            <input class="with-gap" name="jenis" type="radio" value="Makanan"
                                              required />
                                            <span>Makanan</span>
                                          </label>
                                          <?php } ?>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="waves-effect waves-light btn modal-trigger"
                                          style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Submit</button>
                                      </div>
                                    </div>
                                  </form>
                                  <?php } } ?>

                                </div>
                              </div>
                            </div>
                            <div class="row section">
                              <div class="col s12">
                                <!-- <p>The Medium Card limits the height of the card to 400px.</p> -->
                              </div>
                              <?php
                                    $data = "SELECT * from masakan ORDER BY nama_masakan";
                                    $bacadata = $mysqli->query($data);
                                    while($select_result = mysqli_fetch_array($bacadata))
                                {
                                    $id_masakan          = $select_result['id_masakan'];
                                    $nama_masakan        = $select_result['nama_masakan'];
                                    $harga               = $select_result['harga'];
                                    $gambar              = $select_result['gambar'];
                                ?>
                              <div class="col s12 m3 l3">
                                <div class="card">
                                  <div class="card-image waves-effect waves-block waves-light" style="height: 35%;">
                                    <div class="col s12 m12 l12">
                                    </div>
                                    <img class="activator" src="../../assets/images/masakan/<?php echo $gambar;?>">
                                  </div>
                                  <div class="card-content">
                                    <span class="card-title activator grey-text text-darken-4" style="font-size:1.3em">
                                      <?php echo $nama_masakan;?><i class="material-icons right">more_vert</i></span>
                                    <p>
                                      <?= "Rp.".number_format($harga) ?>
                                      <p>
                                  </div>
                                  <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4"><i
                                        class="material-icons right">close</i></span>
                                    <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=BE6PnyytM45acZyu"
                                      method="post">
                                      <div class="input-field" style="margin-top: 5px">
                                        <div class="row">
                                          <input type="hidden" name="id_order" value="<?php echo $dada['id_order']; ?>">
                                          <input type="hidden" name="id_user" value="<?php echo $ada['id_user']; ?>">
                                          <input type="hidden" name="id_masakan" value="<?php echo $id_masakan; ?>">
                                          <input type="hidden" name="harga" value="<?php echo $harga; ?>">


                                          <div class="input-field col s12">
                                            <input name="kuantitas" type="number" class="validate col s4 m6 l4"
                                              required>
                                            <label class="active">Jumlah</label>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="input-field col s12">
                                            <input name="keterangan" type="text" required>
                                            <label class="active">Catatan</label>
                                          </div>
                                        </div>
                                        <a href="../../config/mf_min_proc.php?id_masakan=<?php echo $id_masakan;?>"><button
                                            class="btn waves-effect waves-light" type="submit"
                                            name="action">Pesan</button></a>
                                        <!-- <button class="btn waves-effect waves-light" type="submit" name="action"><a href="?<?php echo $id_masakan;?>">Daftarkan</a></button>-->
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!--card stats end-->
      </div>
      <!--end container-->
    </section>
    <!-- END CONTENT -->
  </div>
  <!-- END WRAPPER -->
  </div>

  <?php include "aside.php"; ?>
  <!-- END MAIN -->
  <?php include "../footer.php"; ?>

  <?php } else { ?>
  <a href="../mf_p_logout_pelanggan.php" style="margin:0 50%">Logout</a>
  <?php } ?>
  <div class="hiddendiv common"></div>
  <div class="drag-target" data-sidenav="slide-out"
    style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);">
  </div>
  <div class="drag-target" data-sidenav="chat-out"
    style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
  </div>
</body>

</html>
<?php
  error_reporting(0);
  include "cs_js/index_head.php";
  include "cnn.php";

  session_start();

  if($_SESSION['id_level']=="1"){
    header("location:mf_min/mf_min.php");
  }

  elseif($_SESSION['id_level']=="2"){
    header("location:mf_pelayan/mf_pelayan.php");
  }

  elseif($_SESSION['id_level']=="3"){
    header("location:mf_kasir/mf_kasir.php");
  }

  elseif($_SESSION['id_level']=="4"){
    header("location:mf_owner/mf_owner.php");
  }

  elseif($_SESSION['id_level']=="5"){
    header("location:mf_pelanggan/mf_pelanggan.php");
  }
  ?>
<!--- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --->
<html lang="en">
<!-- Ganti judul --->
<title>RST | Login</title>
<!-- Ganti judul --->

<body class="loaded gradient-45deg-purple-light-blue">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <header id="header" class="page-topbar">
  </header>
  <div>
    <!-- START WRAPPER -->
    <div class="wrapper">
      <!-- START CONTENT -->
      <section id="content">
        <!--start container-->
        <div class="container">
          <div class="section">
            <!--Basic Form-->
            <!-- Form with icon prefixes -->
            <div class="row">
              <div class="col s4 m4 l4">
              </div>
              <!-- Form with validation -->
              <div class="col s12 m12 l4" style="margin-top: 180px;">
                <div class="card-panel">
                  <h4 class="header2">
                    <a href="../format" title="RST | UI"><center>RST</center></a>                    
                  </h4>
                  <div class="row">
                    <form class="col s12" action="mf_p_login.php" method="post">
                      <div class="row">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">account_circle</i>
                          <input id="inpAlphaNum" type="text" char-allow="&" class="alphanumeric" name="username" required="">
                          <label for="inpAlphaNum" class="">Username</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">lock</i>
                          <input id="inpAlphaNum" type="password" char-allow="&" class="alphanumeric" name="password" required="">
                          <label for="inpAlphaNum" class="">Password</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="row">
                          <div class="input-field col s12">
                            <a href="mf_forgot_pass.php" style="margin-left: 20px">Lupa Password ?</a>
                            <button class="btn waves-effect waves-light right gradient-45deg-purple-light-blue" type="submit" name="action" value="LOGIN">Login
                              <i class="material-icons right">send</i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <div class="hiddendiv common"></div>
  <script type="text/javascript">
    $(document)
    .on('keydown', '.alpha', function(e) {
      var a = e.key;
      if (a.length == 1) return /[a-z]|\$|#|\*/i.test(a);
      return true;
    })
    .on('keydown', '.numeric', function(e) {
      var a = e.key;
      if (a.length == 1) return /[0-9]|\+|-/.test(a);
      return true;
    })
    .on('keydown', '.alphanumeric', function(e) {
      var a = e.key;
      if (a.length == 1) return /[a-z]|[0-9]|_/i.test(a);
      return true;
    })
  </script>
  <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
  <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
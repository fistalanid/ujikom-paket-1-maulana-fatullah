<?php
include "../head.php";
include "../koneksi.php";

include "../../database/database.php";
$db = new database();
session_start();

if($_SESSION['id_level']==""){
  header("location:index.php?msg=login_to_access_casheer");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../mf_min/mf_min.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
//user kasir
$kasir = "SELECT * FROM user WHERE id_level='3' ";
//user pelayan
$pelayan = "SELECT * FROM user WHERE id_level='2' ";
//total transaksi
$total_transaksi = "SELECT * FROM transaksi";
//transaksi hari ini
$harian = "SELECT * FROM transaksi WHERE tanggal = NOW()";
//transaksi 7 hari terakhir
$mingguan = "SELECT * FROM transaksi WHERE tanggal >= current_date - 7";
//transaksi 1 bulan terakhir
$bulanan = "SELECT * FROM transaksi WHERE tanggal >= current_date - 30";


$result=$mysqli->query($kasir);
  $kasir = mysqli_num_rows($result); 

$result=$mysqli->query($pelayan);
  $pelayan = mysqli_num_rows($result); 

$result=$mysqli->query($total_transaksi);
  $total_transaksi = mysqli_num_rows($result); 

$result=$mysqli->query($harian);
  $harian = mysqli_num_rows($result); 

$result=$mysqli->query($mingguan);
  $mingguan = mysqli_num_rows($result); 

$result=$mysqli->query($bulanan);
  $bulanan = mysqli_num_rows($result); 

?>
<html lang="en">
<title>RST | Pemilik</title>

<body class="loaded">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START HEADER -->
  <?php include "../top_nav.php";?>
  <!-- END HEADER -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">
      <!-- START LEFT SIDEBAR NAV-->
      <?php include "aside.php"; ?>
      <!-- END LEFT SIDEBAR NAV-->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START CONTENT -->
      <section id="content">
        <!--start container-->
        <div class="container">
        <div id="card-stats">
            <div class="row mt-1">
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text" style="<?= $widget ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">people</i>
                      <p>Kasir</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $kasir ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text" style="<?= $widget_t ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">people</i>
                      <p>Pelayan</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $pelayan ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text" style="<?= $widget_b ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Total Transaksi</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $total_transaksi ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text" style="<?= $widget ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Transaksi Harian</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $harian ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text" style="<?= $widget_t ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Transaksi Mingguan</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $mingguan ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text" style="<?= $widget_b ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Transaksi Bulanan</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $bulanan ?></h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="card-widgets">
            <section id="content">
              <!--start container-->
              <div class="container">
                <!--card stats start-->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
                <div id="card-widgets">
                  <section id="content">
                    <!--start container-->
                    <div class="container">
                      <div class="section">
                        <div class="row">
                          <div class="col-md-4 col-sm-4">
                            <div class="card-panel">
                              <!--Responsive Table-->
                              <div id="responsive-table">
                                <h4 class="header">Transaksi Harian</h4>
                                <div class="row">
                                  <div class="col s11">
                                    <table width="100%" class="display nowrap" id="dataTables-example">
                                      <thead>
                                        <tr>
                                          <th style="text-align:center;">No</th>
                                          <th style="text-align:center;">No. Transaksi</th>
                                          <th style="text-align:center;">Nama</th>
                                          <th style="text-align:center;">No. Pesanan</th>
                                          <th style="text-align:center;">Tanggal</th>
                                          <th style="text-align:center;">Pembayaran</th>
                                          <th style="text-align:center;">Total Harga</th>
                                          <th style="text-align:center;">Kembalian</th>
                                        </tr>
                                      </thead>

                                      <tbody>
                                        <?php
                                                                        $no = 1;
                                                                        foreach($db->mf_show_today_transaksi() as $x){
                                                                    ?>
                                        <tr>
                                          <td style="text-align:center;" scope="row">
                                            <?= $no++; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['id_transaksi']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['nama_pelanggan']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['id_order']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['tanggal']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= "Rp.", number_format($x['kembalian']+$x['total_bayar']) ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= "Rp." ,number_format($x['total_bayar']) ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= "Rp." ,number_format($x['kembalian']) ?>
                                          </td>
                                        </tr>
                                        <?php } ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
                <!--card stats end-->
              </div>
              <!--end container-->
            </section>
          </div>
          <!--card stats start-->
          <!-- //////////////////////////////////////////////////////////////////////////// -->
          <!--card stats end-->
        </div>
        <!--end container-->
      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <!-- END MAIN -->
  <?php include "../footer.php"; ?>
  <div class="hiddendiv common"></div>
  <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
  <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
<?php 
  include "../koneksi.php";
  include "../cnn.php";
?>
<aside id="left-sidebar-nav">
  <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container" style="transform: translateX(0px);">
    <li class="user-details cyan darken-2">
      <div class="row">
        <div class="col col s4 m4 l4">
          <img src="../../assets/images/avatar/avatar-13.png" alt=""
            class="circle responsive-img valign profile-image cyan">
        </div>
        <div class="col col s8 m8 l8">

          <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#"
            data-activates="profile-dropdown-nav">
            <?php echo $_SESSION['username']; ?>
            <i class="mdi-navigation-arrow-drop-down right"></i>
          </a>
          <ul id="profile-dropdown-nav" class="dropdown-content"
            style="white-space: nowrap; position: absolute; top: 59.0469px; left: 101.234px; display: none; opacity: 1;">
            <li>
              <a href="mf_profil.php" class="grey-text text-darken-1">
                <i class="material-icons">face</i> Profile</a>
            </li>
            <li>
              <a href="mf_help.php" class="grey-text text-darken-1">
                <i class="material-icons">live_help</i> Help</a>
            </li>
            <li>
              <a href="../mf_p_logout.php" class="grey-text text-darken-1"
                onClick="return confirm('Anda ingin Keluar ?')">
                <i class="material-icons">keyboard_tab</i> Logout</a>
            </li>
          </ul>
          <p class="user-roal">Pemilik</p>
        </div>
      </div>
    </li>
    <li class="no-padding">
      <ul class="collapsible" data-collapsible="accordion">
        <li class="bold">
          <a href="mf_owner.php" class="waves-effect waves-cyan">
            <i class="material-icons">pie_chart_outlined</i>
            <span class="nav-text">Dashboard</span>
          </a>
        </li>
        <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        <!-- <li class="bold">
          <a class="waves-effect waves-cyan dropdown-trigger" data-activates="transaksi">
            <i class="material-icons">done_all</i>
            <span class="nav-text">Transaksi</span>
          </a>
        </li>
        <ul id="transaksi" class="dropdown-content" tabindex="0">
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_owner_belum_dibayar.php">
              <i class="material-icons">warning</i>Transaksi Belum Dibayar</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_owner_semua_transaksi.php">
              <i class="material-icons">done_all</i>Semua Transaksi</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_owner_transaksi_harian.php">
              <i class="material-icons">today</i>Transaksi Harian</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_owner_transaksi_mingguan.php">
              <i class="material-icons">event</i>Transaksi Mingguan</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_owner_transaksi_bulanan.php">
              <i class="material-icons">date_range</i>Transaksi Bulanan</a>
          </li>
        </ul> -->
        <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        <li class="divider" tabindex="-1"></li>

        <li class="bold">
          <a href="mf_owner_belum_dibayar.php" class="waves-effect waves-cyan">
            <i class="material-icons">warning</i>
            <span class="nav-text">Transaksi Belum Dibayar</span>
          </a>
        </li>
        
        <li class="divider" tabindex="-1"></li>

        <li class="bold">
          <a href="mf_owner_semua_transaksi.php" class="waves-effect waves-cyan">
            <i class="material-icons">done_all</i>
            <span class="nav-text">Semua Transaksi</span>
          </a>
        </li>
        <li class="bold">
          <a href="mf_owner_transaksi_harian.php" class="waves-effect waves-cyan">
            <i class="material-icons">today</i>
            <span class="nav-text">Transaksi Harian</span>
          </a>
        </li>
        <li class="bold">
          <a href="mf_owner_transaksi_mingguan.php" class="waves-effect waves-cyan">
            <i class="material-icons">event</i>
            <span class="nav-text">Transaksi Mingguan</span>
          </a>
        </li>
        <li class="bold">
          <a href="mf_owner_transaksi_bulanan.php" class="waves-effect waves-cyan">
            <i class="material-icons">date_range</i>
            <span class="nav-text">Transaksi Bulanan</span>
          </a>
        </li>
        <!-- <li>
          <a class="btn waves-effect waves-light gradient-45deg-red-pink modal-trigger" href="#bayar_langsung"
            style="font-size:12px">
            <i class="material-icons white-text" style="margin-left:-10px">payment</i>Bayar langsung
          </a>
        </li> -->
      </ul>
    </li>
    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
      <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
      <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
    </div>
  </ul>
  <a href="#" data-activates="slide-out"
    class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
    <i class="material-icons">menu</i>
  </a>
</aside>

<!--- Modal --->
<!-- proses pembayaran langsung f3yairPJbq9c7ti0 -->
<!-- aksi di mf_min_proc FpE46vHa3RKhw9N4 -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=f3yairPJbq9c7ti0" method="post"
  enctype="multipart/form-data">
  <div id="bayar_langsung" class="modal">
    <div class="modal-content">
      <h6 class="header">Pembayaran Langsung</h6>
      <?php
          $user = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM user where username = '$_SESSION[username]'"));
          $data = "SELECT * FROM user WHERE id_user = '$user[id_user]'";
          $bacadata = $mysqli->query($data);
          while($select_result = mysqli_fetch_array($bacadata)){
            $id_user  = $select_result['id_user'];
            $id_order = $select_result['id_order'];
        ?>
      <input name="id_user" type="text" value="<?php echo $id_user ?>" required="">
      <input name="id_order" type="text" value="<?php echo $id_order ?>" required="">
      <div class="input-field col s6">
        <i class="material-icons prefix">payment</i>
        <input name="nama_pelanggan" id="icon_prefix" type="number" class="validate" required>
        <label for="icon_prefix">Bayar</label>
      </div>
      <?php } ?>
    </div>

    <div class="modal-footer">
      <button type="submit" class="waves-effect waves-light btn modal-trigger">Bayar</button>
    </div>
  </div>
</form>
<!-- Modal -->

<?php 
  $fff = "SELECT *,`order`.`id_meja`,`order`.`id_user` FROM `meja` JOIN `order` ON `meja`.id_meja=`order`.id_meja JOIN user ON user.id_user=`order`.id_user ORDER BY `order`.id_meja ASC";
  $keranjang = $mysqli->query($fff);
    while($keranjang_ane = mysqli_fetch_array($keranjang)){
      $id_meja_ane  = $keranjang_ane['id_meja'];
      $id_user_ane  = $keranjang_ane['id_user'];
      $id_order_ane = $keranjang_ane['id_order'];
?>

<!--- Keranjang Pelanggan --->
<!-- proses memindahkan masakan ke detail_order -->
<form action="../../config/mf_min_to_detail.php" method="post" enctype="multipart/form-data">
  <div id="keranjang_pelanggan?id_order=<?= $id_order_ane ?>" class="modal">
    <?php
          $sssa = mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'X'");
          $a = mysqli_fetch_array($sssa);
          if(mysqli_num_rows($sssa)){
        ?>
    <a href='../../config/mf_min_cancel_pelanggan.php?id_order=<?= $a['id_order'] ?>&&id_meja=<?= $a['id_meja'] ?>'
      onClick='return confirm("Batalkan pelanggan ?")'><i class='material-icons btn-floating'
        style='float:right;padding:10px;border-radius:1px'>delete_forever</i></a>
    <?php } ?>
    <div class="modal-content">
      <h6 style="font-weight:bold">Pesanan Pelanggan</h6>
      <table class="responsive-table centered Highlight striped bordered">
        <thead>
          <tr>
            <th style="text-align:center;">Masakan</th>
            <th width="1%" style="text-align:center;">Kuantitas</th>
            <th style="text-align:center;">Satuan</th>
            <th style="text-align:center;">Jumlah</th>
            <th style="text-align:center;">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
                $total_semua = 0 ;          
                $data = "SELECT * FROM `detail_order` JOIN `masakan` ON `detail_order`.`id_masakan`=`masakan`.`id_masakan` WHERE detail_order.id_order = '$id_order_ane'";
                $bacadata = $mysqli->query($data);
                while($select_result = mysqli_fetch_array($bacadata))
                {
                $id_masakan            = $select_result['id_masakan'];
                $id_order              = $select_result['id_order'];
                $id_detail_order       = $select_result['id_detail_order'];
                $nama_masakan          = $select_result['nama_masakan'];
                $kuantitas             = $select_result['kuantitas'];
                $harga                 = $select_result['harga'];
                $total_harga           = $select_result['kuantitas']*$select_result['harga'];
                $total_semua           += ($total_harga);

            ?>
          <tr>
            <td>
              <input type="hidden" name="id_masakan[]" value="<?php echo $id_masakan ?>">
              <?= $nama_masakan; ?>
            </td>
            <td>
              <input type="hidden" name="kuantitas[]" value="<?php echo $kuantitas ?>">
              <?php echo $kuantitas ?>
            </td>
            <input type="hidden" name="keterangan[]" value="<?php echo $keterangan ?>">
            <input type="hidden" name="id_user" value="<?php echo $id_user ?>">
            <td>
              <?= "Rp.".number_format($harga) ?>
            </td>
            <td>
              <input type="hidden" name="total_harga" value="<?php echo $total_harga ?>">
              <?= "Rp.".number_format($total_harga) ?>
            </td>
            <td>
              <?php
                $p_proses = $mysqli->query("SELECT * FROM detail_order WHERE id_detail_order = '$id_detail_order'");
                $ppp = mysqli_fetch_array($p_proses);
                if ($ppp['status_detail_order'] == 'Diproses') {
              ?>
              <a class="btn" disabled>Diproses</a>
              <?php } elseif ($ppp['status_detail_order'] == 'Selesai') { ?>
              <a class="btn" style="background-color: #00adff;" readonly>Selesai</a>
              <?php } ?>
            </td>

          </tr>
          <?php } ?>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total : RP.<?= number_format($total_semua) ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</form>
<?php } ?>
<!-- Keranjang Pelanggan -->

<?php
    $atasastrfs = "SELECT * from detail_order JOIN `order` ON detail_order.id_order=`order`.id_order JOIN masakan ON masakan.id_masakan=detail_order.id_masakan";
          $asasfas = $mysqli->query($atasastrfs);
          while($asasasa = mysqli_fetch_array($asasfas)){
            $id_user_o        = $asasasa['id_user'];
            $nama_pelanggan_o = $asasasa['nama_pelanggan'];
            $id_meja_o        = $asasasa['id_meja'];
    $tas = "SELECT *,SUM(detail_order.kuantitas*masakan.harga) as aa FROM `detail_order` JOIN `masakan` ON `detail_order`.`id_masakan`=`masakan`.`id_masakan` WHERE detail_order.id_order = '$asasasa[id_order]'";
    $sda = $mysqli->query($tas);
    while($rare = mysqli_fetch_array($sda)){
      $id_order_o   = $rare['id_order'];
      $total_harga  = $rare['aa'];
?>
<!-- proses pembayaran transaksi MON8L7GaeywyGSPx -->
<form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=MON8L7GaeywyGSPx" method="post"
  enctype="multipart/form-data">
  <div id="pembayaran?id_order=<?= $id_order_o ?>" class="modal">
    <div class="modal-content">
      <h6 class="header">Pembayaran</h6>

      <input name="nama_pelanggan" type="hidden" value="<?php echo $nama_pelanggan_o ?>">
      <input name="id_meja" type="hidden" value="<?php echo $id_meja_o ?>">
      <input name="id_user" type="hidden" value="<?php echo $id_user_o ?>">
      <input name="id_order" type="hidden" value="<?php echo $asasasa['id_order'] ?>">
      <input name="total_bayar" type="hidden" value="<?php echo $total_harga ?>">
      <div class="row">
        <div class="input-field col s12">
          <input type="text" class="validate col s12 m12 l12" value="<?= 'Rp.'.number_format($total_harga) ?>"
            style="font-size:1.2em" disabled>
          <label class="active" style="font-size:1.2em">Total Bayar</label>
        </div>
      </div>
      <div class="input-field col s6">
        <i class="material-icons prefix">payment</i><I></I>
        <input name="pembayaran" id="input-phone-salah" type="number" class="validate" required>
        <label>Rupiah</label>
      </div>

    </div>

    <div class="modal-footer">
      <button type="submit" class="waves-effect waves-light btn modal-trigger">Bayar</button>
    </div>
  </div>
</form>
<?php } } ?>
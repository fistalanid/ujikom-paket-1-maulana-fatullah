<?php
include "../head.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
session_start();

if($_SESSION['id_level']==""){
  header("location:index.php?msg=login_to_access_casheer");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../mf_min/mf_min.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">
<title>RST | Pelayan :: <?php echo $_SESSION['username']; ?></title>
<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <?php include "../top_nav.php"; ?>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <?php include "aside.php"; ?>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div class="card-panel">
                        <h4 class="header">Transaksi Belum Dibayar</h4>
                        <div class="row">
                            <?php
                                $data = "SELECT *,`order`.`id_meja`,`order`.`id_user` FROM `meja` JOIN `order` ON `meja`.id_meja=`order`.id_meja JOIN user ON user.id_user=`order`.id_user WHERE NOT `order`.status = 'V' AND user.status = 'O' ORDER BY `order`.id_meja ASC ";
                                $bacadata = $mysqli->query($data);
                                while($select_result = mysqli_fetch_array($bacadata))
                            {
                                $id_meja          = $select_result['id_meja'];
                                $id_user          = $select_result['id_user'];
                                $id_order         = $select_result['id_order'];
                                $nama_pelanggan   = $select_result['nama_pelanggan'];
                                $status_meja      = $select_result['status_meja'];
                                $no_meja          = $select_result['no_meja'];
                                $status_order     = $select_result['status_order'];
                                $keterangan       = $select_result['keterangan'];
                                $tanggal          = $select_result['tanggal'];
                            ?>
                            <div class="col s12 m4 l4">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light">
                                        <div class="col s12 m12 l12" style="height: 35%">
                                            <div class="col s5 m5 l5"></div>
                                            <div class="col s4 m4 l4" style="text-shadow: black 2px 2px 5px;font-size: 40px;color: #ff4081;z-index: 99999;position: relative;margin-top: 38%">
                                                <?php echo $no_meja; ?>
                                            </div>
                                            <div class="col s3 m4 l4"></div>
                                        </div>
                                        <img style="position:absolute;z-index: 1;margin-top:10%" class="activator" src="../../assets/images/gallary/frame.png">
                                    </div>
                                    <div class="card-content">
                                        <span class="card-title activator grey-text text-darken-4">
                                            <?php echo $nama_pelanggan?><i class="material-icons right">more_vert</i></span>
                                        
                                            <?php
                                                $dere = $mysqli->query("SELECT * FROM detail_order WHERE detail_order.status_detail_order = 'Selesai' AND id_order = '$id_order' ");
                                                $ppp = mysqli_fetch_array($dere);
                                                $p_proses = $mysqli->query("SELECT * FROM detail_order WHERE NOT status_detail_order = 'Selesai' AND id_order = '$id_order'");
                                                if (mysqli_num_rows($p_proses)) {
                                            ?>
                                                
                                            <?php } elseif($ppp['status_detail_order'] == 'Selesai') { ?>
                                                <p><a href="#pembayaran?id_order=<?= $id_order ?>" class="btn waves-effect waves-light modal-trigger" style="background-color: #00adff;">Bayar</a></p>
                                                
                                            <?php } ?>

                                    </div>
                                    <div class="card-reveal">
                                        <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                                        <p style="font-size: 1.2em">ID Order <?php echo $id_order ?></p>
                                        <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=nFWWeEdVWrjWQmtm"
                                            method="post">
                                            <div class="input-field" style="margin-top: 5px">
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" class="validate col s12 m12 l12" value="<?php echo $tanggal;?>"
                                                            disabled>
                                                        <label class="active">Tanggal Pesanan</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" value="<?php echo $keterangan; ?>" disabled>
                                                        <label class="active">Catatan</label>
                                                    </div>
                                                </div>
                                                <a href="#keranjang_pelanggan?id_order=<?= $id_order ?>" class="btn waves-effect waves-light modal-trigger">Pesanan</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>
                        </div>
                    </div>

                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
    <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
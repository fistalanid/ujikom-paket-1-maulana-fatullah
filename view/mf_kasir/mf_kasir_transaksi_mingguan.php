<?php
session_start();
include "../head.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
 
if($_SESSION['id_level']==""){
    header("location:index.php?msg=login_to_access_casheer");
    }
    
    elseif($_SESSION['id_level']=="1"){
    header("location:../mf_min/mf_min.php");
    }
    
    elseif($_SESSION['id_level']=="2"){
    header("location:../mf_pelayan/mf_pelayan.php");
    }
    
    elseif($_SESSION['id_level']=="4"){
    header("location:../mf_owner/mf_owner.php");
    }
    
    elseif($_SESSION['id_level']=="5"){
    header("location:../mf_pelanggan/mf_pelanggan.php");
    }   
?>
<html lang="en">
<title>RST | Admin - Transaksi Mingguan</title>

<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <?php include "../top_nav.php";?>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <?php include "aside.php" ?>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div id="card-widgets">
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <div class="section">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="card-panel">
                                                <!--Responsive Table-->
                                                <div id="responsive-table">
                                                    <h4 class="header">Data Transaksi Mingguan</h4>
                                                    <div class="row">
                                                        <div class="col s11">
                                                            <table width="100%"
                                                                class="table table-striped table-bordered table-hover"
                                                                id="dataTables-example">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="text-align:center;">No</th>
                                                                        <th style="text-align:center;">No. Transaksi</th>
                                                                        <th style="text-align:center;">Nama</th>
                                                                        <th style="text-align:center;">No. Pesanan</th>
                                                                        <th style="text-align:center;">Tanggal</th>
                                                                        <th style="text-align:center;">Pembayaran</th>
                                                                        <th style="text-align:center;">Total Harga</th>
                                                                        <th style="text-align:center;">Kembalian</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    <?php
                                                                        $no = 1;
                                                                        foreach($db->mf_show_week_transaksi() as $x){
                                                                    ?>
                                                                    <tr>
                                                                        <td style="text-align:center;" scope="row">
                                                                            <?= $no++; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['id_transaksi']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['nama_pelanggan']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['id_order']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['tanggal']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= "Rp.", number_format($x['kembalian']+$x['total_bayar']) ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= "Rp." ,number_format($x['total_bayar']) ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= "Rp." ,number_format($x['kembalian']) ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
    <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
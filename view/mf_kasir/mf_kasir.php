<?php
include "../head.php";

session_start();

if($_SESSION['id_level']==""){
  header("location:../index.php?msg=login_to_access_casheer");
}

elseif($_SESSION['id_level']=="1"){
  header("location:../mf_min/mf_min.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">
<title>RST | Kasir</title>

<body class="loaded">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <?php include "../top_nav.php"; ?>
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">
      <!-- START LEFT SIDEBAR NAV-->
      <?php include "aside.php"; ?>
      <!-- END LEFT SIDEBAR NAV-->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START CONTENT -->
      <section id="content">
        <!--start container-->
        <div class="container">
          <!--card stats start-->
          <!-- //////////////////////////////////////////////////////////////////////////// -->
          <div class="card-panel">
                <h4 class="header">Meja Kosong</h4>
            <div class="row">
              <?php
                include "../koneksi.php";
                $no=0;
                $data = "SELECT * from meja WHERE status_meja = 'O'";
                $bacadata = $mysqli->query($data);
                while($select_result = mysqli_fetch_array($bacadata))
            {
                $id_meja          = $select_result['id_meja'];
                $no_meja        = $select_result['no_meja'];
            ?>
              <div class="col s12 m4 l4">
                <div class="card">
                  <div class="card-image waves-effect waves-block waves-light">
                    <div class="col s12 m12 l12" style="height: 35%">
                          <div class="col s5 m5 l5"></div>
                        <div class="col s4 m4 l4" style="text-shadow: black 2px 2px 5px;font-size: 40px;color: #ff4081;z-index: 99999;position: relative;margin-top: 38%"><?php echo $no_meja; ?></div>
                          <div class="col s3 m4 l4"></div>
                    </div>
                  <img style="position:absolute;z-index: 1;margin-top:10%" class="activator" src="../../assets/images/gallary/frame.png">
                  </div>
                  <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">RST | Kasir<i class="material-icons right">more_vert</i></span>
                  </div>
                  <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Pelanggan<i class="material-icons right">close</i></span>
                    <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=nFWWeEdVWrjWQmtm" method="post">
                      <div class="input-field" style="margin-top: 5px">
                          <div class="row">
                            <div class="input-field col s12">
                              <input name="username" type="text" class="validate col s4 m6 l3" value="RST<?php echo $no_meja;?>" readonly>
                              <input name="id_meja" type="hidden" class="validate col s4 m6 l3" value="<?php echo $no_meja;?>" readonly>
                              <label class="active">Username</label> 
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s12">
                              <input name="password" type="password" value="999999" readonly>
                              <label class="active">Password Pelanggan</label>
                            </div>
                          </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
           <?php
              } ?>
            </div>
          </div>
          <!-- //////////////////////////////////////////////////////////////////////////// -->
          <!--card stats end-->
        </div>
        <!--end container-->
      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <!-- END MAIN -->
  <?php include "../footer.php"; ?>
  <div class="hiddendiv common"></div>
  <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
  <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
<?php
include "head.php";

session_start();
 
// cek apakah yang mengakses halaman ini sudah login
if($_SESSION['id_level']==""){
  header("location:index.php?pesan=failed");
}
?>
<html lang="en">

<body class="loaded">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START HEADER -->
  <header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
      <nav class="navbar-color gradient-45deg-light-blue-cyan">
        <div class="nav-wrapper">
          <ul class="left">
            <li>
              <h1 class="logo-wrapper">
                <a href="index.html" class="brand-logo darken-1">
                  <img src="../assets/images/logo/materialize-logo.png" alt="materialize logo">
                </a>
              </h1>
            </li>
          </ul>
          <div class="header-search-wrapper hide-on-med-and-down">


          </div>
          <ul class="right hide-on-med-and-down">

            <li>
              <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                <i class="material-icons">settings_overscan</i>
              </a>
            </li>
            <li>
              <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown">
                <i class="material-icons">notifications_none
                  <small class="notification-badge pink accent-2">5</small>
                </i>
              </a>
              <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 951.609px; position: absolute; top: 64px; display: none;">
                <li>
                  <h6>NOTIFICATIONS
                    <span class="new badge">5</span>
                  </h6>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="#!" class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> A new order has been placed!</a>
                  <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                </li>
                <li>
                  <a href="#!" class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle red small">stars</span> Completed the task</a>
                  <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                </li>
                <li>
                  <a href="#!" class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle teal small">settings</span> Settings updated</a>
                  <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                </li>
                <li>
                  <a href="#!" class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle deep-orange small">today</span> Director meeting started</a>
                  <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                </li>
                <li>
                  <a href="#!" class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle amber small">trending_up</span> Generate monthly report</a>
                  <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                </li>
              </ul>
            </li>
            <li>
              </a>
              <ul id="profile-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1150.64px; position: absolute; top: 64px; display: none;">
                <li>
                  <a href="#" class="grey-text text-darken-1">
                    <i class="material-icons">face</i> Profile</a>
                </li>
                <li>
                  <a href="#" class="grey-text text-darken-1">
                    <i class="material-icons">settings</i> Settings</a>
                </li>
                <li>
                  <a href="#" class="grey-text text-darken-1">
                    <i class="material-icons">live_help</i> Help</a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="#" class="grey-text text-darken-1">
                    <i class="material-icons">lock_outline</i> Lock</a>
                </li>
                <li>
                  <a href="#" class="grey-text text-darken-1">
                    <i class="material-icons">keyboard_tab</i> Logout</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mf_p_logout.php" class="waves-effect waves-block waves-heavy">
                <i class="material-icons">keyboard_tab</i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <!-- end header nav-->
  </header>
  <!-- END HEADER -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">
      <!-- START LEFT SIDEBAR NAV-->
      <aside id="left-sidebar-nav">
        <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container" style="transform: translateX(0px);">
          <li class="user-details cyan darken-2">
            <div class="row">
              <div class="col col s4 m4 l4">
                <img src="../assets/images/avatar/avatar-6.png" alt="" class="circle responsive-img valign profile-image cyan">
              </div>
              <div class="col col s8 m8 l8">

                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown-nav">
                  <?php echo $_SESSION['username']; ?>
                  <i class="mdi-navigation-arrow-drop-down right"></i>
                </a>
                <ul id="profile-dropdown-nav" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 59.0469px; left: 101.234px; display: none; opacity: 1;">
                  <li>
                    <a href="mf_profil.php" class="grey-text text-darken-1">
                      <i class="material-icons">face</i> Profile</a>
                  </li>
                  <li>
                    <a href="mf_help.php" class="grey-text text-darken-1">
                      <i class="material-icons">live_help</i> Help</a>
                  </li>
                  <li>
                    <a href="mf_p_logout.php" class="grey-text text-darken-1">
                      <i class="material-icons">keyboard_tab</i> Logout</a>
                  </li>
                </ul>
                <p class="user-roal">Admin</p>
              </div>
            </div>
          </li>
          <li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
              <li class="bold">
                <a href="mf_min.php" class="waves-effect waves-cyan">
                  <i class="material-icons">pie_chart_outlined</i>
                  <span class="nav-text">Dashboard</span>
                </a>
              </li>
              <li class="bold">
                <a href="mf_kasir.php" class="waves-effect waves-cyan">
                  <i class="material-icons">people</i>
                  <span class="nav-text">Kasir</span>
                </a>
              </li>
              <li class="bold">
                <a href="mf_pelayan.php" class="waves-effect waves-cyan">
                  <i class="material-icons">people</i>
                  <span class="nav-text">Pelayan</span>
                </a>
              </li>
              <li class="bold">
                <a href="mf_owner.php" class="waves-effect waves-cyan">
                  <i class="material-icons">people</i>
                  <span class="nav-text">Owner</span>
                </a>
              </li>
              <li class="bold">
                <a href="mf_masakan.php" class="waves-effect waves-cyan">
                  <i class="material-icons">restaurant_menu</i>
                  <span class="nav-text">Masakan</span>
                </a>
              </li>
              <li class="bold">
                <a href="mf_transaksi.php" class="waves-effect waves-cyan">
                  <i class="material-icons">done_all</i>
                  <span class="nav-text">Transaksi</span>
                </a>
              </li>
              <!-- <li>
                  <a class="btn waves-effect waves-light gradient-45deg-red-pink" href="https://pixinvent.com/materialize-material-design-admin-template/landing/" target="_blank">
                    <i class="material-icons white-text">file_upload</i>Upgrade to Pro!
                  </a>
                </li> -->
            </ul>
          </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
          </div>
          <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
          </div>
        </ul>
        <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
          <i class="material-icons">menu</i>
        </a>
      </aside>
      <!-- END LEFT SIDEBAR NAV-->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START CONTENT -->
      <section id="content">
        <!--start container-->
        <div class="container">
          <!--card stats start-->
          <!-- //////////////////////////////////////////////////////////////////////////// -->






          <!--card stats end-->
        </div>
        <!--end container-->
      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <!-- END MAIN -->
  <?php include "footer.php"; ?>
  <div class="hiddendiv common"></div>
  <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
  <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
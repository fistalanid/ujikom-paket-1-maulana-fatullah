<div class="col col s8 m8 l8">

    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#"
        data-activates="profile-dropdown-nav">
        <?php echo $_SESSION['username']; ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
    <ul id="profile-dropdown-nav" class="dropdown-content"
        style="white-space: nowrap; position: absolute; top: 59.0469px; left: 101.234px; display: none; opacity: 1;">
        <li>
            <a href="../mf_profil.php" class="grey-text text-darken-1">
                <i class="material-icons">face</i> Profile</a>
        </li>
        <li>
            <a href="../mf_help.php" class="grey-text text-darken-1">
                <i class="material-icons">live_help</i> Help</a>
        </li>
        <li>
            <a href="../mf_p_logout.php" onClick="return confirm('Anda ingin Keluar ?')"
                class="grey-text text-darken-1">
                <i class="material-icons">keyboard_tab</i> Logout</a>
        </li>
    </ul>
    <p class="user-roal">Admin</p>
</div>
<?php
error_reporting(0);
  include "cs_js/index_head.php";
  include "cnn.php";
  include "function.php";

session_start();

if($_SESSION['id_level']=="1"){
  header("location:mf_min/mf_min.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:mf_pelanggan/mf_pelanggan.php");
}



  
if(isset($_POST['submit']))
{
  $email = $_POST['email'];
  $email = mysqli_real_escape_string($koneksi, $email);
  
  if(checkUser($email) == "true")
  {
    $userID = UserID($email);
    $token = generateRandomString();
    
    $query = mysqli_query($koneksi, "INSERT INTO recovery_keys (userID, token) VALUES ($userID, '$token') ");
    if($query)
    {
       $send_mail = send_mail($email, $token);


      if($send_mail === 'success') {
         $msg = 'Succeed ! Check your email';
         $msgclass = 'bg-success';
      } else {
        $msg = 'There is something wrong !.';
        $msgclass = 'bg-danger';
      }



    } else {
        $msg = 'There is something wrong.';
         $msgclass = 'bg-danger';
    }
    
  } else {
    $msg = "This email doesn't exist in our database.";
         $msgclass = 'bg-danger';
  }
}

?>
<html lang="en">
<title>RST | Reset Password</title>

<body class="loaded gradient-45deg-purple-light-blue">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <header id="header" class="page-topbar">
  </header>
  <div>
    <!-- START WRAPPER -->
    <div class="wrapper">
      <!-- START CONTENT -->
      <section id="content">
        <!--start container-->
        <div class="container">
          <div class="section">
            <!--Basic Form-->
            <!-- Form with icon prefixes -->
            <div class="row">
              <div class="col s4 m4 l4">
              </div>
              <!-- Form with validation -->
              <div class="col s12 m12 l4" style="margin-top: 200px;">
                <div class="card-panel">
                  <h4 class="header2">
                    <center>Reset Password</center>
                  </h4>
                  <div class="row">

                    <form action="" class="col s12" method="post">
                      <?php if(isset($msg)) {?>
    <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
<?php } ?>
                      <div class="row">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">mail</i>
                          <input id="name4" type="email" name="email" class="validate">
                          <label for="first_name" class="">E-mail</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="row">
                          <div class="input-field col s12">
                            <a href="index.php" style="margin-left: 20px">Back</a>
                            <button class="btn waves-effect waves-light right gradient-45deg-purple-light-blue" type="submit" name="submit">Send
                              <i class="material-icons right">send</i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <!-- jQuery Library -->
  <script type="text/javascript" src="../assets/vendors/jquery-3.2.1.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="../assets/js/materialize.min.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="../assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
  <!--plugins.js - Some Specific JS codes for Plugin Settings-->
  <script type="text/javascript" src="../assets/js/plugins.js"></script>
  <!--custom-script.js - Add your own theme custom JS-->
  <script type="text/javascript" src="../assets/js/custom-script.js"></script>

  <div class="hiddendiv common"></div>
  <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
  <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
<?php
include "../head.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
//user kasir
$kasir = "SELECT * FROM user WHERE id_level='3' ";
//user pelayan
$pelayan = "SELECT * FROM user WHERE id_level='2' ";
//total transaksi
$total_transaksi = "SELECT * FROM transaksi";
//transaksi hari ini
$harian = "SELECT * FROM transaksi WHERE tanggal = NOW()";
//transaksi 7 hari terakhir
$mingguan = "SELECT * FROM transaksi WHERE tanggal >= current_date - 7";
//transaksi 1 bulan terakhir
$bulanan = "SELECT * FROM transaksi WHERE tanggal >= current_date - 30";

session_start();

if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_administrator");
}

elseif($_SESSION['id_level']=="1"){
  $warna  = "background: linear-gradient(45deg, #363442 0%, #2196F3 50%, #E91E63 100%);";
  $widget = "background: #363442";
  $widget_t = "background: #2e8fea";
  $widget_b = "background: #dc266d";
}

elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}


$hasgda=$mysqli->query($kasir);
  $kasir = mysqli_num_rows($hasgda); 

$jasvgda=$mysqli->query($pelayan);
  $pelayan = mysqli_num_rows($jasvgda); 

$shg=$mysqli->query($total_transaksi);
  $total_transaksi = mysqli_num_rows($shg); 

$asflj=$mysqli->query($harian);
  $harian = mysqli_num_rows($asflj); 

$laiuf=$mysqli->query($mingguan);
  $mingguan = mysqli_num_rows($laiuf); 

$asi=$mysqli->query($bulanan);
  $bulanan = mysqli_num_rows($asi); 

?>
<html lang="en">
<title>RST | Administrator</title>

<body class="loaded">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START HEADER -->
  <?php include "top_bar.php";?>
  <!-- END HEADER -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">
      <!-- START LEFT SIDEBAR NAV-->
      <aside id="left-sidebar-nav">
        <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container" style="transform: translateX(0px);">
          <li class="user-details cyan darken-2">
            <div class="row">
              <div class="col col s4 m4 l4">
                <img src="../../assets/images/avatar/avatar-7.png" alt=""
                  class="circle responsive-img valign profile-image cyan">
              </div>
              <?php include "../user-dropdown.php"; ?>
            </div>
          </li>
          <li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
              <?php include "left-menu.php" ?>
            </ul>
          </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
          </div>
          <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
          </div>
        </ul>
        <a href="#" data-activates="slide-out"
          class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
          <i class="material-icons">menu</i>
        </a>
      </aside>
      <!-- END LEFT SIDEBAR NAV-->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START CONTENT -->
      <section id="content">
        <!--start container-->
        <div class="container">
          <!--card stats start-->
          <div id="card-stats">
            <div class="row mt-1">
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text"
                  style="<?= $widget ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">people</i>
                      <p>Kasir</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $kasir ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text"
                  style="<?= $widget_t ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">people</i>
                      <p>Pelayan</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $pelayan ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text"
                  style="<?= $widget_b ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Total Transaksi</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $total_transaksi ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text"
                  style="<?= $widget ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Transaksi Harian</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $harian ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text"
                  style="<?= $widget_t ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Transaksi Mingguan</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $mingguan ?></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l4">
                <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text"
                  style="<?= $widget_b ?>">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">payment</i>
                      <p>Transaksi Bulanan</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?= $bulanan ?></h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="card-widgets">
            <section id="content">
              <!--start container-->
              <div class="container">
                <!--card stats start-->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
                <div id="card-widgets">
                  <section id="content">
                    <!--start container-->
                    <div class="container">
                      <div class="section">
                        <div class="row">
                          <div class="col-md-4 col-sm-4">
                            <div class="card-panel">
                              <!--Responsive Table-->
                              <div id="responsive-table">
                                <h4 class="header">Transaksi Harian</h4>
                                <div class="row">
                                  <div class="col s11">
                                    <table width="100%" class="display nowrap" id="dataTables-example">
                                      <thead>
                                        <tr>
                                          <th style="text-align:center;">No</th>
                                          <th style="text-align:center;">No. Transaksi</th>
                                          <th style="text-align:center;">Nama</th>
                                          <th style="text-align:center;">No. Pesanan</th>
                                          <th style="text-align:center;">Tanggal</th>
                                          <th style="text-align:center;">Pembayaran</th>
                                          <th style="text-align:center;">Total Harga</th>
                                          <th style="text-align:center;">Kembalian</th>
                                        </tr>
                                      </thead>

                                      <tbody>
                                        <?php
                                                                        $no = 1;
                                                                        foreach($db->mf_show_today_transaksi() as $x){
                                                                    ?>
                                        <tr>
                                          <td style="text-align:center;" scope="row">
                                            <?= $no++; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['id_transaksi']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['nama_pelanggan']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['id_order']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= $x['tanggal']; ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= "Rp.", number_format($x['kembalian']+$x['total_bayar']) ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= "Rp." ,number_format($x['total_bayar']) ?>
                                          </td>
                                          <td style="text-align:center;">
                                            <?= "Rp." ,number_format($x['kembalian']) ?>
                                          </td>
                                        </tr>
                                        <?php } ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
                <!--card stats end-->
              </div>
              <!--end container-->
            </section>
          </div>
        </div>
        <!--end container-->
      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <!-- END MAIN -->
  <?php include "../footer.php"; ?>
  <div class="hiddendiv common"></div>
  <div class="drag-target" data-sidenav="slide-out"
    style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);">
  </div>
  <div class="drag-target" data-sidenav="chat-out"
    style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
  </div>
</body>

</html>
<li class="bold">
    <a href="mf_min.php" class="waves-effect waves-cyan">
        <i class="material-icons">pie_chart_outlined</i>
        <span class="nav-text">Dashboard</span>
    </a>
</li>
<li class="bold">
    <a href="mf_min_masakan.php" class="waves-effect waves-cyan">
        <i class="material-icons">restaurant_menu</i>
        <span class="nav-text">Masakan</span>
    </a>
</li>
<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<li class="bold">
    <a class="waves-effect waves-cyan dropdown-trigger" data-activates="user">
        <i class="material-icons">people</i>
        <span class="nav-text">User</span>
    </a>
</li>

<ul id="user" class="dropdown-content" tabindex="0">
    <li class="divider" tabindex="-1"></li>
    <li tabindex="0">
        <a href="mf_min_kasir.php">
            <i class="material-icons">people</i>Kasir</a>
    </li>
    <li class="divider" tabindex="-1"></li>
    <li tabindex="0">
        <a href="mf_min_pelayan.php">
            <i class="material-icons">people</i>Pelayan</a>
    </li>
    <li class="divider" tabindex="-1"></li>
    <li tabindex="0">
        <a href="mf_min_owner.php">
            <i class="material-icons">people</i>Pemilik</a>
    </li>
</ul>
<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

<li class="bold">
    <a class="waves-effect waves-cyan dropdown-trigger" data-activates="transaksi">
        <i class="material-icons">payment</i>
        <span class="nav-text">Transaksi</span>
    </a>
</li>
<ul id="transaksi" class="dropdown-content" tabindex="0">
<li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_min_belum_dibayar.php">
              <i class="material-icons">warning</i>Transaksi Belum Dibayar</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_min_semua_transaksi.php">
              <i class="material-icons">done_all</i>Semua Transaksi</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_min_transaksi_harian.php">
              <i class="material-icons">today</i>Transaksi Harian</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_min_transaksi_mingguan.php">
              <i class="material-icons">event</i>Transaksi Mingguan</a>
          </li>
          <li class="divider" tabindex="-1"></li>
          <li tabindex="0">
            <a href="mf_min_transaksi_bulanan.php">
              <i class="material-icons">date_range</i>Transaksi Bulanan</a>
          </li>
</ul>
<li class="divider" tabindex="-1"></li>

<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- <li class="bold">
    <a class="waves-effect waves-cyan dropdown-trigger" data-activates="front_end">
        <i class="material-icons">dashboard</i>
        <span class="nav-text">Front End</span>
    </a>
</li> -->

<ul id="front_end" class="dropdown-content" tabindex="0">
    <li class="divider" tabindex="-1"></li>
    <li tabindex="0">
        <a href="mf_min_home.php">
            <i class="material-icons">home</i>Top Home</a>
    </li>
    <li class="divider" tabindex="-1"></li>
    <li tabindex="0">
        <a href="mf_min_services.php">
            <i class="material-icons">toc</i>Services</a>
    </li>
    <li class="divider" tabindex="-1"></li>
    <li tabindex="0">
        <a href="mf_min_product.php">
            <i class="material-icons">apps</i>Product</a>
    </li>
    <li class="divider" tabindex="-1"></li>
    <li tabindex="0">
        <a href="mf_min_about.php">
            <i class="material-icons">help</i>About</a>
    </li>
</ul>
<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
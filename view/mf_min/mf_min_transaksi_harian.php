<?php
session_start();
include "../head.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
 
if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_administrator");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">
<title>RST | Admin - Transaksi Harian</title>

<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <?php include "top_bar.php";?>    
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container" style="transform: translateX(0px);">
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="../../assets/images/avatar/avatar-7.png" alt="" class="circle responsive-img valign profile-image cyan">
                            </div>
                            <?php include "../user-dropdown.php"; ?>
                        </div>
                    </li>
                    <?php include "left-menu.php" ?>
                    
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                        <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                    </div>
                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
                    <i class="material-icons">menu</i>
                </a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div id="card-widgets">
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <div class="section">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="card-panel">
                                                <!--Responsive Table-->
                                                <div id="responsive-table">
                                                    <h4 class="header">Data Semua Transaksi</h4>
                                                    <div class="row">
                                                        <div class="col s11">
                                                            <table width="100%"
                                                                class="display nowrap"
                                                                id="dataTables-example">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="text-align:center;">No</th>
                                                                        <th style="text-align:center;">No. Transaksi</th>
                                                                        <th style="text-align:center;">Nama</th>
                                                                        <th style="text-align:center;">No. Pesanan</th>
                                                                        <th style="text-align:center;">Tanggal</th>
                                                                        <th style="text-align:center;">Pembayaran</th>
                                                                        <th style="text-align:center;">Total Harga</th>
                                                                        <th style="text-align:center;">Kembalian</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    <?php
                                                                        $no = 1;
                                                                        foreach($db->mf_show_today_transaksi() as $x){
                                                                    ?>
                                                                    <tr>
                                                                        <td style="text-align:center;" scope="row">
                                                                            <?= $no++; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['id_transaksi']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['nama_pelanggan']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['id_order']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= $x['tanggal']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= "Rp.", number_format($x['kembalian']+$x['total_bayar']) ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= "Rp." ,number_format($x['total_bayar']) ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?= "Rp." ,number_format($x['kembalian']) ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
    <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
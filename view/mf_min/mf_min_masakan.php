<?php
include "../head.php";
include "../koneksi.php";

session_start();

// cek apakah yang mengakses halaman ini sudah login
if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_administrator");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">
<title>RST | Admin - Masakan</title>

<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <?php include "top_bar.php";?>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container"
                    style="transform: translateX(0px);">
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="../../assets/images/avatar/avatar-7.png" alt=""
                                    class="circle responsive-img valign profile-image cyan">
                            </div>
                            <?php include "../user-dropdown.php"; ?>
                        </div>
                    </li>
                    <li class="no-padding">
                        <ul class="collapsible" data-collapsible="accordion">
                            <!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                            <?php include "left-menu.php" ?>
                            <!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                            <li>
                                <a class="btn waves-effect waves-light gradient-45deg-red-pink modal-trigger"
                                    href="#tambah_masakan">
                                    <i class="material-icons white-text">add</i>Tambah Masakan
                                </a>
                            </li>
                        </ul>
                    </li>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                        <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                    </div>
                </ul>
                <a href="#" data-activates="slide-out"
                    class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
                    <i class="material-icons">menu</i>
                </a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->

            <!--- Modal --->
            <form action="../../config/mf_min_tambah_masakan.php" method="post" enctype="multipart/form-data">
                <div id="tambah_masakan" class="modal modal-fixed-footer">
                    <div class="modal-content">
                        <h6 style="font-weight:bold">Tambah Masakan</h6>
                        <input name="nama_masakan" type="text" placeholder="Nama Masakan" required="">
                        <input name="harga" type="number" placeholder="Harga" required="">
                        <label>Gambar</label>
                        <br>
                        <br>
                        <input type="file" name="gambar" accept="image/*" required="">
                        <br>
                        <br>
                        <div class="input-field col s12">
                            <select name="id_kategori">
                                <option disabled selected>Pilih Kategori</option>
                                <?php
                                    $a = "SELECT * FROM `kategori` ORDER BY nama_kategori ASC";
                                    $p = $mysqli->query($a);
                                    while($c = mysqli_fetch_array($p))
                                    {
                                        $id_kategori    = $c['id_kategori'];
                                        $nama_kategori  = $c['nama_kategori'];
                                ?>
                                <option name="id_kategori" value="<?= $id_kategori ?>" required><?= $nama_kategori ?>
                                </option>
                                <?php } ?>
                            </select>
                            <label>Kategori Menu</label>
                        </div>
                        <div class="container"><label>Jenis Menu</label><br>
                            <label>
                                <input class="with-gap " name="jenis" type="radio" value="Makanan" required />
                                <span>Makanan</span>
                            </label>
                            <label>
                                <input class="with-gap " name="jenis" type="radio" value="Minuman" required />
                                <span>Minuman</span>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="waves-effect waves-light btn modal-trigger"
                            style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Submit</button>
                    </div>
                </div>
            </form>
            <!-- Modal -->

            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div id="card-widgets">
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <div class="section">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="card-panel">
                                                <!--Responsive Table-->
                                                <div id="responsive-table">
                                                    <h4 class="header">Data Masakan</h4>

                                                    <div class="col s12 m6 l6">
                                                        <div class="card horizontal" style="height: 130px">
                                                            <div class="card-image">
                                                                <img src="../../assets/images/gallary/frame.png"
                                                                    style="height: 101%;">
                                                            </div>
                                                            <div class="card-stacked">
                                                                <div class="card-content" style="padding: 13px">
                                                                    <p>Makanan</p>
                                                                </div>
                                                                <div class="card-action">
                                                                    <a href="#lihat_masakan?jenis=Makanan"
                                                                        class="btn waves-effect waves-light btn modal-trigger">Lihat
                                                                        Makanan</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col s12 m6 l6">
                                                        <div class="card horizontal" style="height: 130px">
                                                            <div class="card-image">
                                                                <img src="../../assets/images/gallary/frame.png"
                                                                    style="height: 101%;">
                                                            </div>
                                                            <div class="card-stacked">
                                                                <div class="card-content" style="padding: 13px">
                                                                    <p>Minuman</p>
                                                                </div>
                                                                <div class="card-action">
                                                                    <a href="#lihat_masakan?jenis=Minuman"
                                                                        class="btn waves-effect waves-light btn modal-trigger">Lihat
                                                                        Minuman</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row section">
                                                        <div class="col s12">
                                                            <div class="row section">
                                                                <div class="col s12">
                                                                    <!-- <p>The Medium Card limits the height of the card to 400px.</p> -->
                                                                </div>

                                                                <!--- Ubah Masakan --->

                                                                <div id="lihat_masakan?jenis=Makanan" class="modal">
                                                                    <div class="modal-content">
                                                                    <h6 style="font-weight :bold">Kategori Makanan</h6>
                                                                        <?php
                                                                            $makan = "SELECT * FROM kategori WHERE jenis = 'Makanan' ";
                                                                                $o_makan = $mysqli->query($makan);
                                                                                while($p_makan = mysqli_fetch_array($o_makan)){
                                                                            ?>

                                                                        <div class="col s12 m6 l6">
                                                                            <div class="card horizontal"
                                                                                style="height: 130px">
                                                                                <div class="card-image">
                                                                                    <img src="../../assets/images/gallary/frame.png"
                                                                                        style="height: 101%;">
                                                                                </div>
                                                                                <div class="card-stacked">
                                                                                    <div class="card-content"
                                                                                        style="padding: 13px">
                                                                                        <p><?= $p_makan['nama_kategori'] ?>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="card-action">
                                                                                        <a href="#lihat_kategori?nama_kategori=<?= $p_makan['nama_kategori'] ?>"
                                                                                            class="btn waves-effect waves-light btn modal-trigger">Lihat</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>

                                                                <div id="lihat_masakan?jenis=Minuman" class="modal">
                                                                    <div class="modal-content">
                                                                    <h6 style="font-weight :bold">Kategori Minuman</h6>
                                                                        <?php 
                                                                                $minum = "SELECT * FROM kategori WHERE jenis = 'Minuman' ";
                                                                                    $o_minum = $mysqli->query($minum);
                                                                                    while($p_minum = mysqli_fetch_array($o_minum)){
                                                                            ?>
                                                                        <div class="col s12 m6 l6">
                                                                            <div class="card horizontal"
                                                                                style="height: 130px">
                                                                                <div class="card-image">
                                                                                    <img src="../../assets/images/gallary/frame.png"
                                                                                        style="height: 101%;">
                                                                                </div>
                                                                                <div class="card-stacked">
                                                                                    <div class="card-content"
                                                                                        style="padding: 13px">
                                                                                        <p><?= $p_minum['nama_kategori'] ?>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="card-action">
                                                                                        <a href="#lihat_kategori?nama_kategori=<?= $p_minum['nama_kategori'] ?>"
                                                                                            class="btn waves-effect waves-light btn modal-trigger">Lihat</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <?php 
                                                                    $asfasfasf = "SELECT * FROM kategori WHERE jenis = 'Minuman' ";
                                                                        $csgsdsdg = $mysqli->query($asfasfasf);
                                                                        while($asfeafsaef = mysqli_fetch_array($csgsdsdg)){
                                                                ?>
                                                                <div id="lihat_kategori?nama_kategori=<?= $asfeafsaef['nama_kategori'] ?>"
                                                                    class="modal">
                                                                    <div class="modal-content">
                                                                    <h6 style="font-weight :bold"><?= $asfeafsaef['nama_kategori'] ?></h6>
                                                                        <?php
                                                                                $eetetas = "SELECT * FROM kategori WHERE jenis = 'Minuman' AND nama_kategori = '$asfeafsaef[nama_kategori]' ";
                                                                                    $fddfb = $mysqli->query($eetetas);
                                                                                    while($vddfhdfdf = mysqli_fetch_array($fddfb)){
                                                                                
                                                                                    include "../koneksi.php";
                                                                                    $data = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$asfeafsaef[nama_kategori]' ORDER BY nama_masakan ASC";
                                                                                    $bacadata = $mysqli->query($data);
                                                                                    while($select_result = mysqli_fetch_array($bacadata))
                                                                                    {
                                                                                    $id_masakan          = $select_result['id_masakan'];
                                                                                    $nama_masakan        = $select_result['nama_masakan'];
                                                                                    $gambar              = $select_result['gambar'];
                                                                                    $harga               = $select_result['harga'];
                                                                                ?>
                                                                        <div class="col s12 m3 l3">
                                                                            <div class="card medium"
                                                                                style="height:360px">
                                                                                <div class="card-image">
                                                                                    <img src="../../assets/images/masakan/<?php echo $gambar; ?>"
                                                                                        alt="sample">
                                                                                    <input type="hidden" name=""
                                                                                        placeholder="<?php echo $id_masakan; ?>">
                                                                                    <span
                                                                                        class="card-title"><?php echo $nama_masakan; ?></span>
                                                                                </div>
                                                                                <div class="card-content"
                                                                                    style="padding-top:10px">
                                                                                    <style>
                                                                                        .as li {
                                                                                            float: left;

                                                                                        }
                                                                                    </style>
                                                                                    <ul class="as">
                                                                                        <li
                                                                                            style="font-size:1.6em;padding-right: 15px">
                                                                                            <b>Rp.<?php echo number_format($harga); ?></b>
                                                                                        </li>
                                                                                        <li
                                                                                            style="float:right;margin-top:12%">
                                                                                            RST | Admin</li>
                                                                                    </ul>
                                                                                </div>
                                                                                <div class="card-action center">
                                                                                    <a href="#ubah_masakan<?php echo $id_masakan;?>"
                                                                                        class="btn waves-effect waves-light btn modal-trigger">Ubah</a>
                                                                                </div>
                                                                            </div>


                                                                        </div>

                                                                        <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                            $ggfdgfg = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$asfeafsaef[nama_kategori]' ORDER BY nama_masakan ASC";
                                                                            $eryet = $mysqli->query($ggfdgfg);
                                                                            while($fgjfg = mysqli_fetch_array($eryet)){
                                                                        ?>
                                                                <!--- Ubah Masakan --->
                                                                <form
                                                                    action="../../config/mf_min_ubah_masakan.php?id_masakan=<?php echo $fgjfg['id_masakan'];?>"
                                                                    method="post" enctype="multipart/form-data">
                                                                    <div id="ubah_masakan<?php echo $fgjfg['id_masakan'];?>"
                                                                        class="modal modal-fixed-footer">
                                                                        <div class="modal-content">
                                                                            <h6 style="font-weight:bold">Ubah
                                                                                Minuman</h6>
                                                                            <input name="nama_masakan" type="text"
                                                                                value="<?php echo $fgjfg['nama_masakan'];?>"
                                                                                required="">
                                                                            <input name="harga" type="number"
                                                                                value="<?php echo $fgjfg['harga'];?>"
                                                                                required="">
                                                                            <label>Gambar</label>
                                                                            <br>
                                                                            <br>
                                                                            <input type="file"
                                                                                value="<?php echo $fgjfg['gambar'];?>"
                                                                                name="gambar" accept="image/*">
                                                                            <br>
                                                                            <br>
                                                                            <div class="input-field col s12">
                                                                                <select name="id_kategori">
                                                                                    <option disabled>Pilih
                                                                                        Kategori</option>
                                                                                    <option name="id_kategori"
                                                                                        value="<?= $fgjfg['id_kategori'] ?>"
                                                                                        selected>
                                                                                        <?= $fgjfg['nama_kategori'] ?>
                                                                                    </option>

                                                                                    <?php
                                                                                        $a = "SELECT * FROM `kategori` WHERE NOT nama_kategori = '$fgjfg[nama_kategori]' ORDER BY nama_kategori ASC";
                                                                                        $p = $mysqli->query($a);
                                                                                        while($c = mysqli_fetch_array($p))
                                                                                        {
                                                                                            $id_kategori    = $c['id_kategori'];
                                                                                            $nama_kategori  = $c['nama_kategori'];
                                                                                    ?>
                                                                                    <option name="id_kategori"
                                                                                        value="<?= $id_kategori ?>">
                                                                                        <?= $nama_kategori ?>
                                                                                    </option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                                <label>Kategori Menu</label>
                                                                            </div>
                                                                            <div class="container"><label>Jenis
                                                                                    Menu</label><br>
                                                                                <?php
                                                                                        if ($fgjfg['jenis'] == 'Makanan') {
                                                                                        ?>
                                                                                <label>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Makanan"
                                                                                        required checked />
                                                                                    <span>Makanan</span>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Minuman"
                                                                                        required />
                                                                                    <span>Minuman</span>
                                                                                </label>
                                                                                <?php } else { ?>
                                                                                <label>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Minuman"
                                                                                        required checked />
                                                                                    <span>Minuman</span>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Makanan"
                                                                                        required />
                                                                                    <span>Makanan</span>
                                                                                </label>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit"
                                                                                class="waves-effect waves-light btn modal-trigger"
                                                                                style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Submit</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                <!-- Ubah Masakan -->
                                                                <?php } }
                                                                    $fhgh = "SELECT * FROM kategori WHERE jenis = 'Makanan' ";
                                                                        $kgyjhf = $mysqli->query($fhgh);
                                                                        while($wqeweqs = mysqli_fetch_array($kgyjhf)){
                                                                ?>
                                                                <div id="lihat_kategori?nama_kategori=<?= $wqeweqs['nama_kategori'] ?>"
                                                                    class="modal">
                                                                    <div class="modal-content">
                                                                    <h6 style="font-weight :bold"><?= $wqeweqs['nama_kategori'] ?></h6>
                                                                        <?php
                                                                                $aiusfasif = "SELECT * FROM kategori WHERE jenis = 'Makanan' AND nama_kategori = '$wqeweqs[nama_kategori]' ";
                                                                                    $hfhfhf = $mysqli->query($aiusfasif);
                                                                                    while($dgdfdf = mysqli_fetch_array($hfhfhf)){
                                                                                
                                                                                    include "../koneksi.php";
                                                                                    $dgdgdg = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$wqeweqs[nama_kategori]' ORDER BY nama_masakan ASC";
                                                                                    $bvbvb = $mysqli->query($dgdgdg);
                                                                                    while($dgdsggs = mysqli_fetch_array($bvbvb))
                                                                                    {
                                                                                    $id_masakan          = $dgdsggs['id_masakan'];
                                                                                    $nama_masakan        = $dgdsggs['nama_masakan'];
                                                                                    $gambar              = $dgdsggs['gambar'];
                                                                                    $harga               = $dgdsggs['harga'];
                                                                                ?>
                                                                        <div class="col s12 m3 l3">
                                                                            <div class="card medium"
                                                                                style="height:360px">
                                                                                <div class="card-image">
                                                                                    <img src="../../assets/images/masakan/<?php echo $gambar; ?>"
                                                                                        alt="sample">
                                                                                    <input type="hidden" name=""
                                                                                        placeholder="<?php echo $id_masakan; ?>">
                                                                                    <span
                                                                                        class="card-title"><?php echo $nama_masakan; ?></span>
                                                                                </div>
                                                                                <div class="card-content"
                                                                                    style="padding-top:10px">
                                                                                    <style>
                                                                                        .as li {
                                                                                            float: left;

                                                                                        }
                                                                                    </style>
                                                                                    <ul class="as">
                                                                                        <li
                                                                                            style="font-size:1.6em;padding-right: 15px">
                                                                                            <b>Rp.<?php echo number_format($harga); ?></b>
                                                                                        </li>
                                                                                        <li
                                                                                            style="float:right;margin-top:12%">
                                                                                            RST | Admin</li>
                                                                                    </ul>
                                                                                </div>
                                                                                <div class="card-action center">
                                                                                    <a href="#ubah_masakan<?php echo $id_masakan;?>"
                                                                                        class="btn waves-effect waves-light btn modal-trigger">Ubah</a>
                                                                                </div>
                                                                            </div>


                                                                        </div>

                                                                        <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                            $rffdhf = "SELECT * from masakan JOIN kategori ON masakan.id_kategori=kategori.id_kategori WHERE kategori.nama_kategori = '$wqeweqs[nama_kategori]' ORDER BY nama_masakan ASC";
                                                                            $dgsdg = $mysqli->query($rffdhf);
                                                                            while($asfagg = mysqli_fetch_array($dgsdg)){
                                                                        ?>
                                                                <!--- Ubah Masakan --->
                                                                <form
                                                                    action="../../config/mf_min_ubah_masakan.php?id_masakan=<?php echo $asfagg['id_masakan'];?>"
                                                                    method="post" enctype="multipart/form-data">
                                                                    <div id="ubah_masakan<?php echo $asfagg['id_masakan'];?>"
                                                                        class="modal modal-fixed-footer">
                                                                        <div class="modal-content">
                                                                            <h6 style="font-weight:bold">Ubah
                                                                                Minuman</h6>
                                                                            <input name="nama_masakan" type="text"
                                                                                value="<?php echo $asfagg['nama_masakan'];?>"
                                                                                required="">
                                                                            <input name="harga" type="number"
                                                                                value="<?php echo $asfagg['harga'];?>"
                                                                                required="">
                                                                            <label>Gambar</label>
                                                                            <br>
                                                                            <br>
                                                                            <input type="file"
                                                                                value="<?php echo $asfagg['gambar'];?>"
                                                                                name="gambar" accept="image/*">
                                                                            <br>
                                                                            <br>
                                                                            <div class="input-field col s12">
                                                                                <select name="id_kategori">
                                                                                    <option disabled>Pilih
                                                                                        Kategori</option>
                                                                                    <option name="id_kategori"
                                                                                        value="<?= $asfagg['id_kategori'] ?>"
                                                                                        selected>
                                                                                        <?= $asfagg['nama_kategori'] ?>
                                                                                    </option>

                                                                                    <?php
                                                                                        $aaksufasiuf = "SELECT * FROM `kategori` WHERE NOT nama_kategori = '$asfagg[nama_kategori]' ORDER BY nama_kategori ASC";
                                                                                        $alisfg = $mysqli->query($aaksufasiuf);
                                                                                        while($asagsga = mysqli_fetch_array($alisfg))
                                                                                        {
                                                                                            $id_kategori    = $asagsga['id_kategori'];
                                                                                            $nama_kategori  = $asagsga['nama_kategori'];
                                                                                    ?>
                                                                                    <option name="id_kategori"
                                                                                        value="<?= $id_kategori ?>">
                                                                                        <?= $nama_kategori ?>
                                                                                    </option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                                <label>Kategori Menu</label>
                                                                            </div>
                                                                            <div class="container"><label>Jenis
                                                                                    Menu</label><br>
                                                                                <?php
                                                                                        if ($asfagg['jenis'] == 'Makanan') {
                                                                                        ?>
                                                                                <label>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Makanan"
                                                                                        required checked />
                                                                                    <span>Makanan</span>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Minuman"
                                                                                        required />
                                                                                    <span>Minuman</span>
                                                                                </label>
                                                                                <?php } else { ?>
                                                                                <label>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Minuman"
                                                                                        required checked />
                                                                                    <span>Minuman</span>
                                                                                    <input class="with-gap" name="jenis"
                                                                                        type="radio" value="Makanan"
                                                                                        required />
                                                                                    <span>Makanan</span>
                                                                                </label>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit"
                                                                                class="waves-effect waves-light btn modal-trigger"
                                                                                style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Submit</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                <?php } } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out"
        style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);">
    </div>
    <div class="drag-target" data-sidenav="chat-out"
        style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    </div>
</body>

</html>
<?php
session_start();
include "../head.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
 
// cek apakah yang mengakses halaman ini sudah login
if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_administrator");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">
<title>RST | Admin - Kasir</title>

<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START HEADER -->
            <?php include "top_bar.php";?>    
            <!-- END HEADER -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container"
                    style="transform: translateX(0px);">
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="../../assets/images/avatar/avatar-7.png" alt=""
                                    class="circle responsive-img valign profile-image cyan">
                            </div>
                            <?php include "../user-dropdown.php"; ?>
                        </div>
                    </li>
                    <li class="no-padding">
                        <ul class="collapsible" data-collapsible="accordion">
                            <!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                            <?php include "left-menu.php" ?>
                            <!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                            <li>
                                <a class="btn waves-effect waves-light gradient-45deg-red-pink modal-trigger"
                                    href="#modal">
                                    <i class="material-icons white-text">add</i>Tambah Owner
                                </a>
                            </li>
                        </ul>
                    </li>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                        <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                    </div>
                </ul>
                <a href="#" data-activates="slide-out"
                    class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
                    <i class="material-icons">menu</i>
                </a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!--- Modal --->
            <!-- proses tambah owner sR9ze7fUBPCQIIyt -->
            <!-- aksi di mf_min_proc FpE46vHa3RKhw9N4 -->
            <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=sR9ze7fUBPCQIIyt" method="post"
                enctype="multipart/form-data">
                <div id="modal" class="modal modal-fixed-footer" style="height: 65%">
                    <div class="modal-content">
                        <input name="nama_user" type="text" placeholder="Nama Lengkap" required="">
                        <input name="email" type="email" placeholder="E-mail" required="">
                        <input name="username" type="text" placeholder="Username" required="">
                        <input name="password" type="password" placeholder="Password" required="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="waves-effect waves-light btn modal-trigger"
                            style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Submit</button>
                    </div>
                </div>
            </form>
            <!-- Modal -->
            <?php
                $no=0;
                $data = "SELECT * from user";
                $bacadata = $mysqli->query($data);
                while($select_result = mysqli_fetch_array($bacadata))
                {
                ?>
            <?php
                $id = $select_result['id_user']; 
                $query_edit = $mysqli->query("SELECT * FROM user WHERE id_user='$id'");
                $r = mysqli_fetch_array($query_edit);
            ?>
            <!--- Modal 1 --->
            <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=uwj81kVED09iuGph" method="post"
                enctype="multipart/form-data">
                <div id="modal1<?php echo $select_result['id_user'];?>" class="modal modal-fixed-footer"
                    style="height: 65%">
                    <div class="modal-content">
                        <input type="hidden" name="id_user" value="<?php echo $r['id_user']?>">
                        <input name="nama_user" type="text" placeholder="Nama Lengkap"
                            value="<?php echo $r['nama_user']?>" required="">
                        <input name="email" type="email" placeholder="E-mail" value="<?php echo $r['email']?>"
                            required="">
                        <input name="username" type="text" placeholder="Username" value="<?php echo $r['username']?>"
                            required="">
                        <input name="password" type="password" placeholder="Password"
                            value="<?php echo $r['password']?>" required="">
                        <input type="hidden" name="password_lama" placeholder="" value="<?php echo $r['password'] ?>">
                        <div class="container"><label>Status Kasir</label><br>
                            <?php
                                if ($r['status'] == 'O') {
                            ?>
                            <label>
                            
                            <input class="with-gap " name="status" type="radio" value="I"
                                    required/>
                                <span>Non Aktifkan</span>
                            </label>
                            <?php } else { ?>
                            <label>
                                <input class="with-gap " name="status" type="radio" value="O"
                                    required/>
                                <span>Aktifkan</span>
                            </label>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="waves-effect waves-light btn modal-trigger"
                            style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%)">Submit</button>
                    </div>
                </div>
            </form>
            <!-- Modal 1 -->
            <?php } ?>

            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div id="card-widgets">
                        <section id="content">
                            <!--start container-->
                            <div class="container">
                                <div class="section">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="card-panel">
                                                <!--Responsive Table-->
                                                <div id="responsive-table">
                                                    <h4 class="header">Data Owner</h4>
                                                    <div class="row">
                                                        <div class="col s11">
                                                            <table width="100%"
                                                                class="display nowrap"
                                                                id="dataTables-example">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="text-align:center;">No</th>
                                                                        <th style="text-align:center;">Nama</th>
                                                                        <th style="text-align:center;">Username</th>
                                                                        <th style="text-align:center;">Opsi</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    <?php
                                                                        $no = 1;
                                                                        foreach($db->mf_show_owner() as $x){
                                                                    ?>
                                                                    <tr>
                                                                        <td style="text-align:center;" scope="row">
                                                                            <?php echo $no++; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?php echo $x['nama_user']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <?php echo $x['username']; ?>
                                                                        </td>
                                                                        <td style="text-align:center;"><a
                                                                                class="btn waves-effect waves-light gradient-45deg-red-pink modal-trigger"
                                                                                href="#modal1<?php echo $x['id_user'];?>">Edit</a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out"
        style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);">
    </div>
    <div class="drag-target" data-sidenav="chat-out"
        style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    </div>
</body>

</html>
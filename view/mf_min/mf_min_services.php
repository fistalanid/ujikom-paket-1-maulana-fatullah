<?php
include "../head.php";
include "../koneksi.php";

session_start();

if($_SESSION['id_level']==""){
  header("location:../../format/index.php?msg=login_to_access_administrator");
}

elseif($_SESSION['id_level']=="1"){
    $warna    = "background: linear-gradient(45deg, #363442 0%, #2196F3 50%, #E91E63 100%);";
}
  
elseif($_SESSION['id_level']=="2"){
  header("location:../mf_pelayan/mf_pelayan.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../mf_kasir/mf_kasir.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../mf_owner/mf_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../mf_pelanggan/mf_pelanggan.php");
}
?>
<html lang="en">

<head>
    <title>RST | Administrator</title>
    <!-- Front End CSS -->
    <link rel="stylesheet" href="../../format/css/styles-merged.css">
    <link rel="stylesheet" href="../../format/css/style.min.css">
    <link rel="stylesheet" href="../../format/css/custom.css">
</head>


<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="navbar-color gradient-45deg-light-blue-cyan" style="<?= $warna ?>">
                <div class="nav-wrapper">
                    <ul class="left">
                        <li>
                            <h1 class="logo-wrapper">
                                <a href="index.html" class="brand-logo darken-1">
                                    <img src="../../assets/images/logo/materialize-logo.png" alt="materialize logo">
                                </a>
                            </h1>
                        </li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">


                    </div>
                    <ul class="right hide-on-med-and-down">

                        <li>
                            <a href="javascript:void(0);"
                                class="waves-effect waves-block waves-light toggle-fullscreen">
                                <i class="material-icons">settings_overscan</i>
                            </a>
                        </li>

                        <li>
                            </a>
                            <ul id="profile-dropdown" class="dropdown-content"
                                style="white-space: nowrap; opacity: 1; left: 1150.64px; position: absolute; top: 64px; display: none;">
                                <li>
                                    <a href="#" class="grey-text text-darken-1">
                                        <i class="material-icons">face</i> Profile</a>
                                </li>
                                <li>
                                    <a href="#" class="grey-text text-darken-1">
                                        <i class="material-icons">settings</i> Settings</a>
                                </li>
                                <li>
                                    <a href="#" class="grey-text text-darken-1">
                                        <i class="material-icons">live_help</i> Help</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" class="grey-text text-darken-1">
                                        <i class="material-icons">lock_outline</i> Lock</a>
                                </li>
                                <li>
                                    <a href="#" class="grey-text text-darken-1">
                                        <i class="material-icons">keyboard_tab</i> Logout</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="../mf_p_logout.php" class="waves-effect waves-block waves-heavy"
                                onClick="return confirm('Anda ingin Keluar ?')">
                                <i class="material-icons">keyboard_tab</i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container"
                    style="transform: translateX(0px);">
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="../../assets/images/avatar/avatar-7.png" alt=""
                                    class="circle responsive-img valign profile-image cyan">
                            </div>
                            <?php include "../user-dropdown.php"; ?>
                        </div>
                    </li>
                    <li class="no-padding">
                        <ul class="collapsible" data-collapsible="accordion">
                            <?php include "left-menu.php" ?>
                        </ul>
                    </li>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                        <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                    </div>
                </ul>
                <a href="#" data-activates="slide-out"
                    class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
                    <i class="material-icons">menu</i>
                </a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <?php
                $sss = $mysqli->query("SELECT * FROM `f_services`");
                
                while($ddd = mysqli_fetch_array($sss)){
                
                    $id_services                = $ddd['id_services'];
                    $d_services_desc            = $ddd['d_services_desc'];
                    $d_first_services_title     = $ddd['d_first_services_title'];
                    $d_second_services_title    = $ddd['d_second_services_title'];
                    $d_third_services_title     = $ddd['d_third_services_title'];
                    $d_first_services_desc      = $ddd['d_first_services_desc'];
                    $d_second_services_desc     = $ddd['d_second_services_desc'];
                    $d_third_services_desc      = $ddd['d_third_services_desc'];
            ?>
            <section class="probootstrap-section">
                <div class="container">
                    <div class="row">
                        <div
                            class="col-lg-12 col-md-12 mb70 section-heading probootstrap-animate fadeInUp probootstrap-animated">
                            <h2>Pelayanan Kami</h2>
                            <p class="lead"><?= $d_services_desc ?></p>
                        </div>
                    </div>
                    <div class="row mb70">
                        <div class="col-md-4 probootstrap-animate fadeInUp probootstrap-animated">
                            <div class="probootstrap-box">
                                <div class="icon text-center"><i class="icon-tools2"></i></div>
                                <h3><?= $d_first_services_title ?></h3>
                                <p><?= $d_first_services_desc ?></p>
                            </div>
                        </div>
                        <div class="col-md-4 probootstrap-animate fadeInUp probootstrap-animated">
                            <div class="probootstrap-box">
                                <div class="icon text-center"><i class="icon-desktop"></i></div>
                                <h3><?= $d_second_services_title ?></h3>
                                <p><?= $d_second_services_desc ?></p>
                            </div>
                        </div>
                        <div class="col-md-4 probootstrap-animate fadeInUp probootstrap-animated">
                            <div class="probootstrap-box">
                                <div class="icon text-center"><i class="icon-lightbulb"></i></div>
                                <h3><?= $d_third_services_title ?></h3>
                                <p><?= $d_third_services_desc ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </section>
                            <?php } ?>
            <a href="#edit_service" class="btn-floating modal-trigger"
                style="position: fixed;z-index: 999999;top: 80%;right:10%"><i class="material-icons">edit</i></a>
            <!-- END CONTENT -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END WRAPPER -->
    </div>

    <!--- Edit Home --->
    <!-- proses edit pelayanan front end Fq6Mtcslt9DraqPR -->
    <!-- aksi di mf_min_proc FpE46vHa3RKhw9N4 -->
    <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=Fq6Mtcslt9DraqPR" method="post"
        enctype="multipart/form-data">
        <div id="edit_service" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h6 class="header">Edit Pelayanan</h6>
                <?php
          $sss = $mysqli->query("SELECT * FROM `f_services`");
          
          while($ddd = mysqli_fetch_array($sss)){
        
            $id_services                = $ddd['id_services'];
            $d_services_desc            = $ddd['d_services_desc'];
            $d_first_services_title     = $ddd['d_first_services_title'];
            $d_second_services_title    = $ddd['d_second_services_title'];
            $d_third_services_title     = $ddd['d_third_services_title'];
            $d_first_services_desc      = $ddd['d_first_services_desc'];
            $d_second_services_desc     = $ddd['d_second_services_desc'];
            $d_third_services_desc      = $ddd['d_third_services_desc'];
            // Pelayanan terbaik untuk pelanggan yang terbaik akan diberikan oleh restauran kami 
        ?>
    <input type="hidden" name="id_services" value="<?= $id_services ?>">
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <textarea name="d_services_desc" id="icon_prefix2" class="materialize-textarea"
                        required><?= $d_services_desc ?></textarea>
                    <label for="icon_prefix2">Deskripsi Pelayanan</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <textarea name="d_first_services_title" id="icon_prefix2" class="materialize-textarea"
                        required><?= $d_first_services_title ?></textarea>
                    <label for="icon_prefix2">Judul Pelayanan Pertama</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <textarea name="d_second_services_title" id="icon_prefix2" class="materialize-textarea"
                        required><?= $d_second_services_title ?></textarea>
                    <label for="icon_prefix2">Judul Pelayanan Kedua</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <textarea name="d_third_services_title" id="icon_prefix2" class="materialize-textarea"
                        required><?= $d_third_services_title ?></textarea>
                    <label for="icon_prefix2">Judul Pelayanan Ketiga</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <textarea name="d_first_services_desc" id="icon_prefix2" class="materialize-textarea"
                        required><?= $d_first_services_desc ?></textarea>
                    <label for="icon_prefix2">Dekripsi Pelayanan Pertama</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <textarea name="d_second_services_desc" id="icon_prefix2" class="materialize-textarea"
                        required><?= $d_second_services_desc ?></textarea>
                    <label for="icon_prefix2">Dekripsi Pelayanan Kedua</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <textarea name="d_second_services_desc" id="icon_prefix2" class="materialize-textarea"
                        required><?= $d_third_services_desc ?></textarea>
                    <label for="icon_prefix2">Dekripsi Pelayanan Ketiga</label>
                </div>
                <?php } ?>
            </div>

            <div class="modal-footer">
                <button type="submit" class="waves-effect waves-light btn modal-trigger"
                    style="background: linear-gradient(45deg, #e91d1d 0%, #a04358 100%);margin-top:"></button>
            </div>
        </div>
    </form>
    <!-- Edit Home -->
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out"
        style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);">
    </div>
    <div class="drag-target" data-sidenav="chat-out"
        style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    </div>
</body>

</html>
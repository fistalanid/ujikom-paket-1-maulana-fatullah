<?php
include "../head.php";
include "../cnn.php";
include "../koneksi.php";
include "../../database/database.php";
$db = new database();
session_start();

// cek apakah yang mengakses halaman ini sudah login
if($_SESSION['id_level']==""){
    header("location:../../format/index.php?msg=login_to_access_administrator");
  }
  
  elseif($_SESSION['id_level']=="2"){
    header("location:../mf_pelayan/mf_pelayan.php");
  }
  
  elseif($_SESSION['id_level']=="3"){
    header("location:../mf_kasir/mf_kasir.php");
  }
  
  elseif($_SESSION['id_level']=="4"){
    header("location:../mf_owner/mf_owner.php");
  }
  
  elseif($_SESSION['id_level']=="5"){
    header("location:../mf_pelanggan/mf_pelanggan.php");
  }
?>
<html lang="en">
<title>RST | Pelayan :: <?php echo $_SESSION['username']; ?></title>

<body class="loaded">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <?php include "../top_nav.php"; ?>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container"
                    style="transform: translateX(0px);">
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="../../assets/images/avatar/avatar-7.png" alt=""
                                    class="circle responsive-img valign profile-image cyan">
                            </div>
                            <?php include "../user-dropdown.php"; ?>
                        </div>
                    </li>
                    <li class="no-padding">
                        <ul class="collapsible" data-collapsible="accordion">
                            <!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                            <?php include "left-menu.php" ?>
                            <!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                        </ul>
                    </li>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                        <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                        <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                    </div>
                </ul>
                <a href="#" data-activates="slide-out"
                    class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
                    <i class="material-icons">menu</i>
                </a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTENT -->
            <section id="content">
                <!--start container-->
                <div class="container">
                    <!--card stats start-->
                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <div class="card-panel">
                        <h4 class="header">Meja Pesanan Belum Dibayar</h4>
                        <div class="row">
                            <?php
                                $data = "SELECT *,`order`.`id_meja`,`order`.`id_user` FROM `meja` JOIN `order` ON `meja`.id_meja=`order`.id_meja JOIN user ON user.id_user=`order`.id_user WHERE NOT `order`.status = 'V' AND user.status = 'O' ORDER BY `order`.id_meja ASC";
                                $bacadata = $mysqli->query($data);
                                while($select_result = mysqli_fetch_array($bacadata))
                            {
                                $id_meja          = $select_result['id_meja'];
                                $id_user          = $select_result['id_user'];
                                $id_order         = $select_result['id_order'];
                                $nama_pelanggan   = $select_result['nama_pelanggan'];
                                $status_meja      = $select_result['status_meja'];
                                $no_meja          = $select_result['no_meja'];
                                $status_order     = $select_result['status_order'];
                                $keterangan       = $select_result['keterangan'];
                                $tanggal          = $select_result['tanggal'];
                            ?>
                            <div class="col s12 m4 l4">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light">
                                        <div class="col s12 m12 l12" style="height: 35%">
                                            <div class="col s5 m5 l5"></div>
                                            <div class="col s4 m4 l4" style="text-shadow: black 2px 2px 5px;font-size: 40px;color: #ff4081;z-index: 99999;position: relative;margin-top: 38%">
                                                <?php echo $no_meja; ?>
                                            </div>
                                            <div class="col s3 m4 l4"></div>
                                        </div>
                                        <img style="position:absolute;z-index: 1;margin-top:10%" class="activator" src="../../assets/images/gallary/frame.png">
                                    </div>
                                    <div class="card-content">
                                        <span class="card-title activator grey-text text-darken-4">
                                            <?php echo $nama_pelanggan?><i class="material-icons right">more_vert</i></span>
                                    </div>
                                    <div class="card-reveal">
                                        <span class="card-title grey-text text-darken-4">
                                            <?php echo $status_order ?><i class="material-icons right">close</i></span>
                                        <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=nFWWeEdVWrjWQmtm"
                                            method="post">
                                            <div class="input-field" style="margin-top: 5px">
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" class="validate col s12 m12 l12" value="<?php echo $tanggal;?>"
                                                            disabled>
                                                        <label class="active">Tanggal Pesanan</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <input type="text" value="<?php echo $keterangan; ?>" disabled>
                                                        <label class="active">Catatan</label>
                                                    </div>
                                                </div>
                                                <a href="#keranjang_pelanggan?id_order=<?= $id_order ?>" class="btn waves-effect waves-light modal-trigger">Pesanan</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=1LzTtCxkaoeNpmEa" method="post"
                                enctype="multipart/form-data">
                                <div id="pesan?id_order=<?php echo $id_order; ?>" class="modal modal-fixed-footer"
                                    style="height: 100%">
                                    <div class="modal-content">
                                        <div id="responsive-table">
                                            <h4 class="header">Data Masakan</h4>
                                            <div class="row section">
                                                <div class="col s12">
                                                    <div class="row section">
                                                        <div class="col s12">
                                                        </div>
                                                        <?php
                                                            $s = "SELECT * from masakan";
                                                            $a = $mysqli->query($s);
                                                            while($c = mysqli_fetch_array($a))
                                                        {
                                                            $id_masakan          = $c['id_masakan'];
                                                            $nama_masakan        = $c['nama_masakan'];
                                                            $harga               = $c['harga'];
                                                            $gambar              = $c['gambar'];
                                                        ?>
                                                        <!-- <div class="col"></div> -->
                                                        <div class="col s12 m6 l6">
                                                            <div class="card">
                                                                <div class="card-image waves-effect waves-block waves-light" style="height: 85%;">
                                                                    <div class="col s12 m12 l12">
                                                                    </div>
                                                                    <img class="activator" src="../../assets/images/masakan/<?php echo $gambar;?>">
                                                                </div>
                                                                <div class="card-content">
                                                                    <span class="card-title activator grey-text text-darken-4">
                                                                        <?php echo $nama_masakan;?><i class="material-icons right">more_vert</i>
                                                                    </span>
                                                                    <p>
                                                                        <?= $harga?>
                                                                        <p>
                                                                </div>
                                                                <div class="card-reveal">
                                                                    <span class="card-title grey-text text-darken-4"><i
                                                                            class="material-icons right">close</i></span>
                                                                    <form action="../../config/mf_min_proc.php?FpE46vHa3RKhw9N4=1LzTtCxkaoeNpmEa"
                                                                        method="post">
                                                                        <div class="input-field">
                                                                            <div class="row">
                                                                                <input type="hidden" name="id_order"
                                                                                    value="<?php echo $id_order; ?>">
                                                                                <input type="hidden" name="id_masakan"
                                                                                    value="<?php echo $id_masakan; ?>">
                                                                                <div class="input-field col s12">
                                                                                    <input name="kuantitas" type="text"
                                                                                        class="validate col s8 m4 l5">
                                                                                    <label class="active">Jumlah</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="input-field col s12">
                                                                                    <input name="keterangan" type="text">
                                                                                    <label class="active">Catatan</label>
                                                                                </div>
                                                                            </div>
                                                                            <button class="btn waves-effect waves-light" type="submit" name="action">Pesan</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                        </div>
                    </div>

                    <!-- //////////////////////////////////////////////////////////////////////////// -->
                    <!--card stats end-->
                </div>
                <!--end container-->
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
    </div>
    <?php 
  $fff = "SELECT *,`order`.`id_meja`,`order`.`id_user` FROM `meja` JOIN `order` ON `meja`.id_meja=`order`.id_meja JOIN user ON user.id_user=`order`.id_user ORDER BY `order`.id_meja ASC";
  $keranjang = $mysqli->query($fff);
    while($keranjang_ane = mysqli_fetch_array($keranjang)){
      $id_meja_ane  = $keranjang_ane['id_meja'];
      $id_user_ane  = $keranjang_ane['id_user'];
      $id_order_ane = $keranjang_ane['id_order'];
?>

<!--- Keranjang Pelanggan --->
<!-- proses memindahkan masakan ke detail_order -->
<form action="../../config/mf_min_to_detail.php" method="post" enctype="multipart/form-data">
  <div id="keranjang_pelanggan?id_order=<?= $id_order_ane ?>" class="modal">
    <?php
          $sssa = mysqli_query($koneksi,"SELECT * FROM user JOIN `order` ON user.id_user=`order`.id_user WHERE user.username = '$_SESSION[username]' AND `order`.status = 'X'");
          $a = mysqli_fetch_array($sssa);
          if(mysqli_num_rows($sssa)){
        ?>
    <a href='../../config/mf_min_cancel_pelanggan.php?id_order=<?= $a['id_order'] ?>&&id_meja=<?= $a['id_meja'] ?>'
      onClick='return confirm("Batalkan pelanggan ?")'><i class='material-icons btn-floating'
        style='float:right;padding:10px;border-radius:1px'>delete_forever</i></a>
    <?php } ?>
    <div class="modal-content">
      <h6 style="font-weight:bold">Pesanan Pelanggan</h6>
      <table class="responsive-table centered Highlight striped bordered">
        <thead>
          <tr>
            <th style="text-align:center;">Masakan</th>
            <th width="1%" style="text-align:center;">Kuantitas</th>
            <th style="text-align:center;">Satuan</th>
            <th style="text-align:center;">Jumlah</th>
            <th style="text-align:center;">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
                $total_semua = 0 ;          
                $data = "SELECT * FROM `detail_order` JOIN `masakan` ON `detail_order`.`id_masakan`=`masakan`.`id_masakan` WHERE detail_order.id_order = '$id_order_ane'";
                $bacadata = $mysqli->query($data);
                while($select_result = mysqli_fetch_array($bacadata))
                {
                $id_masakan            = $select_result['id_masakan'];
                $id_order              = $select_result['id_order'];
                $id_detail_order       = $select_result['id_detail_order'];
                $nama_masakan          = $select_result['nama_masakan'];
                $kuantitas             = $select_result['kuantitas'];
                $harga                 = $select_result['harga'];
                $total_harga           = $select_result['kuantitas']*$select_result['harga'];
                $total_semua           += ($total_harga);

            ?>
          <tr>
            <td>
              <input type="hidden" name="id_masakan[]" value="<?php echo $id_masakan ?>">
              <?= $nama_masakan; ?>
            </td>
            <td>
              <input type="hidden" name="kuantitas[]" value="<?php echo $kuantitas ?>">
              <?php echo $kuantitas ?>
            </td>
            <input type="hidden" name="keterangan[]" value="<?php echo $keterangan ?>">
            <input type="hidden" name="id_user" value="<?php echo $id_user ?>">
            <td>
              <?= "Rp.".number_format($harga) ?>
            </td>
            <td>
              <input type="hidden" name="total_harga" value="<?php echo $total_harga ?>">
              <?= "Rp.".number_format($total_harga) ?>
            </td>
            <td>
              <?php
                $p_proses = $mysqli->query("SELECT * FROM detail_order WHERE id_detail_order = '$id_detail_order'");
                $ppp = mysqli_fetch_array($p_proses);
                if ($ppp['status_detail_order'] == 'Diproses') {
              ?>
              
                <a href="../../config/mf_min_update_status_detail_selesai.php?id_detail_order=<?= $id_detail_order ?>" onClick="return confirm('<?php echo $kuantitas ," " ,$nama_masakan; ?> telah siap ?')" class="btn modal-trigger">Diproses</a>
                |
              <a class="btn modal-trigger" disabled>Ubah</a>
              <?php } elseif ($ppp['status_detail_order'] == 'Selesai') { ?>
                <a class="btn" style="background-color: #00adff;" readonly>Selesai</a>
              <?php } ?>
            </td>

          </tr>
          <?php } ?>
          <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Total : RP.<?= number_format($total_semua) ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</form>
  <?php } ?>
    <!-- END MAIN -->
    <?php include "../footer.php"; ?>
    <div class="hiddendiv common"></div>
    <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color:rgba(0, 0, 0, 0);"></div>
    <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>
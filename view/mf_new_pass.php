<!--- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --->
<?php
  error_reporting(0);
  include "cs_js/index_head.php";
  include "cnn.php";
  include "function.php";

  session_start();

  if($_SESSION['id_level']=="1"){
    header("location:mf_min/mf_min.php");
  }

  elseif($_SESSION['id_level']=="2"){
    header("location:mf_pelayan/mf_pelayan.php");
  }

  elseif($_SESSION['id_level']=="3"){
    header("location:mf_kasir/mf_kasir.php");
  }

  elseif($_SESSION['id_level']=="4"){
    header("location:mf_owner/mf_owner.php");
  }

  elseif($_SESSION['id_level']=="5"){
    header("location:mf_pelanggan/mf_pelanggan.php");
  }

$email = $_GET['email'];
$token = $_GET['token'];

$userID = UserID($email); 

$verifytoken = verifytoken($userID, $token);




if(isset($_POST['submit']))
{
  $new_password = $_POST['new_password'];
  $new_password = $new_password;
  $retype_password = $_POST['retype_password'];
  $retype_password = $retype_password;
  
  if($new_password == $retype_password)
  {
    $update_password = mysqli_query($koneksi, "UPDATE user SET password = '$new_password' WHERE id_user = $userID");
    if($update_password)
    {
        mysqli_query($koneksi, "UPDATE recovery_keys SET valid = 0 WHERE userID = $userID AND token ='$token'");
        $msg = "<script type='text/javascript'>
                  alert('Password Berhasil Diganti ,silahkan login kembali');
                  document.location.href='index.php';
                </script>";
              }
  }else
  {
     $msg = "Password doesn't match";
     $msgclass = 'bg-danger';
  }
  
}

  ?>
<!--- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --->
<html lang="en">
<!-- Ganti judul --->
<title>RST | Reset Password</title>
<!-- Ganti judul --->

<body class="loaded gradient-45deg-purple-light-blue">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <header id="header" class="page-topbar">
  </header>
  <div>
    <!-- START WRAPPER -->
    <div class="wrapper">
      <!-- START CONTENT -->
      <section id="content">
        <!--start container-->
        <div class="container">
          <div class="section">
            <!--Basic Form-->
            <!-- Form with icon prefixes -->
            <div class="row">
              <div class="col s4 m4 l4">
              </div>
              <!-- Form with validation -->
              <div class="col s12 m12 l4" style="margin-top: 180px;">
                <div class="card-panel">
                  <h4 class="header2">
                    <center>Reset Password</center>
                  </h4>
                  <div class="row">
                    <form class="col s12" method="post">
                      <?php if(isset($msg)) { ?>
                        <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
                      <?php } ?>
                      <div class="row">
                        <div class="input-field col s12">
                          <i class="material-icons prefix">lock</i>
                          <input id="password" type="password" name="new_password" class="validate" required="">
                          <label for="password" class="">Password</label>
                        </div>
                        <div class="input-field col s12">
                          <i class="material-icons prefix">lock</i>
                          <input id="password" type="password" id="retype_password" name="retype_password" class="validate" required="">
                          <label for="password" class="">Konfirmasi Password</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="row">
                          <div class="input-field col s12">
                            <a href="#login" onClick="javascript:history.go(-1)" style="margin-left: 20px">Back</a>
                            <button class="btn waves-effect waves-light right gradient-45deg-purple-light-blue" type="submit" name="submit">Ubah
                              <i class="material-icons right">send</i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <!--- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --->
  <script type="text/javascript" src="../assets/vendors/jquery-3.2.1.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="../assets/js/materialize.min.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="../assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
  <!--plugins.js - Some Specific JS codes for Plugin Settings-->
  <script type="text/javascript" src="../assets/js/plugins.js"></script>
  <!--custom-script.js - Add your own theme custom JS-->
  <script type="text/javascript" src="../assets/js/custom-script.js"></script>
  <!--- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --->
  <div class="hiddendiv common"></div>
  <div class="drag-target" data-sidenav="slide-out" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
  <div class="drag-target" data-sidenav="chat-out" style="right: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>
</body>

</html>